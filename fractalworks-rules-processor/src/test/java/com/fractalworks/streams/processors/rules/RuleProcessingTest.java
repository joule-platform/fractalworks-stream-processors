package com.fractalworks.streams.processors.rules;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Simple rules processing test that demonstrate API usage
 *
 * @author Lyndon Adams
 */
public class RuleProcessingTest {

    @Test
    public void executeLowUsageTest() throws StreamsException {
        RulesProcessor processor = new RulesProcessor();
        processor.setName("dataUsageCategory");
        processor.setRuleFile("rules/simple.drl" );
        processor.initialize(null);

        StreamEvent event = new StreamEvent();
        event.addValue("dataUsage", 0.45d);
        event.addValue("daysleft", 15);
        processor.apply( event, null);

        RuleResponse response = (RuleResponse) event.getValue("dataUsageCategory");
        assertNotNull( response);
        assertEquals( "Low data usage", response.get("desc"));
    }

    @Test
    public void executeMediumUsageTest() throws StreamsException {
        RulesProcessor processor = new RulesProcessor();
        processor.setName("dataUsageCategory");
        processor.setRuleFile("rules/simple.drl" );
        processor.initialize(null);

        StreamEvent event = new StreamEvent();
        event.addValue("dataUsage", 0.61d);
        event.addValue("daysleft", 9);
        processor.apply( event, null);

        RuleResponse response = (RuleResponse) event.getValue("dataUsageCategory");
        assertNotNull(response);
        assertEquals("Medium data usage", response.get("desc"));
    }

    @Test
    public void executeHighUsageTest() throws StreamsException {
        RulesProcessor processor = new RulesProcessor();
        processor.setName("dataUsageCategory");
        processor.setRuleFile("rules/simple.drl" );
        processor.initialize(null);

        StreamEvent event = new StreamEvent();
        event.addValue("dataUsage",0.91d );
        event.addValue("daysleft", 4);
        processor.apply( event, null);

        RuleResponse response = (RuleResponse) event.getValue("dataUsageCategory");
        assertNotNull(response);
        assertEquals("High data usage", response.get("desc"));
    }


    public void executeRuleTypeLatencyTest() throws StreamsException {
        RulesProcessor processor = new RulesProcessor();
        processor.setName("dataUsageCategory");
        processor.setRuleFile("rules/simple.drl" );
        processor.setPoolSize(10);
        processor.initialize(null);

        long starttime = System.currentTimeMillis();
        StreamEvent event = new StreamEvent();
        event.addValue("dataUsage", 0.99d);
        event.addValue("daysleft", 2);

        for(int i=0; i<500000; i++) {

            processor.apply(event, null);

            RuleResponse response = (RuleResponse) event.getValue("dataUsageCategory");
        }
        System.out.println("Time: " + ( System.currentTimeMillis() - starttime));
    }

}
