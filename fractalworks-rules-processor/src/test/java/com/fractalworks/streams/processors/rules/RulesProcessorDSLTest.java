package com.fractalworks.streams.processors.rules;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

/**
 * Rule DSL validity test
 *
 * @author lyndon Adams
 */
class RulesProcessorDSLTest {

    @ParameterizedTest
    @ValueSource(strings = {"dsl/valid/simple.yaml", "dsl/valid/withDataStore.yaml"})
    void validSimpleProcessorYamlTest(String filename) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        RulesProcessor processor = readMapper.readValue(file, RulesProcessor.class);
        Assertions.assertNotNull(processor);
    }
}
