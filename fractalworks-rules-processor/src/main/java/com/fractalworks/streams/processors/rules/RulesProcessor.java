package com.fractalworks.streams.processors.rules;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.rules.drools.DroolsFactory;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieContainerSessionsPool;
import org.kie.api.runtime.KieSession;

import javax.annotation.Nonnull;
import java.util.Properties;

/**
 * Stateless rules processor implemented using the RedHat Drools rules engine. Events are processed in stream using
 * a combination of reference data, rules and standard responses.
 *
 * Example drl file:
 *
 * package com.fractalworks.streams.processors.rules;
 * import com.fractalworks.streams.core.data.streams.StreamEvent;
 * global com.fractalworks.streams.core.data.streams.Context ctx;
 * global com.fractalworks.streams.processors.rules.RuleResponse response;
 *
 * dialect  "mvel"
 *
 * rule "High data usage"
 *     when
 *         StreamEvent( getValue("dataUsage") > 0.90,
 *                           getValue("daysleft") <= 5)
 *     then
 *         response.setRuleId("High data usage");
 *         response.put("desc","High data usage");
 * end
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "rules")
public class RulesProcessor extends AbstractProcessor {
    private String ruleFile;
    private KieContainerSessionsPool sessionsPool;
    private int poolSize = 8;

    public RulesProcessor() {
        super();
        super.cloneEvent = false;
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        DroolsFactory droolsFactory = new DroolsFactory();
        KieContainer container = droolsFactory.createRuleContainer( ruleFile );
        if( container != null){
            sessionsPool = container.newKieSessionsPool(poolSize);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        KieSession session = sessionsPool.newKieSession();

        // Add context
        if (context != null) {
            session.setGlobal("context", context);
        }

        // Add reference data
        if (dataStores != null) {
            dataStores.forEach(session::setGlobal);
        }

        // Set response object and execute rule
        RuleResponse response = new RuleResponse();
        session.setGlobal("response", response);

        // Add Event as a fact to process
        session.insert(event);
        session.fireAllRules();

        // Only add a response object if
        if (!response.isEmpty()) {
            event.addValue(uuid, name, response);
        }

        // Release resources
        session.dispose();
        metrics.incrementMetric(Metric.PROCESSED);
        return event;
    }

    @JsonProperty(value = "rule file")
    public void setRuleFile(@Nonnull String ruleFile) {
        this.ruleFile = ruleFile;
    }

    @JsonProperty(value = "pool size")
    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    @Override
    public void shutdown() {
        super.shutdown();
        sessionsPool.shutdown();
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(ruleFile == null || ruleFile.isBlank() ){
            throw new InvalidSpecificationException("ruleFile must be defined");
        }
        if(poolSize < 1){
            throw new InvalidSpecificationException("pool size cannot be less than 1");
        }
    }

}
