package com.fractalworks.streams.processors.rules;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A simple response object which is used to return any rules based outcomes to the caller
 *
 * @author Lyndon Adams
 */
public class RuleResponse extends HashMap<String,Object> {

    String ruleId;

    public RuleResponse(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public RuleResponse(int initialCapacity) {
        super(initialCapacity);
    }

    public RuleResponse() {
    }

    public RuleResponse(Map<String, ?> m) {
        super(m);
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RuleResponse that = (RuleResponse) o;
        return Objects.equals(ruleId, that.ruleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ruleId);
    }
}
