package com.fractalworks.streams.processors.rules.drools;

import org.kie.api.KieServices;
import org.kie.api.builder.*;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Drools utility class to load drl files and build the required rules subsystems
 *
 * @author Lyndon Adams
 */
public class DroolsFactory {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private KieServices kieServices;

    public DroolsFactory() {
        init();
    }

    private void init(){
        kieServices = KieServices.Factory.get();
        KieRepository kieRepository = kieServices.getRepository();
        kieRepository.addKieModule(kieRepository::getDefaultReleaseId);
    }

    /**
     * Create a rule container
     *
     * @param ruleFile - Standard Drool rule file
     * @return
     */
    public KieContainer createRuleContainer(String ruleFile) {
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(ResourceFactory.newClassPathResource(ruleFile));
        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();
        KieModule kieModule = kb.getKieModule();
        return kieServices.newKieContainer(kieModule.getReleaseId());
    }

    public KieSession getKieSession(Resource dt) {
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem()
                .write(dt);

        kieServices.newKieBuilder(kieFileSystem).buildAll();

        KieRepository kieRepository = kieServices.getRepository();
        ReleaseId krDefaultReleaseId = kieRepository.getDefaultReleaseId();
        KieContainer kieContainer = kieServices.newKieContainer(krDefaultReleaseId);
        return kieContainer.newKieSession();
    }

    public void setGlobalVariable(KieSession session, String key, Object value){
        try {
            session.setGlobal(key, value);
        } catch (RuntimeException e) {
            if (!e.getLocalizedMessage().equals(String.format("Unexpected global [%s]",value))) {
                throw e;
            }
            logger.error(e.getLocalizedMessage(), e);
        }
    }

}
