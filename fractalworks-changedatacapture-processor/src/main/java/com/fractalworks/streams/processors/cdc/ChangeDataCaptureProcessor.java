/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.cdc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.util.helpers.ObjectCascadeSearchHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Event based Change Data Capture processor. Provides a map of state changes when they occur using "cdc" key. Default behaviour is
 * to drop the event if no CDC event has occured i.e. acts as a change filter, set emitOnlyCDC to false if event needs to pass through.
 * <p>
 * On a CDC field change the event will be populated with a map of changed fields and values
 * where the {@link Tuple} is set with previous and current values, V1 and V2 respectively.
 * <p>
 * Nested field paths are supported using the dot notation i.e. f1.f2.f3
 * <p>
 *
 * Example config
 *
 * processors:
 *
 * changeDataCapture:
 *     name: "my-cdc"
 *     keyField: "symbol"
 *     fields: ["f1","f2"]
 *
 *
 * Default setting
 *
 *   enabled: true
 *   cloneEvent: true
 *   pathNotation: "."
 *   emitOnChange: true
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "change data capture")
public class ChangeDataCaptureProcessor extends AbstractProcessor {

    public static final String CDC_EVENT = "CDC";
    public static final String CDC_FIELDS = "CDC_FIELDS";

    private String keyField;
    private char pathSeparator = '.';
    private List<String> trackingFields;

    private final Map<Object, StreamEvent> localState = new HashMap<>();
    private boolean emitOnlyCDC = true;
    private boolean dropDuplicates = true;

    public ChangeDataCaptureProcessor() {
        super();
        super.cloneEvent = true;
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        try {
            final StreamEvent orignalEvent = event.deepCopy();

            // Traceable fields
            final AtomicBoolean isCDCApplied = new AtomicBoolean(false);

            // Apply CDC algorithm
            final Map<String, Tuple<Object, Object>> changedFields = applyCDC(orignalEvent, event,isCDCApplied);

            // If CDC found process next step
            if (isCDCApplied.get()) {
                // Optimised record
                if (emitOnlyCDC) {
                    event = createdCDCRecord(orignalEvent, changedFields);
                } else {
                    event.addValue(uuid, CDC_EVENT, true);
                    event.addValue(uuid, CDC_FIELDS, changedFields);
                }
                metrics.incrementMetric(Metric.PROCESSED);
                return event;
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw new StreamsException(String.format("Failed CDC processing for event UUID [ %s ]", event.getUUID().toString()), e);
        }
        metrics.incrementMetric(Metric.PROCESSED);
        return (!dropDuplicates) ? event : null;
    }

    /**
     * Apply Change Data Capture (CDC) algorithm to compare two StreamEvents and identify changed fields.
     *
     * @param orignalEvent The original StreamEvent before any modification.
     * @param event The modified StreamEvent.
     * @param cdc A AtomicBoolean flag to indicate if the CDC algorithm is applied and changes are detected.
     * @return A Map containing the changed fields and their corresponding previous and current values.
     */
    private Map<String, Tuple<Object, Object>> applyCDC(final StreamEvent orignalEvent,final StreamEvent event, AtomicBoolean cdc){
        Map<String, Tuple<Object, Object>> changedFields = new HashMap<>();
        localState.computeIfPresent(event.getValue(keyField), (keyObject, prevStreamEvent) -> {
                    // Iterate in parallel over fields
                    trackingFields.parallelStream().forEach(field -> {
                                // Find value
                                List<String> fieldPath = ObjectCascadeSearchHelper.prepareSearchPath(field, pathSeparator);
                                Object previousValue = ObjectCascadeSearchHelper.findValue(prevStreamEvent, fieldPath);
                                Object currenValue = ObjectCascadeSearchHelper.findValue(orignalEvent, fieldPath);

                                if (!compare(previousValue, currenValue)) {
                                    cdc.set(true);
                                    changedFields.put(field, new Tuple<>(previousValue, currenValue));
                                }
                            }
                    );
                    return orignalEvent;
                }
        );
        localState.put(orignalEvent.getValue(keyField), orignalEvent);
        return changedFields;
    }

    /**
     * Creates a Change Data Capture (CDC) record based on the original StreamEvent and the map of changed records.
     *
     * @param orig The original StreamEvent before any modification.
     * @param cdcRecords A map containing the changed fields and their corresponding previous and current values.
     * @return The StreamEvent representing the CDC record.
     */
    private StreamEvent createdCDCRecord(StreamEvent orig, Map<String, Tuple<Object, Object>> cdcRecords) {
        StreamEvent cdcEvent = new StreamEvent(orig.getEventType(), orig.getEventTime(), orig.getIngestTime());
        cdcEvent.setSubType(orig.getSubType());
        cdcEvent.addValue(uuid, CDC_EVENT, true);
        cdcRecords.forEach((key, o) -> cdcEvent.addValue(uuid, key, o.getY()));
        return cdcEvent;
    }

    /**
     *
     * Compares two objects using their equals() method.
     *
     * @param o1 The first object to compare.
     * @param o2 The second object to compare.
     * @return {@code true} if the two objects are equal, {@code false} otherwise.
     */
    private boolean compare(Object o1, Object o2) {
        return o1.equals(o2);
    }

    public String getKeyField() {
        return keyField;
    }

    @JsonProperty(value = "key field", required = true)
    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }


    public List<String> getTrackingFields() {
        return trackingFields;
    }
    @JsonProperty(value = "tracking fields", required = true)
    public void setTrackingFields(List<String> trackingFields) {
        this.trackingFields = trackingFields;
    }

    public char getPathSeparator() {
        return pathSeparator;
    }

    @JsonProperty(value = "path notation")
    public void setPathSeparator(char pathSeparator) {
        this.pathSeparator = pathSeparator;
    }

    public boolean isEmitOnlyCDC() {
        return emitOnlyCDC;
    }

    @JsonProperty(value = "emit only changes")
    public void setEmitOnlyCDC(boolean emitOnlyCDC) {
        this.emitOnlyCDC = emitOnlyCDC;
    }

    public boolean isDropDuplicates() {
        return dropDuplicates;
    }

    @JsonProperty(value = "drop duplicates")
    public void setDropDuplicates(boolean dropDuplicates) {
        this.dropDuplicates = dropDuplicates;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (keyField == null || keyField.isBlank()) {
            throw new InvalidSpecificationException("key field must be provided");
        }

        if (trackingFields == null || trackingFields.isEmpty()) {
            throw new InvalidSpecificationException("trackingFields must be provided");
        }
    }
}