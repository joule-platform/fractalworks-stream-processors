/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.cdc;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 * ChangeDataCaptureProcessor tests for single and multiple field changes
 *
 * @author Lyndon Adams
 */
public class ChangeDataCaptureProcessorEmitCDCEventOnlyTest {

    @Test
    public void dropDuplicateEventsUsingSingleFieldCDCTest() throws StreamsException {
        List<String> trackingFields = Arrays.asList("price");

        ChangeDataCaptureProcessor processor = new ChangeDataCaptureProcessor();
        processor.setKeyField("symbol");
        processor.setTrackingFields(trackingFields);
        processor.setCloneEvent(false);
        processor.setEmitOnlyCDC(true);

        StreamEvent evt1 = new StreamEvent("market", 10000);
        evt1.addValue("symbol", "IBM");
        evt1.addValue("price", 100.00d);

        StreamEvent response = processor.apply(evt1, null);
        assertNull( response);

        // Apply same event again
        response = processor.apply(evt1, null);
        assertNull( response);
    }

    @Test
    public void dropDuplicateEventsUsingMultipleFieldCDCTest() throws StreamsException {
        List<String> trackingFields = Arrays.asList("price", "vol", "risk");

        ChangeDataCaptureProcessor processor = new ChangeDataCaptureProcessor();
        processor.setKeyField("symbol");
        processor.setTrackingFields(trackingFields);
        processor.setCloneEvent(true);
        processor.setEmitOnlyCDC(true);

        StreamEvent evt1 = new StreamEvent("market", 10000);
        evt1.addValue("symbol", "IBM");
        evt1.addValue("price", 100.00d);
        evt1.addValue("vol", 10000L);
        evt1.addValue("risk", 0.01d);

        StreamEvent response = processor.apply(evt1, null);
        assertNull( response);

        // Apply same event again
        response = processor.apply(evt1, null);
        assertNull( response);
    }

    @Test
    public void simpleSingleFieldCDCTest() throws StreamsException {

        List<String> trackingFields = Arrays.asList("price");

        ChangeDataCaptureProcessor processor = new ChangeDataCaptureProcessor();
        processor.setKeyField("symbol");
        processor.setTrackingFields(trackingFields);
        processor.setCloneEvent(false);
        processor.setEmitOnlyCDC(true);

        StreamEvent evt1 = new StreamEvent("market", 10000);
        evt1.addValue("symbol", "IBM");
        evt1.addValue("price", 100.00d);

        StreamEvent response = processor.apply(evt1, null);
        assertNull( response);

        StreamEvent evt2 = new StreamEvent("market", 20000);
        evt2.addValue("symbol", "IBM");
        evt2.addValue("price", 110.00d);
        response = processor.apply(evt2, null);
        assertEquals(true, response.contains(ChangeDataCaptureProcessor.CDC_EVENT));

        Double price = (Double) response.getValue("price");
        assertNotSame(price, evt1.getValue("price"));
    }


    @Test
    public void simpleSingleFieldCDCTestUsingClonedEvent() throws StreamsException {
        List<String> trackingFields = Arrays.asList("price");

        ChangeDataCaptureProcessor processor = new ChangeDataCaptureProcessor();
        processor.setKeyField("symbol");
        processor.setTrackingFields(trackingFields);
        processor.setCloneEvent(true);
        processor.setEmitOnlyCDC(true);

        StreamEvent evt1 = new StreamEvent("market", 10000);
        evt1.addValue("symbol", "IBM");
        evt1.addValue("price", 100.00d);

        StreamEvent response = processor.apply(evt1, null);
        assertNull( response);

        StreamEvent evt2 = new StreamEvent("market", 20000);
        evt2.addValue("symbol", "IBM");
        evt2.addValue("price", 110.00d);
        response = processor.apply(evt2, null);
        assertEquals(true, response.contains(ChangeDataCaptureProcessor.CDC_EVENT));

        Double price = (Double) response.getValue("price");
        assertNotSame(price , evt1.getValue("price"));
    }

    @Test
    public void multipleFieldCDCTest() throws StreamsException {

        List<String> trackingFields = Arrays.asList("price", "vol", "risk");

        ChangeDataCaptureProcessor processor = new ChangeDataCaptureProcessor();
        processor.setKeyField("symbol");
        processor.setTrackingFields(trackingFields);
        processor.setEmitOnlyCDC(true);

        StreamEvent evt1 = new StreamEvent("market", 10000);
        evt1.addValue("symbol", "IBM");
        evt1.addValue("price", 100.00d);
        evt1.addValue("vol", 10000L);
        evt1.addValue("risk", 0.01d);
        evt1.addValue("description", "IMB Common Stock");

        StreamEvent response = processor.apply(evt1, null);
        assertNull( response);

        StreamEvent evt2 = new StreamEvent("market", 20000);
        evt2.addValue("symbol", "IBM");
        evt2.addValue("price", 110.00d);
        evt2.addValue("vol", 20000L);
        evt2.addValue("risk", 0.61d);
        evt2.addValue("description", "IMB Common Stock");
        response = processor.apply(evt2, null);
        assertEquals(true, response.contains(ChangeDataCaptureProcessor.CDC_EVENT) );

        Double price = (Double) response.getValue("price");
        assertNotSame(price , evt1.getValue("price"));

        Long vol = (Long) response.getValue("vol");
        assertNotSame(vol , evt1.getValue("vol"));

        Double risk = (Double) response.getValue("risk");
        assertNotSame(risk , evt1.getValue("risk"));

        assertTrue(!response.contains("description"));
    }

}
