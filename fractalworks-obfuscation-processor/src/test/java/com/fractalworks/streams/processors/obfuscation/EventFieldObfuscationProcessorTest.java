/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.obfuscation.types.*;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

/**
 * Test EventFieldObfuscateProcessor
 *
 * @author Lyndon Adams
 */
public class EventFieldObfuscationProcessorTest {

    @Test
    public void simpleMaskingTest() throws StreamsException {

        MaskingObfuscationType creditcardMasker = new MaskingObfuscationType();
        creditcardMasker.setMaskingPattern("XXXX XXXX XXXX 1234");

        MaskingObfuscationType nameMasker = new MaskingObfuscationType();
        nameMasker.setApplyAll(true);

        RSAEncryptionType encrypt = new RSAEncryptionType();
        encrypt.setKeyLocation("/tmp");

        Map<String, ObfuscationType<?>> fieldMap = new HashMap<>();
        fieldMap.put("creditcard", creditcardMasker);
        fieldMap.put("expireDate", new DateVarianceObfuscationType());
        fieldMap.put("balance", new NumberVarianceObfuscationType());
        fieldMap.put("name", nameMasker);
        fieldMap.put("address", encrypt);

        StreamEvent event = new StreamEvent();
        event.addValue("creditcard", "1234 5678 9012 9299");
        event.addValue("expireDate", new Date());
        event.addValue("balance", 1000.00d);
        event.addValue("name", "Lyndon Adams");
        event.addValue("address", "1 Green Lane, Southampton, SO12 7UK");
        final Map<String, Object> orgDict = event.getDictionary();


        EventFieldObfuscationProcessor processor = new EventFieldObfuscationProcessor();
        processor.setFields(fieldMap);
        processor.setCloneEvent(true);
        processor.initialize(null);

        StreamEvent evt = processor.apply(event, null);
        System.out.println("evt --> " + evt);
        for (Map.Entry<String, Object> e : evt.getDictionary().entrySet()) {
            assertNotSame(orgDict.get(e.getKey()), equals(e.getValue() ) );
        }

    }

    @Test
    public void dslEventFieldObfuscationProcessorTest() throws JsonProcessingException {

        MaskingObfuscationType creditcardMasker = new MaskingObfuscationType();
        creditcardMasker.setMaskingPattern("XXXX XXXX XXXX 1234");

        MaskingObfuscationType nameMasker = new MaskingObfuscationType();
        nameMasker.setApplyAll(true);

        RSAEncryptionType encrypt = new RSAEncryptionType();
        encrypt.setKeyLocation("/tmp");

        Map<String, ObfuscationType<?>> fieldMap = new HashMap<>();
        fieldMap.put("creditcard", creditcardMasker);
        fieldMap.put("expireDate", new DateVarianceObfuscationType());
        fieldMap.put("balance", new NumberVarianceObfuscationType());
        fieldMap.put("name", nameMasker);
        fieldMap.put("address", encrypt);
        fieldMap.put("salary", new RedactionType());

        EventFieldObfuscationProcessor processor = new EventFieldObfuscationProcessor();
        processor.setFields(fieldMap);
        processor.setCloneEvent(true);

        ObjectMapper mapper = SystemSymbolTable.getInstance().getSystemObjectMapper();
        String dsl = mapper.writeValueAsString(processor);
        System.out.println(dsl);

        assertNotNull(dsl);
    }

    @Test
    public void validObfuscationProcessorYamlTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("streams/dsl/valid/validDSLEventFieldObfuscationProcessor.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventFieldObfuscationProcessor processor = readMapper.readValue(file, EventFieldObfuscationProcessor.class);

        assertNotNull(processor);
    }
}