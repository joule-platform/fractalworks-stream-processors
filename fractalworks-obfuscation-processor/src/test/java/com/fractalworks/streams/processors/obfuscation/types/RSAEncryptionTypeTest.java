/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

/**
 * Tests for the EncryptionObfuscationType class for encryption and decryption
 *
 * @author Lyndon Adams
 */
public class RSAEncryptionTypeTest {

    @Test
    public void defaultEncryptionTest() throws ObfuscationException {

        String originalStr = "Lyndon Adams";

        // Encrypt mode
        RSAEncryptionType encrypt = new RSAEncryptionType();
        encrypt.setKeyBitSize(2048);
        encrypt.setKeyLocation("/tmp");

        String encryptStr = null;
        for(int i=0; i< 10; i++) encryptStr = encrypt.obfuscate(originalStr);
        assertNotSame(originalStr,encryptStr);

        // Decrypt mode
        RSAEncryptionType decrypt = new RSAEncryptionType();
        decrypt.setRotateKeys(false);
        decrypt.setKeyBitSize(2048);
        decrypt.setKeyLocation("/tmp");
        decrypt.setDecrypt(true);
        String decryptStr = decrypt.obfuscate(encryptStr);
        assertEquals(originalStr,decryptStr);
    }

    @Test
    public void autoIVEncryptionTest() throws ObfuscationException {

        String originalStr = "Lyndon Adams";

        // Encrypt mode
        RSAEncryptionType encrypt = new RSAEncryptionType();
        encrypt.setKeyBitSize(2048);
        encrypt.setKeyLocation("/tmp");
        String encryptStr = null;
        for(int i=0; i< 10; i++) encryptStr = encrypt.obfuscate(originalStr);
        assertNotSame(originalStr,encryptStr);

        //byte[] iv = encrypt.getIv();

        // Decrypt mode
        RSAEncryptionType decrypt = new RSAEncryptionType();
        decrypt.setRotateKeys(false);
        decrypt.setKeyBitSize(2048);
        decrypt.setKeyLocation("/tmp");
        decrypt.setDecrypt(true);

        // Do i really need thr IV ?
        //decrypt.setIv(iv);
        String decryptStr = decrypt.obfuscate(encryptStr);
        assertEquals(originalStr,decryptStr);
    }
}