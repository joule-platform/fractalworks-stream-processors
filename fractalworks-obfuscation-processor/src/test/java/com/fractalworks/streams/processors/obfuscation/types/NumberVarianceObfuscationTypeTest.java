/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Number obfuscation tests
 *
 * @author Lyndon Adams
 */
public class NumberVarianceObfuscationTypeTest {

    static public double variance = 0.35d;

    @Test
    public void obfuscateDouble() throws ObfuscationException {
        NumberVarianceObfuscationType type = new NumberVarianceObfuscationType();
        type.setVariance(variance);
        double originalValue = 1.0;
        Number newValue = type.obfuscate(originalValue);
        assertTrue(newValue instanceof Double);
        assertNotSame(newValue.doubleValue(), originalValue);
    }

    @Test
    public void obfuscateFloat() throws ObfuscationException {
        NumberVarianceObfuscationType type = new NumberVarianceObfuscationType();
        type.setVariance(variance);
        float originalValue = 1.0f;
        Number newValue = type.obfuscate(originalValue);
        assertTrue(newValue instanceof Float);
        assertNotSame(newValue.floatValue(), originalValue);
    }


    @Test
    public void obfuscateLong() throws ObfuscationException {
        NumberVarianceObfuscationType type = new NumberVarianceObfuscationType();
        type.setVariance(variance);
        long originalValue = 100L;
        Number newValue = type.obfuscate(originalValue);
        assertTrue(newValue instanceof Long);
        assertNotSame(newValue.longValue(), originalValue);
    }

    @Test
    public void obfuscateInt() throws ObfuscationException {
        NumberVarianceObfuscationType type = new NumberVarianceObfuscationType();
        type.setVariance(variance);
        int originalValue = 100;
        Number newValue = type.obfuscate(originalValue);
        assertTrue(newValue instanceof Integer);
        assertNotSame(newValue.intValue(), originalValue);
    }


}
