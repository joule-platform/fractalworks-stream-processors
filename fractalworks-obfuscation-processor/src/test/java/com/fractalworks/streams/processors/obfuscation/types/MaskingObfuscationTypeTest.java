/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Masking obfuscation tests using patterns
 *
 * @author Lyndon Adams
 */
public class MaskingObfuscationTypeTest {

    static public double variance = 0.35d;

    @Test
    public void maskAll() throws ObfuscationException {
        MaskingObfuscationType type = new MaskingObfuscationType();
        type.setApplyAll(true);
        type.setMask('*');

        String originalValue = "0123456789";
        String newValue = type.obfuscate(originalValue);
        assertNotEquals(newValue, originalValue);
    }


    @Test
    public void maskByPattern1() throws ObfuscationException {
        String originalValue = "0123 4567 8956 7001";
        String maskingPattern = "XXXX XXXX XXXX 7001";

        MaskingObfuscationType type = new MaskingObfuscationType();
        type.setMaskingPattern(maskingPattern);
        type.setMask('*');

        String newValue = type.obfuscate(originalValue);
        assertNotEquals(newValue, originalValue);
    }

    @Test
    public void maskByPattern2() throws ObfuscationException {
        String originalValue = "0123 4567 8956 7001";
        String maskingPattern = "XXXX 0000 XXXX 0000";

        MaskingObfuscationType type = new MaskingObfuscationType();
        type.setMaskingPattern(maskingPattern);
        type.setMask('*');

        String newValue = type.obfuscate(originalValue);
        assertNotEquals(newValue, originalValue);
    }
}
