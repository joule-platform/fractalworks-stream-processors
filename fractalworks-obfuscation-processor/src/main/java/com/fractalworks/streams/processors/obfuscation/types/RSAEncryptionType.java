/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationType;
import com.google.common.base.Objects;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

/**
 * RSA Encryption type using generated keys
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "encryption")
public class RSAEncryptionType implements ObfuscationType<String> {

    @JsonIgnore
    private static final String AES = "AES";

    @JsonIgnore
    private static final String RSA = "RSA";

    @JsonIgnore
    private byte[] iv;
    private boolean dynamicIV = false;

    @JsonIgnore
    private static final String PATH_LITERAL = "%s/%s";

    @JsonIgnore
    private SecretKey secKey;

    @JsonIgnore
    private Cipher aesCipher;

    private int aesBitSize = 128;

    // AES block size for IV
    @JsonIgnore
    private int blockSize = 16;

    @JsonIgnore
    private Cipher keyCipher;
    private int keyBitSize = 2048;
    @JsonIgnore
    private KeyPair keyPair;
    private boolean decrypt = false;
    private boolean rotateKeys = true;
    private String keyLocation;

    // Control variables
    @JsonIgnore
    private boolean initCompleted = false;
    private boolean aesCipherInitilized = false;

    private static final String ERROR_DURING_INITIALISING = "Error while initialising";

    private static final String PRIVATE_KEY_TYPE = "private.key";
    private static final String PUBLIC_KEY_TYPE = "public.key";

    public RSAEncryptionType() {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Initializes the encryption/decryption process by setting up key directories,
     * loading/storing keys, and initializing cipher instances.
     *
     * @throws ObfuscationException if an error occurs during initialization
     */
    private void init() throws ObfuscationException {
        try {
            File keyDir = new File(keyLocation);
            FileUtils.forceMkdir( keyDir );

            if (rotateKeys) {
                rotateKeys();
            } else {
                keyPair = restoreKeyPair();
            }

            aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            keyCipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");

            // Encryption
            if (!decrypt) {
                initEncyptionSecretKey();
                if(!dynamicIV) {
                    buildAESCiper();
                }
            } else {
                keyCipher.init(Cipher.PRIVATE_KEY, keyPair.getPrivate());
                secKey = restoreAESKey("aes.key", keyCipher);
            }
            initCompleted = true;
        } catch (ObfuscationException | IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeySpecException | InvalidKeyException | ClassNotFoundException ex) {
            throw new ObfuscationException(ERROR_DURING_INITIALISING, ex);
        }
    }

    /**
     * Initializes the encryption secret key.
     *
     * @throws ObfuscationException if an error occurs during initialization
     */
    private void initEncyptionSecretKey()throws ObfuscationException {
        try {
            // AES Cipher for value encryption for large data
            KeyGenerator generator = KeyGenerator.getInstance(AES);
            generator.init(aesBitSize); // The AES key size in number of bits
            secKey = generator.generateKey();

            // AES key encryption using RSA
            keyCipher.init(Cipher.PUBLIC_KEY, keyPair.getPublic());

            // Encrypt AES secKey and save
            saveAESKey("aes.key", keyCipher.doFinal(secKey.getEncoded()));
        } catch (Exception ex) {
            throw new ObfuscationException("Error while initialising encryption", ex);
        }
    }

    /**
     * Builds an AES Cipher for encryption.
     *
     * @throws ObfuscationException if an error occurs during cipher initialization
     */
    private void buildAESCiper() throws ObfuscationException {
        try {
            iv = createInitializationVector(aesCipher.getBlockSize());
            AlgorithmParameterSpec gcmParameterSpec = new IvParameterSpec(iv);
            aesCipher.init(Cipher.ENCRYPT_MODE, secKey, gcmParameterSpec);
        } catch (Exception ex) {
            throw new ObfuscationException(ERROR_DURING_INITIALISING, ex);
        }
    }

    /**
     * Initializes the decryption process.
     *
     * @param iv the initialization vector used for decryption
     * @throws ObfuscationException if an error occurs during initialization
     */
    private void initDecryption(byte[] iv) throws ObfuscationException {
        try{
            AlgorithmParameterSpec gcmParameterSpec = new IvParameterSpec(iv);
            aesCipher.init(Cipher.DECRYPT_MODE, secKey, gcmParameterSpec);
            aesCipherInitilized = true;
        } catch (Exception ex) {
            throw new ObfuscationException(ERROR_DURING_INITIALISING, ex);
        }
    }

    @Override
    public String obfuscate(Object value) throws ObfuscationException {
        if (!initCompleted) init();
        if (value instanceof String strValue && !strValue.isEmpty()) {
            return (!decrypt) ? encrypt(strValue) : decrypt(strValue);
        } else
            throw new ObfuscationException("Passed data type is not supported for this function.");
    }

    /**
     * Encrypts a value using the AES encryption algorithm.
     *
     * @param value the value to encrypt
     * @return the encrypted value
     * @throws ObfuscationException if an error occurs during encryption
     */
    private String encrypt(String value) throws ObfuscationException {
        byte[] ciphertext;
        try {
            ciphertext = aesCipher.doFinal(value.getBytes(StandardCharsets.UTF_8));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new ObfuscationException(e);
        }
        var localIV = (dynamicIV) ? createInitializationVector(aesCipher.getBlockSize()) : iv;
        byte[] ivAndciphertext = new byte[localIV.length + ciphertext.length];
        System.arraycopy(localIV, 0, ivAndciphertext, 0, localIV.length);
        System.arraycopy(ciphertext, 0, ivAndciphertext, localIV.length, ciphertext.length);

        return Base64.getEncoder().encodeToString(ivAndciphertext);
    }

    /**
     * Decrypts a value using the AES encryption algorithm.
     *
     * @param value the value to decrypt
     * @return the decrypted value
     * @throws ObfuscationException if an error occurs during decryption
     */
    private String decrypt(String value) throws ObfuscationException {
        byte[] ivAndCipertext = Base64.getDecoder().decode(value.getBytes(StandardCharsets.UTF_8));
        byte[] localIv = new byte[aesCipher.getBlockSize()];
        byte[] ciphertext = new byte[ivAndCipertext.length - localIv.length];

        System.arraycopy(ivAndCipertext, 0, localIv, 0, localIv.length);
        System.arraycopy(ivAndCipertext, localIv.length, ciphertext, 0, ciphertext.length);

        if (!dynamicIV && !aesCipherInitilized) {
            initDecryption(localIv);
        } else {
            initDecryption(localIv);
        }
        byte[] cleanBytes;
        try {
            cleanBytes = aesCipher.doFinal(ciphertext);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new ObfuscationException(e);
        }
        return new String(cleanBytes, StandardCharsets.UTF_8);
    }


    /**
     * Create RSA public and private keys
     *
     * @return valid RSA public and private <code>KeyPair</code>
     */
    private KeyPair createKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA);
        kpg.initialize(keyBitSize);
        return kpg.genKeyPair();
    }

    /**
     * Restore <code>KeyPair</code> from storage
     *
     * @return
     */
    private KeyPair restoreKeyPair() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, ClassNotFoundException {
        KeyFactory fact = KeyFactory.getInstance(RSA);

        // Load public key
        Tuple<BigInteger, BigInteger> epair = loadFile(PUBLIC_KEY_TYPE);
        PublicKey pbk = fact.generatePublic(new RSAPublicKeySpec(epair.getX(), epair.getY()));

        // Load private key
        epair = loadFile(PRIVATE_KEY_TYPE);
        PrivateKey pKey = fact.generatePrivate(new RSAPrivateKeySpec(epair.getX(), epair.getY()));

        return new KeyPair(pbk, pKey);
    }

    /**
     * Save <code>KeyPair</code> to user location
     */
    private void saveKeyPair() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory fact = KeyFactory.getInstance(RSA);
        RSAPublicKeySpec pub = fact.getKeySpec(keyPair.getPublic(), RSAPublicKeySpec.class);
        RSAPrivateKeySpec priv = fact.getKeySpec(keyPair.getPrivate(), RSAPrivateKeySpec.class);

        saveRSAKey(PUBLIC_KEY_TYPE, pub.getModulus(), pub.getPublicExponent());
        saveRSAKey(PRIVATE_KEY_TYPE, priv.getModulus(), priv.getPrivateExponent());
    }

    /**
     * Save key file
     *
     * @param filename
     * @param mod
     * @param exp
     * @throws IOException
     */
    private void saveRSAKey(String filename, BigInteger mod, BigInteger exp) throws IOException {
        OutputStream output = new FileOutputStream(String.format(PATH_LITERAL, keyLocation, filename));
        try (ObjectOutputStream os = new ObjectOutputStream(output)) {
            os.writeObject(mod);
            os.writeObject(exp);
        }
    }

    /**
     * Load key file
     *
     * @param filename
     * @return
     * @throws IOException
     */
    private Tuple<BigInteger, BigInteger> loadFile(String filename) throws IOException, ClassNotFoundException {
        InputStream input = new FileInputStream(String.format(PATH_LITERAL, keyLocation, filename));
        Tuple<BigInteger, BigInteger> tuple;
        try (ObjectInputStream is = new ObjectInputStream(input)) {
            BigInteger mod = (BigInteger) is.readObject();
            BigInteger exp = (BigInteger) is.readObject();
            tuple = new Tuple<>(mod, exp);
        }
        return tuple;
    }

    /**
     * Encrypt AES secKey and save
     *
     * @param filename
     * @param encryptedKey
     */
    private void saveAESKey(String filename, byte[] encryptedKey) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(String.format(PATH_LITERAL, keyLocation, filename))) {
            outputStream.write(encryptedKey);
        }
    }

    /**
     * Restore AES encryption key
     *
     * @return
     */
    private SecretKey restoreAESKey(String filename, Cipher keyDecrypter) throws ObfuscationException {
        try {
            File keyFile = new File(String.format(PATH_LITERAL, keyLocation, filename));

            byte[] secKeyString;
            try (InputStream is = new FileInputStream(keyFile)) {
                int keyLength = (int) keyFile.length();
                secKeyString = new byte[keyLength];

                int bytesRead = is.read(secKeyString, 0, keyLength);
                while (bytesRead != -1) {
                    bytesRead = is.read(secKeyString, 0, keyLength);
                }
            }

            // Decrypt key using keyDecrypter and return
            byte[] decryptedKey = keyDecrypter.doFinal(secKeyString);
            return new SecretKeySpec(decryptedKey, 0, decryptedKey.length, AES);
        }catch(IOException | IllegalBlockSizeException | BadPaddingException ex) {
           throw new ObfuscationException("Failed to restore AES key", ex);
        }
    }

    /**
     * Refresh keys and save
     *
     * @return
     * @throws IOException
     */
    public void rotateKeys() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        try {
            keyPair = createKeyPair();
            saveKeyPair();
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            keyPair = null;
            throw e;
        }
    }

    private byte[] createInitializationVector(int blockSize){
        SecureRandom randomSecureRandom = new SecureRandom();
        byte[] vector = new byte[ blockSize ];
        randomSecureRandom.nextBytes(vector);
        return vector;
    }

    @JsonProperty(value = "dynamic iv")
    public void setDynamicIV(boolean dynamicIV) {
        this.dynamicIV = dynamicIV;
    }

    public int getKeyBitSize() {
        return keyBitSize;
    }

    @JsonProperty(value = "rsa bit length")
    public void setKeyBitSize(int keyBitSize) {
        this.keyBitSize = keyBitSize;
    }

    public int getAesBitSize() {
        return aesBitSize;
    }

    @JsonProperty(value = "aes bit length")
    public void setAesBitSize(int aesBitSize) {
        this.aesBitSize = aesBitSize;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    @JsonProperty(value = "decrypt")
    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public String getKeyLocation() {
        return keyLocation;
    }

    @JsonProperty(required = true, value = "key location")
    public void setKeyLocation(String keyLocation) {
        this.keyLocation = keyLocation;
    }

    public boolean isRotateKeys() {
        return rotateKeys;
    }

    @JsonProperty(required = false, value = "rotate keys")
    public void setRotateKeys(boolean rotateKeys) {
        this.rotateKeys = rotateKeys;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( aesBitSize!=128 || aesBitSize!=192 || aesBitSize!=256 ){
            throw new InvalidSpecificationException("Invalid aes bit length. Valid sizes {128, 192, 256}");
        }

        if( keyLocation == null || keyLocation.isBlank()){
            throw new InvalidSpecificationException("key location cannot be empty");
        }

        if( keyBitSize < 2048){
            throw new InvalidSpecificationException("key bit length provided is less than the NIST recommended minimum of 2048 bits");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RSAEncryptionType that = (RSAEncryptionType) o;
        return aesBitSize == that.aesBitSize && keyBitSize == that.keyBitSize && decrypt == that.decrypt && Objects.equal(keyLocation, that.keyLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(aesBitSize, keyBitSize, decrypt, keyLocation);
    }
}
