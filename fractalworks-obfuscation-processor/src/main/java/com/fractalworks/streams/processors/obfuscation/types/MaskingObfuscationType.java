/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationType;
import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Masking obfuscater using either a masking pattern or masking all string tokens. Default mask is set to '*'
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "masking")
public class MaskingObfuscationType implements ObfuscationType<String> {

    private String maskingPattern;
    private char mask = '*';
    private boolean applyAll = false;

    @JsonIgnore
    private Integer[] maskingIndex;

    public MaskingObfuscationType() {
        // Required default constructor
    }

    @Override
    public String obfuscate(Object value) throws ObfuscationException {
        if (value instanceof String strValue) {
            if (!strValue.isEmpty()) {
                char[] tokens = strValue.toCharArray();
                if (applyAll) {
                    Arrays.fill(tokens, 0, tokens.length, mask);
                } else {
                    maskByIndex(tokens);
                }
                return (new String(tokens));
            }
            return strValue;
        } else
            throw new ObfuscationException("Passed data type is not supported for this function.");
    }

    /**
     * Masks specific characters in an array by their index.
     *
     * @param tokens the character array containing the tokens to be masked
     */
    private void maskByIndex(char[] tokens){
        if (maskingIndex == null) buildMaskingIndex();
        for (Integer idx : maskingIndex) {
            tokens[idx] = mask;
        }
    }

    private void buildMaskingIndex() {
        List<Integer> maskPositions = new ArrayList<>();
        char[] tokens = maskingPattern.toCharArray();
        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i] == 'X') {
                maskPositions.add(i);
            }
        }
        maskingIndex = new Integer[maskPositions.size()];
        maskingIndex = maskPositions.toArray(maskingIndex);
    }

    public String getMaskingPattern() {
        return maskingPattern;
    }

    @JsonProperty(value = "pattern")
    public void setMaskingPattern(String maskingPattern) {
        this.maskingPattern = maskingPattern;
    }

    public char getMask() {
        return mask;
    }

    @JsonProperty(value = "mask")
    public void setMask(char mask) {
        this.mask = mask;
    }

    public boolean isApplyAll() {
        return applyAll;
    }

    @JsonProperty(value = "applyAll")
    public void setApplyAll(boolean applyAll) {
        this.applyAll = applyAll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaskingObfuscationType that = (MaskingObfuscationType) o;
        return mask == that.mask && applyAll == that.applyAll && Objects.equal(maskingPattern, that.maskingPattern) ;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( maskingPattern == null || maskingPattern.isBlank()){
            throw new InvalidSpecificationException("pattern must be provided");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(maskingPattern, mask, applyAll);
    }
}
