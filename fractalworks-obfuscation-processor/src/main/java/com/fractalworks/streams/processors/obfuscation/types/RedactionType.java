/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationType;
import com.google.common.base.Objects;

/**
 * Null obfuscation type which returns a Null native type
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "redact")
public class RedactionType implements ObfuscationType<String> {

    private boolean asNull = false;

    public RedactionType() {
        // Required default constructor
    }

    public boolean isAsNull() {
        return asNull;
    }

    @JsonProperty(value = "as null")
    public void setAsNull(boolean asNull) {
        this.asNull = asNull;
    }

    @Override
    public String obfuscate(Object value) throws ObfuscationException {
        return (asNull) ? null : "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedactionType that = (RedactionType) o;
        return asNull == that.asNull;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(asNull);
    }
}
