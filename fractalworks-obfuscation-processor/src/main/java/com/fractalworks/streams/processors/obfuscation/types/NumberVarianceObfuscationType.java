/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationType;
import com.google.common.base.Objects;

import java.security.SecureRandom;

/**
 * Number obfuscater using a variance boundary for a positive / minus change. Default variance set to 15%
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "number bucketing")
public class NumberVarianceObfuscationType implements ObfuscationType<Number> {

    private double variance = 0.15d;

    @JsonIgnore
    private final SecureRandom randomGenerator = new SecureRandom();

    public NumberVarianceObfuscationType() {
        // Required default constructor
    }

    @Override
    public Number obfuscate(Object value) throws ObfuscationException {
        if(value instanceof Number numberValue) {
            double originalValue = numberValue.doubleValue();

            double delta;
            double limit = (originalValue * variance);
            do {
                delta = (randomGenerator.nextLong() % limit);
            } while (delta == 0.0);

            var newValue = Double.valueOf((randomGenerator.nextBoolean()) ? originalValue + delta : originalValue - delta);

            Number obfuscatedValue = null;
            if (value instanceof Float) {
                obfuscatedValue = newValue.floatValue();
            } else if (value instanceof Double) {
                obfuscatedValue = newValue;
            } else if (value instanceof Integer) {
                obfuscatedValue = newValue.intValue();
            } else if (value instanceof Long) {
                obfuscatedValue = newValue.longValue();
            }
            return obfuscatedValue;
        } else
            throw new ObfuscationException("Passed data type is not supported for this function.");
    }

    public double getVariance() {
        return variance;
    }

    @JsonProperty(value = "variance")
    public void setVariance(double variance) {
        this.variance = variance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberVarianceObfuscationType that = (NumberVarianceObfuscationType) o;
        return Double.compare(that.variance, variance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(variance);
    }
}
