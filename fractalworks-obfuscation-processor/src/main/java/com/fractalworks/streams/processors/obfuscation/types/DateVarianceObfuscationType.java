/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationException;
import com.fractalworks.streams.processors.obfuscation.ObfuscationType;
import com.google.common.base.Objects;
import org.joda.time.DateTime;

import java.security.SecureRandom;


/**
 * Date obfuscater using a variance boundary for a positive / minus change in date and time. Default variance set to 120.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "date bucketing")
public class DateVarianceObfuscationType implements ObfuscationType<DateTime> {

    @JsonIgnore
    private final SecureRandom randomGenerator = new SecureRandom();

    private int variance = 120;

    public DateVarianceObfuscationType() {
        // Required default constructor
    }

    @Override
    public DateTime obfuscate(Object value) throws ObfuscationException {
        if(value instanceof DateTime dateTimeValue){
            int days;
            do {
                days = (randomGenerator.nextInt() % variance);
            } while (days == 0);
            return (randomGenerator.nextBoolean() ? dateTimeValue.plusDays(days).plusMinutes(randomGenerator.nextInt() % variance) : dateTimeValue.minusDays(days).minusMinutes(randomGenerator.nextInt() % variance));
        } else
            throw new ObfuscationException("Passed data type is not supported for this function.");
    }

    public int getVariance() {
        return variance;
    }

    @JsonProperty(value = "variance")
    public void setVariance(int variance) {
        this.variance = variance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateVarianceObfuscationType that = (DateVarianceObfuscationType) o;
        return variance == that.variance && Objects.equal(randomGenerator, that.randomGenerator);
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( variance <=0){
            throw new InvalidSpecificationException("Invalid variance. Must be greater than zero");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(randomGenerator, variance);
    }
}
