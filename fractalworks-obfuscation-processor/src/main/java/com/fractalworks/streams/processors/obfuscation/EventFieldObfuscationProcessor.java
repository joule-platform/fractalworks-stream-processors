/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.obfuscation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import org.joda.time.DateTime;

import java.util.Map;
import java.util.Properties;

/**
 * Basic event filter that removes required fields from an incoming <code>StreamEvent</code>
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "obfuscation")
public class EventFieldObfuscationProcessor extends AbstractProcessor {

    protected Map<String, ObfuscationType<?>> fields;

    public EventFieldObfuscationProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        try {
            for (Map.Entry<String, ObfuscationType<?>> entry : fields.entrySet()) {
                ObfuscationType<?> o = entry.getValue();
                o.initialize();
            }
        } catch(CustomPluginException pluginEx){
            throw new ProcessorException("User plugin failed", pluginEx);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        // Now remove fields
        for (Map.Entry<String, ObfuscationType<?>> e : fields.entrySet()) {
            if (event.contains(e.getKey())) {
                try {
                    var valueToObfuscate = event.getValue(e.getKey());

                    // Convert Date to joda DateTime
                    if (valueToObfuscate instanceof java.util.Date utilDate) {
                        valueToObfuscate = new DateTime(utilDate.getTime());
                    } else if (valueToObfuscate instanceof java.time.LocalDateTime localDate) {
                        var epochTime = localDate.toLocalDate().toEpochDay();
                        valueToObfuscate = new DateTime(epochTime);
                    }
                    var replacementValue = e.getValue().obfuscate(valueToObfuscate);

                    event.replaceValue(uuid, e.getKey(), replacementValue);
                } catch (ObfuscationException ex) {
                    metrics.incrementMetric(Metric.PROCESS_FAILED);
                    logger.error("Unable to obfuscate field", ex);
                }
            }
        }
        metrics.incrementMetric(Metric.PROCESSED);
        return event;
    }

    @JsonProperty(value = "fields", required = true)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setFields(final Map<String, ObfuscationType<?>> fields) {
        this.fields = fields;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (fields == null || fields.isEmpty()) {
            throw new InvalidSpecificationException("fields cannot be null or empty.");
        }

        for(ObfuscationType<?> type : fields.values()){
            type.validate();
        }
    }
}
