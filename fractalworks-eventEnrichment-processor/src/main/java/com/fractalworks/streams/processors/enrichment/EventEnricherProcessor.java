/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.JouleProcessConfig;
import com.fractalworks.streams.processors.enrichment.criteria.MetricQueryCriteriaSpecification;
import com.fractalworks.streams.processors.enrichment.criteria.ReferenceDataQueryCriteriaSpecification;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.StreamsRuntimeException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.fractalworks.streams.sdk.storage.StoreType.KEY_VALUE;
import static java.util.stream.Collectors.groupingBy;

/**
 * Event enricher processor which enriches an event by looking up a primary key within an in-memory key value store.
 * On a successful lookup a value object is placed in to the event
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "enricher")
public class EventEnricherProcessor extends AbstractProcessor {

    private Map<String, ReferenceDataQueryCriteriaSpecification> enrichmentKeyMap;
    private Collection<ReferenceDataQueryCriteriaSpecification> optimiseQueries;
    private Collection<MetricQueryCriteriaSpecification> metricsDBQueries;

    public EventEnricherProcessor() {
        super();
        super.cloneEvent = false;
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);

        // Optimise enrichment queries
        setupQueryProcessing();
    }

    /**
     * Optimise the number of queries to be executed by grouping by target store and unique queries
     *
     * @return
     */
    private void setupQueryProcessing(){
        optimiseQueries = new ArrayList<>();
        var groupedByStoreName = enrichmentKeyMap.values().stream().collect(groupingBy(ReferenceDataQueryCriteriaSpecification::getStoreName));

        // Iterate over distinct store names
        for (Map.Entry<String, List<ReferenceDataQueryCriteriaSpecification>> entry : groupedByStoreName.entrySet()) {
            var store = entry.getKey();
            var queryCriteriaList = entry.getValue();

            switch (store){
                case JouleProcessConfig.JOULE_DB_ACCESS_NAME:
                    optimiseQueries.addAll( configureInternalDatabaseQuery(queryCriteriaList));
                    break;
                case JouleProcessConfig.METRICS_DB_ACCESS_NAME:
                    metricsDBQueries = configureMetricsDatabaseQuery(queryCriteriaList);
                    break;
                default: {
                    var storeApi = (DataStoreQueryInterface<ReferenceDataObject>) dataStores.get(store);
                    if (storeApi.getStoreType().equals(KEY_VALUE)) {
                        optimiseQueries.addAll(optimiseKeyValueStoreEnrichmentCriteriaList(store, storeApi, queryCriteriaList));
                    }
                }
            }
        }
    }

    /**
     * Configure metrics queries
     * @param queryCriteriaList
     * @return list of <code>MetricQueryCriteriaSpecification</code>
     */
    private List<MetricQueryCriteriaSpecification> configureMetricsDatabaseQuery(List<ReferenceDataQueryCriteriaSpecification> queryCriteriaList) {
        var metricQueries = new ArrayList<MetricQueryCriteriaSpecification>();
        queryCriteriaList.forEach(qc -> {
            if (qc.getStoreName().equalsIgnoreCase(JouleProcessConfig.METRICS_DB_ACCESS_NAME)) {
                var mqc = new MetricQueryCriteriaSpecification(qc);
                mqc.setMetricQueryInterface(metricsQueryInterface);
                mqc.initialise();
                metricQueries.add(mqc);
            }
        });
        return metricQueries;
    }

    /**
     * Configure the internal memory database queries
     * @param queryCriteriaList
     * @return
     */
    private List<ReferenceDataQueryCriteriaSpecification> configureInternalDatabaseQuery(List<ReferenceDataQueryCriteriaSpecification> queryCriteriaList){
        queryCriteriaList.forEach(c -> {
            c.setStoreApi(getJouleDBQueryInterface());
            if(!c.isAllAttributes() && !c.isAsObject()) {
                c.addTargetAttributes(c.getTargetFields());
            }
            c.initialise();
        });
        return queryCriteriaList;
    }

    /**
     * Optimise key-value store enrichment criteria to minimize repeated store calls.
     *
     * @param store The name of the store
     * @param storeApi The DataStoreQueryInterface implementation
     * @param criteria The list of enrichment query criteria
     * @return The collection of optimised enrichment query criteria
     */
    private Collection<ReferenceDataQueryCriteriaSpecification> optimiseKeyValueStoreEnrichmentCriteriaList(String store, DataStoreQueryInterface<ReferenceDataObject> storeApi, List<ReferenceDataQueryCriteriaSpecification> criteria){
        Collection<ReferenceDataQueryCriteriaSpecification> optimiseFieldEnrichmentQueries = new ArrayList<>();
        var groupedByUniqueKeys = criteria.stream().collect(groupingBy(e-> Optional.ofNullable((e.getKey()))));
        conflateEnrichmentQueryCriteriaList(groupedByUniqueKeys, storeApi, store,optimiseFieldEnrichmentQueries, false);

        var groupedByQueries = criteria.stream().collect(groupingBy(e -> Optional.ofNullable(e.getQuery())));
        conflateEnrichmentQueryCriteriaList(groupedByQueries, storeApi, store,optimiseFieldEnrichmentQueries, true);
        return optimiseFieldEnrichmentQueries;
    }

    /**
     * Conflate non-unique queries
     *
     * @param groupedEnrichmentQueries
     * @param storeApi
     * @param store
     * @param optimiseFieldEnrichmentQueries
     * @param isQuery
     */
    private void conflateEnrichmentQueryCriteriaList(Map<Optional<String>,List<ReferenceDataQueryCriteriaSpecification>> groupedEnrichmentQueries,
                                                     DataStoreQueryInterface<ReferenceDataObject> storeApi,
                                                     String store,
                                                     Collection<ReferenceDataQueryCriteriaSpecification> optimiseFieldEnrichmentQueries,
                                                     boolean isQuery){

        groupedEnrichmentQueries.forEach((uniqueQueryElement,queryDefinition)->
            uniqueQueryElement.ifPresent(action -> {
                var singleQuery = new ReferenceDataQueryCriteriaSpecification();
                if(isQuery){
                    singleQuery.setQuery(uniqueQueryElement.get());
                } else {
                    singleQuery.setKey(uniqueQueryElement.get());
                }

                singleQuery.setStoreApi(storeApi);
                singleQuery.setStoreName(store);

                AtomicBoolean allAttributes = new AtomicBoolean(false);
                AtomicBoolean asObject = new AtomicBoolean(false);
                AtomicReference<String> asObjectTargetField = new AtomicReference<>();

                queryDefinition.forEach(c -> {
                    if(c.isAsObject() && !asObject.get()){
                        asObject.set(true);
                        asObjectTargetField.set(c.getParentFieldName());
                    } else if(c.isAllAttributes()){
                        allAttributes.set(true);
                    }
                    else if(c.getTargetFields()!=null) {
                        singleQuery.addTargetAttributes(c.getTargetFields());
                    }
                });
                if(asObject.get()){
                    singleQuery.setAsObject(asObject.get());
                    singleQuery.setParentFieldName(asObjectTargetField.get());
                } else if(allAttributes.get()) {
                    singleQuery.setAllAttributes(allAttributes.get());
                    singleQuery.setTargetFields(null);
                }
                singleQuery.initialise();
                optimiseFieldEnrichmentQueries.add(singleQuery);
            })
        );
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        try {
            optimiseQueries.stream().parallel().forEach(c-> {
                try {
                    enrich(c, event);
                } catch (StreamsException e) {
                    throw new StreamsRuntimeException(e);
                }
            });
            if(metricsDBQueries!=null) {
                metricsDBQueries.forEach(c -> enrichEventWithMetrics(c,event));
            }
            metrics.incrementMetric(Metric.PROCESSED);
        }catch(StreamsRuntimeException e){
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw (StreamsException)e.getCause();
        }
        return event;
    }

    /**
     * Enriches a StreamEvent with data based on the given ReferenceDataQueryCriteriaSpecification.
     *
     * @param criteria The ReferenceDataQueryCriteriaSpecification used to execute the enrichment
     * @param event    The StreamEvent to be enriched
     * @throws StreamsException if there is a failure in the enrichment process
     */
    private void enrich(ReferenceDataQueryCriteriaSpecification criteria, StreamEvent event) throws StreamsException {
        try {
            Optional<ReferenceDataObject> response = criteria.executeQuery(event);
            response.ifPresent(dataObject -> {
                if(criteria.isAsObject()){
                    var obj = (dataObject.getData()!=null) ? dataObject.getData() : new HashMap<>(dataObject);
                    event.addValue(uuid, criteria.getParentFieldName(), obj);
                } else if(criteria.isAllAttributes()) {
                    dataObject.keySet().forEach(k -> event.addValue(uuid, k, dataObject.get(k)));
                } else if(criteria.isDefinedAttributes()){
                    criteria.getTargetAttributes().forEach( f -> event.addValue(uuid, f, dataObject.get(f)));
                }
            });
        }catch(Exception e){
            throw new StreamsException(new StreamsException(String.format("Failure in enrichment for %s ", criteria.getKey()), e));
        }
    }

    /**
     * Enrich an event with pre-computed metric data
     * @param event to be enriched
     */
    public StreamEvent enrichEventWithMetrics(MetricQueryCriteriaSpecification mc, StreamEvent event){
        var results = mc.executeQuery(event);
        results.ifPresent(stringObjectMap -> {
            if(mc.isAsObject()) {
                event.addValue(uuid, mc.getParentFieldName(), new HashMap<>(stringObjectMap));
            } else if(mc.isAllAttributes()){
                event.bulkAddValue(uuid, stringObjectMap);
            } else {
                Arrays.stream(mc.getTargetFields()).forEach(field -> event.addValue(field, stringObjectMap.getOrDefault(field, 0)));
            }
        });
        return event;
    }

    @JsonProperty(value = "fields", required = true)
    public void setEnrichmentKeyMap(Map<String, ReferenceDataQueryCriteriaSpecification> enrichmentKeyMap) {
        this.enrichmentKeyMap = enrichmentKeyMap;
        enrichmentKeyMap.forEach((k,m)->  m.setParentFieldName(k));
    }

    public Collection<ReferenceDataQueryCriteriaSpecification> getOptimiseQueries() {
        return optimiseQueries;
    }

    public Collection<MetricQueryCriteriaSpecification> getMetricsDBQueries() {
        return metricsDBQueries;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (enrichmentKeyMap == null || enrichmentKeyMap.isEmpty()) {
            throw new InvalidSpecificationException("enrich must be provided.");
        }

        for(Map.Entry<String,ReferenceDataQueryCriteriaSpecification> e : enrichmentKeyMap.entrySet()){
            var spec = e.getValue();
            // Convert to correct spec type
            if (spec.getStoreName().equalsIgnoreCase(JouleProcessConfig.METRICS_DB_ACCESS_NAME)) {
                spec = new MetricQueryCriteriaSpecification(spec);
                enrichmentKeyMap.replace(e.getKey(), spec);
            }
            spec.validate();
        }
    }

    @Override
    public String toString() {
        return "EventEnricherProcessor{" +
                "enrichmentKeyMap=" + enrichmentKeyMap +
                "} " + super.toString();
    }
}
