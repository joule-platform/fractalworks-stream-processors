/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment.criteria;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.DuplicateQueryException;
import com.fractalworks.streams.core.exceptions.FailedSQLProcessingException;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.JouleProcessConfig;
import com.fractalworks.streams.core.processors.embeddedsql.EmbeddedSQLStore;
import com.fractalworks.streams.sdk.analytics.sql.SQLQueryResult;
import com.fractalworks.streams.sdk.exceptions.StreamsRuntimeException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.StoreType;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import com.fractalworks.streams.sdk.referencedata.specifications.query.QueryInterface;

import java.util.*;
import java.util.stream.Stream;

/**
 * Enrichment query criteria which executes and returns the required
 * enrichment values to be applied to StreamEvent
 *
 * @author Lyndon Adams
 */
public class ReferenceDataQueryCriteriaSpecification extends QueryInterface {

    protected DataStoreQueryInterface<ReferenceDataObject> storeApi;
    protected boolean allAttributes = false;
    protected boolean asObject = false;
    protected List<String> targetAttributes = new ArrayList<>();

    protected String queryId;
    protected String metricFamily;

    public ReferenceDataQueryCriteriaSpecification() {
        super();
    }

    public void initialise(){
        if(allAttributes || asObject){
            targetAttributes  = new ArrayList<>();
        }
        if(storename.equalsIgnoreCase(JouleProcessConfig.JOULE_DB_ACCESS_NAME)){
            try {
                queryId = UUID.randomUUID().toString().intern();
                EmbeddedSQLStore.getInstance().registerQuery(queryId,query,-1);
            } catch (DuplicateQueryException | FailedSQLProcessingException e) {
                throw new StreamsRuntimeException(e);
            }
        }
    }

    public void setStoreApi(DataStoreQueryInterface<ReferenceDataObject> storeApi) {
        this.storeApi = storeApi;
    }

    public Optional<ReferenceDataObject> executeQuery(StreamEvent evt){
        Optional<ReferenceDataObject> response = Optional.empty();
        switch (storeApi.getStoreType()){
            case KEY_VALUE -> response = (key!=null) ? storeApi.queryByKey(evt.getValue(key)) : storeApi.queryByQL(query,queryFields,evt);
            case SQL -> response = storeApi.queryByQL(query,queryFields,evt);
            case JOULE_DB -> {
                var sqlResults = EmbeddedSQLStore.getInstance().query(queryId, Arrays.stream(queryFields).map(evt::getValue).toArray());
                if(sqlResults.isPresent()){
                    for(SQLQueryResult r :sqlResults.get()){
                        response = Optional.of(new ReferenceDataObject(parentFieldName,r));
                    }
                }
            }
            default -> response = Optional.empty();
        }
        return response;
    }

    public boolean isAllAttributes() {
        return allAttributes;
    }

    @JsonProperty(value = "all attributes", required = false)
    public void setAllAttributes(boolean allAttributes) {
        this.allAttributes = allAttributes;
    }

    public boolean isDefinedAttributes() {
        return !targetAttributes.isEmpty();
    }

    public boolean isAsObject() {
        return asObject;
    }

    @JsonProperty(value = "as object", required = false)
    public void setAsObject(boolean asObject) {
        this.asObject = asObject;
    }

    public StoreType getStoreType(){
        return storeApi.getStoreType();
    }

    public void addTargetAttributes(String[] targetFields) {
        targetAttributes = Stream.concat(targetAttributes.stream(), Arrays.stream(targetFields)).toList();
    }

    public List<String> getTargetAttributes() {
        return targetAttributes;
    }

    public String getMetricFamily() {
        return metricFamily;
    }

    @JsonProperty(value = "by metric family", required = false)
    public void setMetricFamily(String metricFamily) {
        this.metricFamily = metricFamily;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if((!asObject && !allAttributes && targetFields == null) || (asObject && allAttributes && targetFields!=null))
            throw new InvalidSpecificationException("Invalid enrichment configuration. Review Joule enrichment documentation.");

        if(asObject && allAttributes) {
            throw new InvalidSpecificationException("Invalid enrichment configuration. Either 'as object' or 'all attributes' can be set.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReferenceDataQueryCriteriaSpecification that = (ReferenceDataQueryCriteriaSpecification) o;
        return allAttributes == that.allAttributes && asObject == that.asObject && Objects.equals(parentFieldName, that.parentFieldName) && Objects.equals(storeApi, that.storeApi) && Objects.equals(targetAttributes, that.targetAttributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parentFieldName, storeApi, allAttributes, asObject, targetAttributes);
    }
}
