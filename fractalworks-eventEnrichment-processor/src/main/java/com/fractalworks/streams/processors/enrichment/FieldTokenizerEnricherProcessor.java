/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.referencedata.FieldTokenizer;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Event enricher using a field tokenizers that applies user defined tokenization logic to a specified event field. The resulting
 * tokenized fields are added in to the passed event
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "tokenizer enricher")
public class FieldTokenizerEnricherProcessor extends AbstractProcessor {

    private Map<String,Class<? extends FieldTokenizer>> fieldTokenizers;
    private final Map<String,FieldTokenizer> fieldTokenizerMap = new HashMap<>();

    public FieldTokenizerEnricherProcessor() {
        super();
        super.cloneEvent = false;
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        for(Map.Entry<String,Class<? extends FieldTokenizer>> entry : fieldTokenizers.entrySet()) {
            try {
                FieldTokenizer tokenizer = entry.getValue().getConstructor().newInstance();
                fieldTokenizerMap.put(entry.getKey(), tokenizer);
                if( logger.isDebugEnabled()) {
                    logger.debug(String.format( "Added [ %s ] tokenizer for [ %s ]",  tokenizer.getClass().getName(), entry.getKey()));
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                metrics.incrementMetric(Metric.PROCESS_FAILED);
                throw new ProcessorException(String.format("Failure to create [ %s ] FieldTokenizer object" , entry.getKey()), e);
            }
        }
        if( logger.isInfoEnabled()) {
            logger.info("Initialization completed");
        }
        enabled = true;
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        try {
            for(Map.Entry<String,FieldTokenizer> entry : fieldTokenizerMap.entrySet()) {
                Optional<Map<String, Object>> fields = entry.getValue().decode(event.getValue(entry.getKey()));
                if (fields.isPresent()) {
                    for (Map.Entry<String, Object> e : fields.get().entrySet()) {
                        event.addValue(uuid, e.getKey(), e.getValue());
                    }
                }
            }
            metrics.incrementMetric(Metric.PROCESSED);
        }catch(Exception e){
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw new StreamsException("Failure in field enrichment processor", e);
        }
        return event;
    }


    public Map<String,Class<? extends FieldTokenizer>> getFieldTokenizer() {
        return fieldTokenizers;
    }

    @JsonProperty(value = "tokenizers")
    public void setFieldTokenizer(Map<String,Class<? extends FieldTokenizer>> fieldTokenizers) {
        this.fieldTokenizers = fieldTokenizers;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (fieldTokenizers == null || fieldTokenizers.isEmpty()) {
            throw new InvalidSpecificationException("fieldTokenizers must be provided.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldTokenizerEnricherProcessor that = (FieldTokenizerEnricherProcessor) o;
        return Objects.equals(fieldTokenizers, that.fieldTokenizers) && Objects.equals(fieldTokenizerMap, that.fieldTokenizerMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldTokenizers, fieldTokenizerMap);
    }
}
