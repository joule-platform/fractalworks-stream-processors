/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment.criteria;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.FailedSQLProcessingException;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.JouleProcessConfig;
import com.fractalworks.streams.sdk.analytics.metrics.MetricQueryInterface;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.StreamsRuntimeException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.referencedata.specifications.query.QueryType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Metrics query specification for use during enrichment process
 */
public class MetricQueryCriteriaSpecification extends ReferenceDataQueryCriteriaSpecification {

    @JsonIgnore
    protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private UUID queryUUID;
    private MetricQueryInterface metricQueryInterface;

    public MetricQueryCriteriaSpecification() {
        super();
        storename = JouleProcessConfig.METRICS_DB_ACCESS_NAME;
        type = QueryType.QL;
    }

    public MetricQueryCriteriaSpecification(ReferenceDataQueryCriteriaSpecification src){
        this();
        parentFieldName = src.getParentFieldName();
        metricFamily = src.getMetricFamily();
        query = src.getQuery();
        targetFields = src.getTargetFields();
        queryFields = src.getQueryFields();
        key = src.getKey();
        asObject = src.isAsObject();
        allAttributes = src.isAllAttributes();
    }


    public void initialise() {
        try {
            // Apply metric look up criteria
            String predicate = query;
            if(key != null){
                predicate = " WHERE " + key + " = ?";
            }
            queryUUID = metricQueryInterface.registerMetricQuery(metricFamily, targetFields, predicate);
        } catch (FailedSQLProcessingException | ProcessorException e) {
            throw new StreamsRuntimeException("Failed to register metric for enricher", e);
        }
    }

    /**
     * Execute the query using the StreamEvent parameters if required
     * @param event
     * @return
     */
    @Override
    public Optional<ReferenceDataObject> executeQuery(StreamEvent event) {
        Optional<ReferenceDataObject> response = Optional.empty();
        try {
            Object[] params = (queryFields!=null) ? new Object[queryFields.length] : new Object[1];
            if(queryFields!= null){
                var i = 0;
                for(String param : queryFields){
                    params[i++] = event.getValue(param);
                }
            } else {
                params[0] = event.getValue(getKey());
            }
            var result = metricQueryInterface.query(queryUUID,params);
            if (result.isPresent())
                response = Optional.of(new ReferenceDataObject(parentFieldName, result.get()));
        } catch (StreamsException e) {
            if(logger.isErrorEnabled())
                logger.error("Failed to enrich event with metric data", e);
        }
        return response;
    }

    public void setMetricQueryInterface(MetricQueryInterface metricQueryInterface) {
        this.metricQueryInterface = metricQueryInterface;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MetricQueryCriteriaSpecification that = (MetricQueryCriteriaSpecification) o;
        return Objects.equals(queryUUID, that.queryUUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), queryUUID);
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (metricFamily == null) {
            throw new InvalidSpecificationException("metric family must be provided.");
        }
        if( !storename.equals(JouleProcessConfig.METRICS_DB_ACCESS_NAME)){
            throw new InvalidSpecificationException("MetricDB must be provided.");
        }
    }
}
