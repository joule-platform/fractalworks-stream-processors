/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Lyndon Adams
 */
public class TokenizerEnricherTest {

    @Test
    public void validEventTokenizerYamlTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventtokenizer.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        FieldTokenizerEnricherProcessor processor = readMapper.readValue(file, FieldTokenizerEnricherProcessor.class);
        assertNotNull(processor);
    }

    @Test
    public void validEventTokenizerTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventtokenizer.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        FieldTokenizerEnricherProcessor processor = readMapper.readValue(file, FieldTokenizerEnricherProcessor.class);
        processor.initialize(null);

        StreamEvent evt = new StreamEvent("test");
        evt.addValue("longlat", "123.01,34.99");

        // Process
        evt = processor.apply(evt, null);

        // Validate
        assertEquals(34.99f, evt.getValue("latitude"));
        assertEquals(123.01f, evt.getValue("longitude"));
    }

}
