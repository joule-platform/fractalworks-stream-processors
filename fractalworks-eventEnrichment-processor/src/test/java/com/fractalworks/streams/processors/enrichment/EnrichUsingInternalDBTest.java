package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.FailedSQLProcessingException;
import com.fractalworks.streams.core.management.JouleProcessConfig;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.core.processors.embeddedsql.EmbeddedSQLStore;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnrichUsingInternalDBTest {

    static String DATABASE = "./internaldb";

    static EventEnricherProcessor processor;

    @BeforeAll
    public static void setup() throws FailedSQLProcessingException, SQLException, IOException, ProcessorException {
        JouleProcessConfig.setProperty(JouleProcessConfig.JOULE_DB_PATH, "./internaldb/internalDBEnrichmentTest.db");
        JouleProcessConfig.IS_INTERNALDB_ENABLED = true;

        var sqlStore = EmbeddedSQLStore.getInstance();
        sqlStore.createTable("fx_rates(symbol VARCHAR PRIMARY KEY, exchange_rate DOUBLE, market VARCHAR);");
        sqlStore.createIndex("joule", "fx_rates",new String[]{"symbol"},true);

        var conn = sqlStore.getConnection();
        var stmt = conn.createStatement();
        stmt.execute("INSERT INTO fx_rates VALUES ('GBPEUR',1.17, 'LDN');");
        stmt.execute("INSERT INTO fx_rates VALUES ('GBPUSD', 1.268,'LDN');");

        ClassLoader classLoader = EnrichUsingInternalDBTest.class.getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventenricher-withJouleDB.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");

        processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.initialize(null);
    }

    @Test
    @Order(1)
    public void enrichByValue() throws IOException, StreamsException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");
        var responseEvent = processor.apply(event, null);
        assertEquals(1.170d,responseEvent.getValue("exchange_rate"));
    }

    @Test
    @Order(2)
    public void enrichByObject() throws IOException, StreamsException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");

        var responseEvent = processor.apply(event, null);
        var as_object = (Map<String,Object>) responseEvent.getValue("as_object");
        assertEquals(1.170d,as_object.get("exchange_rate"));
    }

    @Test
    @Order(3)
    public void enrichByAllAttributes() throws IOException, StreamsException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");
        var responseEvent = processor.apply(event, null);
        assertEquals("LDN",responseEvent.getValue("market"));
        assertEquals(1.170d,responseEvent.getValue("exchange_rate"));
    }

    @AfterAll
    public static void cleanup(){
        FileUtils.deleteQuietly(new File(DATABASE));
    }
}
