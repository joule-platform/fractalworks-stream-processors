/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment.dsl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.processors.enrichment.EventEnricherProcessor;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.storage.StoreType;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Validate key query optimisation logic
 *
 * @author Lyndon Adams
 */
@ExtendWith(MockitoExtension.class)
public class EnricherQueryOptimisationTest {

    static Map<String, DataStoreQueryInterface<?>> dataStores = new HashMap<>();

    @BeforeAll
    public static void setDataStores(){
        MockDataStoreQueryInterface keyQueryInterface = new MockDataStoreQueryInterface();
        keyQueryInterface.setStoreName("mobiledevices");
        dataStores.put("devicesInfo", keyQueryInterface);

        MockDataStoreQueryInterface dataQueryInterface = new MockDataStoreQueryInterface();
        dataQueryInterface.setStoreName("privacyControlsStore");
        dataStores.put("privacyControls", dataQueryInterface);

        MockDataStoreQueryInterface oqlQueryInterface = new MockDataStoreQueryInterface();
        oqlQueryInterface.setStoreName("userDataBundles");
        dataStores.put("dataBundles", oqlQueryInterface);
    }

    @Test
    void validateKeyBasedQueryOptimisation() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/multipleKeyWithSameSourceOptimisation.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventEnricherProcessor processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.setDataStores(dataStores);
        processor.initialize(null);

        var opt = processor.getOptimiseQueries();
        assertEquals(4, opt.size());
    }

    // Test stud class
    static class MockDataStoreQueryInterface extends DataStoreQueryInterface {
        public MockDataStoreQueryInterface() {}

        public void initialise() throws ProcessorException {}

        @Override
        public StoreType getStoreType() {
            return StoreType.KEY_VALUE;
        }
    }
}
