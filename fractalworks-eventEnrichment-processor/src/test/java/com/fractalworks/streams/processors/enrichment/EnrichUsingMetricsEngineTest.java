package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.JouleProcessConfig;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.core.processors.analytics.metricengine.MetricEngine;
import com.fractalworks.streams.core.processors.analytics.metricengine.specification.MetricsEngineSpecification;
import com.fractalworks.streams.core.processors.embeddedsql.EmbeddedSQLStore;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class EnrichUsingMetricsEngineTest {

    static String DATABASE = "./metricsdb";

    static Optional<ReferenceDataObject> metricsResponse;

    public static void setupMetricEngine() throws IOException, ProcessorException {
        ClassLoader classLoader = EnrichUsingMetricsEngineTest.class.getClassLoader();
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        File metricsEngineDef = new File(classLoader.getResource("dsl/valid/metricsengine.yaml").getFile());
        MetricsEngineSpecification metricsEngineSpecification = readMapper.readValue(metricsEngineDef, MetricsEngineSpecification.class);
        MetricEngine.createInstance(metricsEngineSpecification, null);
    }

    public static EventEnricherProcessor setupEnrichProcessor(String file) throws IOException, InvalidSpecificationException, ProcessorException {
        ClassLoader classLoader = EnrichUsingMetricsEngineTest.class.getClassLoader();
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        File enricherDef = new File(classLoader.getResource(file).getFile());

        var processor = readMapper.readValue(enricherDef, EventEnricherProcessor.class);
        processor.validate();
        processor.initialize(null);
        return processor;
    }

    @BeforeAll
    public static void setup() throws IOException, StreamsException, SQLException {
        JouleProcessConfig.setProperty(JouleProcessConfig.JOULE_DB_PATH, "./metricsdb/metricsEngineEnrichmentTest.db");
        JouleProcessConfig.IS_INTERNALDB_ENABLED = true;

        setupMetricEngine();
        var sqlStore = EmbeddedSQLStore.getInstance();
        var conn = sqlStore.getConnection();
        var stmt = conn.createStatement();

        stmt.execute("INSERT INTO BidMovingAverage VALUES ('GBPEUR', 1.1656,1.168, 1.169, '2024-02-24 11:30:00.123456');");
        stmt.execute("INSERT INTO BidMovingAverage VALUES ('GBPUSD', 1.268,1.269, 1.273, '2024-02-24 11:30:00.123456');");
    }

    @Test
    @Order(1)
    void enrichByValue() throws StreamsException, IOException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");

        var processor = setupEnrichProcessor("dsl/valid/eventenricher-withMetricsByValue.yaml");
        var responseEvent = processor.apply(event, null);
        assertEquals( 1.1656d,responseEvent.getValue("avg_bid_min"));
        assertEquals( 1.168d,responseEvent.getValue("avg_bid"));
        assertNull( responseEvent.getValue("avg_bid_max"));
    }

    @Test
    @Order(2)
    void enrichByObject() throws IOException, StreamsException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");

        var processor = setupEnrichProcessor("dsl/valid/eventenricher-withMetricsByObject.yaml");
        var responseEvent = processor.apply(event, null);
        var as_object = (Map<String, Object>) responseEvent.getValue("as_object_field");
        assertNotNull(as_object);
        assertEquals( 1.1656d,as_object.get("avg_bid_min"));
        assertEquals( 1.168d,as_object.get("avg_bid"));
        assertEquals( 1.169d,as_object.get("avg_bid_max"));
    }

    @Test
    @Order(3)
    void enrichByAllAttributes() throws IOException, StreamsException {
        var event = new StreamEvent("test");
        event.addValue("symbol", "GBPEUR");

        var processor = setupEnrichProcessor("dsl/valid/eventenricher-withMetricsByAllAttributes.yaml");
        var responseEvent = processor.apply(event, null);
        assertNotNull(responseEvent);
        assertEquals( 1.1656d,responseEvent.getValue("avg_bid_min"));
        assertEquals( 1.168d,responseEvent.getValue("avg_bid"));
        assertEquals( 1.169d,responseEvent.getValue("avg_bid_max"));
    }
    @AfterAll
    public static void cleanup(){
        FileUtils.deleteQuietly(new File(DATABASE));
    }
}
