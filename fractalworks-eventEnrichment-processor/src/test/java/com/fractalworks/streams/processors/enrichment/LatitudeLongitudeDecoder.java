package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.sdk.referencedata.FieldTokenizer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Latitude and longitude decoder. Tokenised by latitude then longitude.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "latitudeLongitudeDecoder")
public class LatitudeLongitudeDecoder implements FieldTokenizer {

    public LatitudeLongitudeDecoder() {
    }

    @Override
    public Optional<Map<String, Object>> decode(Object o) {
        if( o instanceof String) {
            String[] co = ((String) o).split(",");
            Map<String, Object> map = new HashMap<>();
            map.put("latitude", Float.parseFloat(co[1]));
            map.put("longitude", Float.parseFloat(co[0]));
            return Optional.of(map);
        }
        return Optional.empty();
    }
}
