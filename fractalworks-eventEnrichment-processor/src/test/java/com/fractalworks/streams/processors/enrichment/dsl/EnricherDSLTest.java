/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.enrichment.dsl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.processors.enrichment.EventEnricherProcessor;
import com.fractalworks.streams.processors.enrichment.FieldTokenizerEnricherProcessor;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Lyndon Adams
 */
public class EnricherDSLTest{

    @Test
    public void validEventTokenizerYamlTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventtokenizer.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        FieldTokenizerEnricherProcessor processor = readMapper.readValue(file, FieldTokenizerEnricherProcessor.class);
        processor.validate();
        assertNotNull(processor);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "eventenricher.yaml",
            "eventenricher-allattributes.yaml",
            "eventenricher-asObject.yaml",
            "eventenricher-withJouleDB.yaml",
            "eventenricher-withMetrics.yaml",
            "eventenricher-withMetricsByAllAttributes.yaml",
            "eventenricher-withMetricsByObject.yaml",
            "eventenricher-withMetricsByValue.yaml",
            "eventenricherwithoql.yaml",
            "multipleKeyWithSameSourceOptimisation.yaml"})
    void validEventEnricherYamlTest(String filaname) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/" + filaname).getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventEnricherProcessor processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.validate();
        assertNotNull(processor);
    }
}
