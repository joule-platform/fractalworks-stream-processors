package com.fractalworks.streams.processors.enrichment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.StoreType;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class EventEnrichmentTest {

    @Mock
    static Map<String, DataStoreQueryInterface<?>> dataStores = new HashMap<>();

    static MockDataStoreQueryInterface keyQueryInterface = new MockDataStoreQueryInterface();

    @BeforeAll
    public static void setDataStores(){
        keyQueryInterface.setStoreName("mobiledevices");
        dataStores.put("deviceStore", keyQueryInterface);
    }

    @Test
    public void enrichmentUsingKeyWithValuesTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventenricher.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventEnricherProcessor processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.setDataStores(dataStores);
        processor.validate();
        processor.initialize(null);

        assertNotNull(processor);

        StreamEvent evt = new StreamEvent("test");
        evt.addValue("tac", "abcde");

        var responseEvt = processor.apply(evt, new Context());
        assertEquals("2023", responseEvt.getValue("year_released"));
        assertEquals("Apple", responseEvt.getValue("deviceManufacturer"));
        assertEquals("15", responseEvt.getValue("modelNumber"));
        assertNull(responseEvt.getValue("os"));
    }

    @Test
    public void enrichmentUsingKeyWithAllValuesTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventenricher-allattributes.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventEnricherProcessor processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.setDataStores(dataStores);
        processor.validate();
        processor.initialize(null);

        assertNotNull(processor);

        StreamEvent evt = new StreamEvent("test");
        evt.addValue("tac", "abcde");

        var responseEvt = processor.apply(evt, new Context());
        assertEquals("2023", responseEvt.getValue("year_released"));
        assertEquals("Apple", responseEvt.getValue("deviceManufacturer"));
        assertEquals("15", responseEvt.getValue("modelNumber"));
        assertEquals("iOS 17", responseEvt.getValue("os"));
    }
    @Test
    public void enrichmentUsingKeyWithAsObjectTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/eventenricher-asObject.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        EventEnricherProcessor processor = readMapper.readValue(file, EventEnricherProcessor.class);
        processor.setDataStores(dataStores);
        processor.validate();
        processor.initialize(null);

        assertNotNull(processor);

        StreamEvent evt = new StreamEvent("test");
        evt.addValue("tac", "abcde");

        var responseEvt = processor.apply(evt, new Context());
        var deviceInfo = (Map<String,Object>)responseEvt.getDictionary().get("deviceInfo");
        assertEquals("2023", deviceInfo.get("year_released"));
        assertEquals("Apple", deviceInfo.get("deviceManufacturer"));
        assertEquals("15", deviceInfo.get("modelNumber"));
        assertEquals("iOS 17", deviceInfo.get("os"));
    }

    static class MockDataStoreQueryInterface extends DataStoreQueryInterface<ReferenceDataObject> {
        public MockDataStoreQueryInterface() {
        }
        public void initialise() throws ProcessorException {
        }

        @Override
        public Optional<ReferenceDataObject> queryByKey(Object key) {
            ReferenceDataObject refData = new ReferenceDataObject();
            refData.setKey(key);
            refData.put("deviceManufacturer","Apple");
            refData.put("year_released","2023");
            refData.put("modelNumber","15");
            refData.put("os","iOS 17");
            return Optional.of(refData);
        }

        @Override
        public StoreType getStoreType() {
            return StoreType.KEY_VALUE;
        }
    }

}
