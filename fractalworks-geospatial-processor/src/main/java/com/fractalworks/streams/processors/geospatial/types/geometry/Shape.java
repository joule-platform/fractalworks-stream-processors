/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geometry;

/**
 * Base Shape interface.
 * <p>
 * Created by lyndon on 25/01/2017.
 */
public interface Shape extends Coordinates {

    /**
     * Get the area of the shape.
     *
     * @return
     */
    double getArea();

    /**
     * Determine if provided x,y points sit with the shape.
     *
     * @param x
     * @param y
     * @return true is exists otherwise false.
     */
    boolean contains(final double x, final double y);

    /**
     * Sub-divide shape.
     *
     * @return
     */
    Shape subdivide();

    /**
     * Determines if passed shape intersects itself.
     *
     * @param searchArea
     * @return True is intersects otherwise false
     */
    boolean intersects(Shape searchArea);
}
