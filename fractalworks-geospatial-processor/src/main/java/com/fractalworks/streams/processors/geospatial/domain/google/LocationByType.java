/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.google;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Location by type using additional meta data such as place_id and typs
 * <p>
 * Created by Lyndon Adams
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "address_components",
        "formatted_address",
        "geometry",
        "place_id",
        "types"
})
public class LocationByType {

    private List<AddressComponent> addressComponents;
    private String formattedAddress;
    private Geometry geometry;
    private String placeId;
    private String[] types;

    /**
     * Default constructor required for serialization.
     */
    public LocationByType() {
        // Required for serialization
    }

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    @JsonProperty("address_components")
    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }


    public String getFormattedAddress() {
        return formattedAddress;
    }

    @JsonProperty("formatted_address")
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }


    public Geometry getGeometry() {
        return geometry;
    }

    @JsonProperty("geometry")
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    @JsonProperty("place_id")
    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String[] getTypes() {
        return types;
    }

    @JsonProperty("types")
    public void setTypes(String[] types) {
        this.types = types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationByType)) return false;
        LocationByType that = (LocationByType) o;
        return Objects.equals(addressComponents, that.addressComponents) &&
                Objects.equals(formattedAddress, that.formattedAddress) &&
                Objects.equals(geometry, that.geometry) &&
                Objects.equals(placeId, that.placeId) &&
                Arrays.equals(types, that.types);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(addressComponents, formattedAddress, geometry, placeId);
        result = 31 * result + Arrays.hashCode(types);
        return result;
    }
}
