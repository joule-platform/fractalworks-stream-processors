/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;

import java.util.Objects;

/**
 * CellTower unique key
 * <p>
 * Created by Lyndon Adams
 */
public class CellTowerKey implements Comparable<CellTowerKey> {

    private final int area;
    private final int net;
    private final int cell;
    private final int mcc;

    public CellTowerKey(int area, int net, int cell, int mcc) {
        this.area = area;
        this.net = net;
        this.cell = cell;
        this.mcc = mcc;
    }

    public int getArea() {
        return area;
    }

    public int getNet() {
        return net;
    }

    public int getCell() {
        return cell;
    }

    public int getMcc() {
        return mcc;
    }

    @Override
    public int compareTo(CellTowerKey o) {
        int value = 0;
        if (cell - o.cell == 0) {
            if (area - o.area == 0) {
                if (net - o.net == 0) {
                    if (mcc - o.mcc != 0) {
                        value = cell + area + net + (mcc - o.mcc);
                    }
                } else {
                    value = cell + area + (net - o.net);
                }
            } else {
                value = cell + (area - o.area);
            }
        } else {
            value = cell - o.cell;
        }
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CellTowerKey)) return false;
        CellTowerKey that = (CellTowerKey) o;
        return area == that.area &&
                net == that.net &&
                cell == that.cell &&
                mcc == that.mcc;
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, net, cell, mcc);
    }
}
