/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.LocationChangeListener;
import com.fractalworks.streams.processors.geospatial.types.geometry.Shape;
import com.fractalworks.streams.processors.geospatial.utils.GeoMath;

import java.util.HashSet;
import java.util.Set;

/**
 * Geofence based upon a circle.
 */
@JsonRootName(value = "geofence")
public class GeoFence implements Shape {

    private int id;
    private double centreX;
    private double centreY;
    private double radius;
    private double area;

    private Tuple<Double, Double> coordinates;

    private final Set<LocationChangeListener<GeoFence>> geofenceListeners = new HashSet<>();

    public GeoFence() {
    }

    public GeoFence(double x, double y, double radius) {
        this.centreX = x;
        this.centreY = y;
        this.radius = radius;
        this.area = Math.PI * (radius * radius);
        this.coordinates = new Tuple<>(centreX, centreY);
    }

    /**
     * Default constructor to create a geofence based upon a circle.
     *
     * @param x
     * @param y
     * @param radius
     * @param id
     */
    public GeoFence(int id, double x, double y, double radius) {
        this(x, y, radius);
        this.id = id;
    }

    public void initialise(){
        this.area = Math.PI * (radius * radius);
        this.coordinates = new Tuple<>(centreX, centreY);
    }

    public void updateCoordinates(double x, double y) {
        this.centreX = x;
        this.centreY = y;
        this.coordinates = new Tuple<>(centreX, centreY);
        geofenceListeners.parallelStream().forEach(l -> l.onLocationChange(this));
    }

    public void registerLocationChangeListener(LocationChangeListener<GeoFence> l) {
        geofenceListeners.add(l);
    }

    public void unregisterLocationChangeListener(LocationChangeListener<GeoFence> l) {
        geofenceListeners.remove(l);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @JsonProperty(value = "coordinates",required = true)
    public void setCoordinates(Tuple<Double, Double> coordinates) {
        if(coordinates!=null) {
            this.coordinates = coordinates;
            this.centreX = coordinates.getX();
            this.centreY = coordinates.getY();
        }
    }

    @Override
    public Tuple<Double, Double> getCoordinates() {
        return coordinates;
    }

    public double getCentreX() {
        return centreX;
    }

    public void setCentreX(double centreX) {
        this.centreX = centreX;
    }

    public void setCentreY(double centreY) {
        this.centreY = centreY;
    }

    public double getCentreY() {
        return centreY;
    }

    @JsonProperty(value = "radius", required = true)
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return area;
    }


    /**
     * Determines if the passed lat,lng points are within the geo fence using a distance by meters approach.
     *
     * @param lat
     * @param lng
     * @return
     */
    @Override
    public boolean contains(double lat, double lng) {
        double distanceByMeter = GeoMath.distanceBetweenPointsByMeter(centreX, centreY, lat, lng);
        return (distanceByMeter <= radius);
    }

    @Override
    public Shape subdivide() {
        return (radius > 2) ? new GeoFence(id, centreX, centreY, (int) (radius / 2)) : null;
    }

    @Override
    public boolean intersects(Shape searchArea) {
        return false;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoFence)) return false;
        GeoFence geoFence = (GeoFence) o;
        return id == geoFence.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "GeoFence{" +
                "id=" + id +
                ", centre_x=" + centreX +
                ", centre_y=" + centreY +
                ", radius=" + radius +
                '}';
    }
}
