/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.country;

import java.util.List;
import java.util.Objects;

/**
 * Town domain class
 *
 * @author Lyndon Adams
 */
public class Town {

    private String name;
    private boolean isCity;

    private List<LocalRegion> boroughs;
    private long population;

    public Town() {
        // Required
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCity() {
        return isCity;
    }

    public void setCity(boolean city) {
        isCity = city;
    }

    public List<LocalRegion> getBoroughs() {
        return boroughs;
    }

    public void setBoroughs(List<LocalRegion> boroughs) {
        this.boroughs = boroughs;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, isCity, boroughs, population);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Town)) return false;
        Town town = (Town) o;
        return isCity == town.isCity &&
                population == town.population &&
                Objects.equals(name, town.name) &&
                Objects.equals(boroughs, town.boroughs);
    }

    @Override
    public String toString() {
        return "Town{" +
                "name='" + name + '\'' +
                ", isCity=" + isCity +
                ", boroughs=" + boroughs +
                ", population=" + population +
                '}';
    }
}
