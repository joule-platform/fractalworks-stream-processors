/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils.jackson;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fractalworks.streams.processors.geospatial.types.geometry.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Point deserilization class for (x,y,z)
 *
 * @author Lyndon Adams
 */
public class PointDeserializer extends JsonDeserializer<Point> {

    private final Logger logger = LoggerFactory.getLogger(PointDeserializer.class);
    private static final String EXCEPTIONMSG = "Unexpected end-of-input when binding data into Point";

    @Override
    public Point deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        if (logger.isInfoEnabled()) {
            logger.info("Deserializing GeoJson point using custom deserializer.");
        }

        if (jp.isExpectedStartArrayToken()) {
            return deserializeArray(jp);
        }
        throw new JsonMappingException(jp, EXCEPTIONMSG);
    }

    protected Point deserializeArray(JsonParser jp) throws IOException {
        Point node = new Point();
        node.setX(extractDouble(jp, false));
        node.setY(extractDouble(jp, false));
        node.setZ(extractDouble(jp, true));
        if (jp.hasCurrentToken() && jp.getCurrentToken() != JsonToken.END_ARRAY)
            jp.nextToken();
        return node;
    }

    private double extractDouble(JsonParser jp, boolean optional)
            throws IOException {
        JsonToken token = jp.nextToken();
        if (token == null) {
            if (optional)
                return Double.NaN;
            else
                throw new JsonParseException(jp, EXCEPTIONMSG);
        } else {
            switch (token) {
                case END_ARRAY:
                    if (optional)
                        return Double.NaN;
                    else
                        throw new JsonMappingException(jp, EXCEPTIONMSG);
                case VALUE_NUMBER_FLOAT:
                    return jp.getDoubleValue();
                case VALUE_NUMBER_INT:
                    return jp.getLongValue();
                default:
                    throw new JsonParseException(jp, String.format("Unexpected token (%s) when binding data into Point", token.name()));
            }
        }
    }
}
