/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.country;

import com.fractalworks.streams.processors.geospatial.types.geometry.Polygon;

import java.util.List;
import java.util.Objects;

/**
 * Country region.
 *
 * @author Lyndon Adams
 */
public class LocalRegion {

    private String areaName;

    private List<String> zipcodes;
    private Polygon boundary;

    public LocalRegion() {
        // Required
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public List<String> getZipCodes() {
        return zipcodes;
    }

    public void setZipCodes(List<String> codes) {
        this.zipcodes = codes;
    }

    public Polygon getBoundary() {
        return boundary;
    }

    public void setBoundary(Polygon polygon) {
        this.boundary = polygon;
    }

    @Override
    public int hashCode() {
        return Objects.hash(areaName, zipcodes, boundary);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocalRegion)) return false;
        LocalRegion that = (LocalRegion) o;
        return Objects.equals(areaName, that.areaName) &&
                Objects.equals(zipcodes, that.zipcodes) &&
                Objects.equals(boundary, that.boundary);
    }

    @Override
    public String toString() {
        return "Region{" +
                "areaName='" + areaName + '\'' +
                ", zipcodes=" + zipcodes +
                ", boundary=" + boundary +
                '}';
    }
}
