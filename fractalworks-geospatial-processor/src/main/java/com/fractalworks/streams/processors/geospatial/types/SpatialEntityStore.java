package com.fractalworks.streams.processors.geospatial.types;

import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.processors.geospatial.types.geometry.Shape;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.storage.StoreType;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

public class SpatialEntityStore implements Store<GeoNode> {

    private final AtomicInteger recordCount = new AtomicInteger();
    private QuadTree<GeoNode> searchTree;

    public SpatialEntityStore(QuadTree<GeoNode> searchTree) {
        this.searchTree = searchTree;
    }

    @Override
    public void initialize() throws TransportException {
    }

    @Override
    public boolean load(GeoNode value) {
        boolean success = searchTree.insert(value);
        if (success) {
            recordCount.incrementAndGet();
        }
        return success;
    }


    public Collection<GeoNode> query(Shape key) {
        return searchTree.query(key);
    }

    @Override
    public StoreType getStoreType() {
        return StoreType.GEO_SPATIAL;
    }

}
