/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;

import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.referencedata.serialization.ReferenceDataArrowParser;
import org.apache.arrow.vector.FieldVector;
import org.apache.arrow.vector.VectorSchemaRoot;

/**
 * CellTower Arrow row parser.
 * <p>
 * Format
 * - radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal
 * <p>
 * Created by Lyndon Adams
 */
public class CellTowerArrowParser implements ReferenceDataArrowParser<CellTower> {

    /**
     * Parse provided vectorSchemaRoot to CellTower.
     *
     * @param row
     * @return CellTower
     */
    @Override
    public CellTower translate(VectorSchemaRoot row) throws TranslationException {
        CellTower cellTower = new CellTower();
        for(FieldVector v : row.getFieldVectors()){
            var str = v.getObject(0);
            switch (v.getName().toLowerCase() ){
                case "radio" -> cellTower.setRadioType(RadioType.getEnum(str.toString().intern()));
                case "mcc" -> cellTower.setMcc( ((Long)str).intValue());
                case "net" -> cellTower.setNet(((Long)str).intValue());
                case "area" -> cellTower.setArea(((Long)str).intValue());
                case "cell" -> cellTower.setCell(((Long)str).intValue());
                case "unit" -> cellTower.setUnit( (str!=null) ? ((Long)str).intValue() : -1);
                case "lon" -> cellTower.setLon((Double)str);
                case "lat" -> cellTower.setLat((Double)str);
                case "range" -> cellTower.setRange(((Long)str).intValue());
                case "samples" -> cellTower.setSamples(((Long)str).intValue());
                case "changeable" -> cellTower.setChangeable(((Long)str).intValue());
                case "created" -> cellTower.setCreated((Long) str);
                case "updated" -> cellTower.setUpdated((Long) str);
                case "averagesignal" -> cellTower.setAverageSignal( (str!=null) ?((Long)str).intValue() : -1);
                default -> throw new TranslationException(String.format("Unknown field '%s'", v.getName()));
            }
        }
        return cellTower;
    }
}
