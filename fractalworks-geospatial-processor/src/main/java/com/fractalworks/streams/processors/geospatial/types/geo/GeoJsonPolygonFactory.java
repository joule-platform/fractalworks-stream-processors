/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fractalworks.streams.processors.geospatial.types.geometry.Point;
import com.fractalworks.streams.processors.geospatial.types.geometry.Polygon;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * GeoJson polygon factory that creates polygons from sets of points.
 *
 * @author Lyndon Adams
 */
public final class GeoJsonPolygonFactory {

    public static final Polygon DEFAULTPOLYGON = new Polygon(new Point(90, -90));

    private GeoJsonPolygonFactory() {
        // Hide constructor
    }

    /**
     * Create a GeoJson polygon from a string of long,lat pairs.
     *
     * @param longLatString - long,lat pairs where each pair is separated by a space
     * @return - Polygon for valid longLatString or default polygon
     */
    public static Polygon createPolygon(final String longLatString) {

        if (longLatString == null || longLatString.length() < 2) {
            return DEFAULTPOLYGON;
        }

        List<Point> points = new ArrayList<>();

        String[] lngLatTokens = longLatString.split(" ");

        for (int i = 0; i < lngLatTokens.length; i++) {
            String[] lngLat = lngLatTokens[i].split(",");

            double lng = Double.parseDouble(lngLat[0]);
            double lat = Double.parseDouble(lngLat[1]);
            points.add(new Point(lng, lat));
        }
        return new Polygon(points);
    }

    /**
     * Create a GeoJson polygon from a string of long,lat pairs.
     *
     * @param coordinates - long,lat pairs
     * @return - Polygon
     */
    public static Polygon createPolygon(final List<Coordinate> coordinates) {

        if (coordinates == null || coordinates.size() < 2) {
            return DEFAULTPOLYGON;
        }

        List<Point> points = new ArrayList<>();

        for (Coordinate coordinate : coordinates) {
            points.add(new Point(coordinate.getLongitude(), coordinate.getLatitude(), coordinate.getAltitude()));
        }

        return new Polygon(points);
    }

}
