/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoTrackingInfo;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import com.fractalworks.streams.sdk.functions.EventFunction;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Properties;

/**
 * GeoFence occupancy state trigger using user defined plugin
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "geofence occupancy trigger")
public class GeoFenceOccupancyStateTrigger extends AbstractProcessor {

    private String geoTrackerField;
    private Class<? extends EventFunction> plugIn;
    private EventFunction function;

    public GeoFenceOccupancyStateTrigger() {
        // Required
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        try {
            function = plugIn.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new ProcessorException("Failure creating instance of custom plugin",  e);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent streamEvent, Context context) throws StreamsException {
        if (streamEvent.contains(geoTrackerField) &&  streamEvent.getValue(geoTrackerField) instanceof GeoTrackingInfo geoTrackingInfo) {
            if (!geoTrackingInfo.getGeoFenceOccupancyState().isEmpty()) {
                Map<Integer, GeoFenceConstants> c = geoTrackingInfo.getGeoFenceOccupancyState();

                final Context localContext = (context!=null) ? context : new Context();

                // Foreach Geofence call specific function
                c.forEach((id, geoInfo) -> {
                    localContext.addValue("trackingTag", geoTrackingInfo.getTrackingTag());
                    localContext.addValue("geofenceId", id);
                    localContext.addValue("state", geoInfo);
                    function.onEvent(streamEvent, localContext);
                });
            }
            metrics.incrementMetric(Metric.PROCESSED);
        }else {
            metrics.incrementMetric(Metric.IGNORED);
        }
        return streamEvent;
    }

    public String getGeoTrackerField() {
        return geoTrackerField;
    }

    @JsonProperty(value = "tracker field", required = true)
    public void setGeoTrackerField(@Nonnull String geoTrackerField) {
        this.geoTrackerField = geoTrackerField;
    }

    public Class<? extends EventFunction> getPlugIn() {
        return plugIn;
    }

    @JsonProperty(value = "plugin", required = true)
    public void setPlugIn(@Nonnull Class<? extends EventFunction> plugIn) {
        this.plugIn = plugIn;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (geoTrackerField == null || geoTrackerField.isEmpty()) {
            throw new InvalidSpecificationException("geoTrackerField cannot be null or empty.");
        }
        if (plugIn == null) {
            throw new InvalidSpecificationException("plugIn cannot be null");
        }
    }
}
