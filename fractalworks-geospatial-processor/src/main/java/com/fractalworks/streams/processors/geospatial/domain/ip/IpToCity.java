/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.ip;

import com.fractalworks.streams.sdk.referencedata.ReferenceData;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Objects;

/**
 * IP to City reference data type
 * <p>
 * Created by Lyndon Adams
 */
public class IpToCity implements ReferenceData {

    private static final long[] precomputedOctets = new long[]{(long) Math.pow(256, 3), (long) Math.pow(256, 2), 256, 1};
    private short[] fromIP;
    private short[] toIP;
    private String country;
    private String area;
    private String city;
    private String misc;
    private long lowerIPRange;
    private long upperIPRange;
    private long midValue;

    public IpToCity() {
    }

    public IpToCity(short[] fromIP, short[] toIP, String country, String area, String city, String misc) {
        this.fromIP = fromIP;
        this.toIP = toIP;
        this.country = (country != null) ? country.intern() : null;
        this.area = area;
        this.city = city;
        this.misc = misc;
    }

    public void ipRanges(){
        lowerIPRange = ipToLong(fromIP);
        upperIPRange = ipToLong(toIP);
        midValue = upperIPRange - (upperIPRange - lowerIPRange);
    }

    public static long ipToLong(@Nonnull String ipAddress) {
        String[] ipAddressOctets = ipAddress.split("\\.");
        short[] addressAsShorts = new short[4];

        for (int i = 0; i < 4; i++) {
            addressAsShorts[i] = Short.parseShort(ipAddressOctets[i]);
        }

        return ipToLong(addressAsShorts);
    }

    public static long ipToLong(short[] bits) {
        long value = 0;
        for (int i = 0; i < 4; i++) {
            value += precomputedOctets[i] * bits[i];
        }
        return value;
    }


    public boolean isWithinRange(long ipAddress) {
        return (lowerIPRange <= ipAddress && ipAddress <= upperIPRange);
    }

    public long getLowerIPRange() {
        return lowerIPRange;
    }

    public long getUpperIPRange() {
        return upperIPRange;
    }

    @Override
    public Long getKey() {
        return midValue;
    }

    public short[] getFromIP() {
        return fromIP;
    }

    public short[] getToIP() {
        return toIP;
    }

    public String getCountry() {
        return country;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getMisc() {
        return misc;
    }

    public IpToCity setFromIP(short[] fromIP) {
        this.fromIP = fromIP;
        return this;
    }

    public IpToCity setToIP(short[] toIP) {
        this.toIP = toIP;
        return this;
    }

    public IpToCity setCountry(String country) {
        this.country = country;
        return this;
    }

    public IpToCity setArea(String area) {
        this.area = area;
        return this;
    }

    public IpToCity setCity(String city) {
        this.city = city;
        return this;
    }

    public IpToCity setMisc(String misc) {
        this.misc = misc;
        return this;
    }

    public IpToCity setLowerIPRange(long lowerIPRange) {
        this.lowerIPRange = lowerIPRange;
        return this;
    }

    public IpToCity setUpperIPRange(long upperIPRange) {
        this.upperIPRange = upperIPRange;
        return this;
    }

    public IpToCity setMidValue(long midValue) {
        this.midValue = midValue;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IpToCity)) return false;
        IpToCity ipToCity = (IpToCity) o;
        return lowerIPRange == ipToCity.lowerIPRange &&
                upperIPRange == ipToCity.upperIPRange &&
                midValue == ipToCity.midValue &&
                Arrays.equals(fromIP, ipToCity.fromIP) &&
                Arrays.equals(toIP, ipToCity.toIP) &&
                Objects.equals(country, ipToCity.country) &&
                Objects.equals(area, ipToCity.area) &&
                Objects.equals(city, ipToCity.city) &&
                Objects.equals(misc, ipToCity.misc);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(country, area, city, misc, lowerIPRange, upperIPRange, midValue);
        result = 31 * result + Arrays.hashCode(fromIP);
        result = 31 * result + Arrays.hashCode(toIP);
        return result;
    }

    @Override
    public IpToCity getData() {
        return this;
    }

    @Override
    public String toString() {
        return "IpToCity{" +
                "fromIP=" + Arrays.toString(fromIP) +
                ", toIP=" + Arrays.toString(toIP) +
                ", country='" + country + '\'' +
                ", area='" + area + '\'' +
                ", city='" + city + '\'' +
                ", misc='" + misc + '\'' +
                ", lowerIPRange=" + lowerIPRange +
                ", upperIPRange=" + upperIPRange +
                ", midValue=" + midValue +
                '}';
    }
}
