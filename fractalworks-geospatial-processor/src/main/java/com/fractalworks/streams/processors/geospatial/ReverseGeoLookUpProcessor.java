/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.geospatial.domain.google.GoogleReverseLookupTask;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoCoordinates;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * Location lookup using geo coordinates. Processor use the Google API to find the location details.
 * <p>
 * Created by Lyndon Adams
 */
@JsonRootName(value = "reverse geocoding")
public class ReverseGeoLookUpProcessor extends AbstractProcessor {

    Map<LatLngKey, GeoCoordinates> cachedLocations = new HashMap<>();
    private String latitudeField = "latitude";
    private String longitudeField = "longitude";
    private String resultFieldName = "geoLocation";
    private String apiKey;

    public ReverseGeoLookUpProcessor() {
        super();
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        Object lat = event.getValue(latitudeField);
        Object lng = event.getValue(longitudeField);

        // Discard processing
        if (lat instanceof Double latDouble && lng instanceof Double lngDouble) {
            LatLngKey key = new LatLngKey(latDouble, lngDouble);
            if (cachedLocations.containsKey(key)) {
                event.addValue(uuid, resultFieldName, cachedLocations.get(key));
            } else {
                event.addValue(uuid, resultFieldName, googleReverseLookup(key));
            }
            metrics.incrementMetric(Metric.PROCESSED);
        } else {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            if (logger.isDebugEnabled()) {
                logger.info("Missing latitude and longitude fields.");
            }
        }
        return event;
    }

    /**
     * Perform OOP call to Google API for location
     *
     * @param key
     * @return GeoCoordinates
     * @throws Exception
     */
    private GeoCoordinates googleReverseLookup(@Nonnull LatLngKey key) throws StreamsException {
        GoogleReverseLookupTask task = new GoogleReverseLookupTask(key.lat, key.lng);
        task.setApiKey(apiKey);
        GeoCoordinates location;
        try {
            location = task.call();
            if (location != null) {
                cachedLocations.put(key, location);
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw new StreamsException("Failure looking up google location.", e);
        }
        return location;
    }

    /**
     * Set search result field name.
     *
     * @param resultFieldName
     */
    @JsonProperty(value = "response field")
    public void setResultFieldName(@Nonnull String resultFieldName) {
        this.resultFieldName = resultFieldName;
    }

    @JsonProperty(value = "latitude field")
    public void setLatitudeField(@Nonnull String latitudeField) {
        this.latitudeField = latitudeField;
    }

    @JsonProperty(value = "longitude field")
    public void setLongitudeField(@Nonnull String longitudeField) {
        this.longitudeField = longitudeField;
    }

    @JsonProperty(value = "apiKey")
    public void setApiKey(@Nonnull String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * Lat/Lng Key
     */
    class LatLngKey {

        private final double lat;
        private final double lng;

        public LatLngKey(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LatLngKey latLngKey = (LatLngKey) o;

            if (Double.compare(latLngKey.lat, lat) != 0) return false;
            return Double.compare(latLngKey.lng, lng) == 0;
        }

        @Override
        public int hashCode() {
            long temp = Double.doubleToLongBits(lat);
            int result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(lng);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (latitudeField == null || latitudeField.isEmpty()) {
            throw new InvalidSpecificationException("latitudeField cannot be null or empty.");
        }
        if (longitudeField == null || longitudeField.isEmpty()) {
            throw new InvalidSpecificationException("longitudeField cannot be null or empty.");
        }
        if (apiKey == null || apiKey.isEmpty()) {
            throw new InvalidSpecificationException("apiKey cannot be null or empty.");
        }
    }

}
