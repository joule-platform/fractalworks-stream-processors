/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.geospatial.types.geometry.Coordinates;
import com.fractalworks.streams.processors.geospatial.types.geometry.Rectangle;
import com.fractalworks.streams.processors.geospatial.types.geometry.Shape;
import com.gs.collections.impl.map.mutable.UnifiedMap;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Quad tree implementation for geospatial processing.
 * <p>
 * Preferred max levels is 16 which gives a 600m area.
 */
@JsonRootName(value = "spatial index")
public class QuadTree<T extends Coordinates> {

    // Concurrency controls
    private final ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = reentrantReadWriteLock.readLock();
    private final Lock writeLock = reentrantReadWriteLock.writeLock();

    private QuadTree<T> northWest;
    private QuadTree<T> northEast;
    private QuadTree<T> southEast;
    private QuadTree<T> southWest;
    private Shape bounds;

    private Set<T> nodes;
    private Map<Object, T> nodeMap = new UnifiedMap<>();

    private int preferredMaxElements = 500;
    private int maxlevels = 16;


    private double topLeftX = -180f;
    private double topLeftY = -90f;
    private int width = 360;
    private int area = 64800;


    public QuadTree() {
        // Required
    }

    /**
     * Create a quad tree starting at points (0,0) using a bounding box using passed parameters.
     *
     * @param width
     * @param height
     */
    public QuadTree(int width, int height) {
        this(0f, 0f, width * height, width);
    }

    /**
     * Constructor uses x:0 y:0 as starting points which is used to create a bounding rectangle using the width and area.
     *
     * @param width
     * @param height
     * @param maxlevels
     * @param preferedMaxElements
     */
    public QuadTree(int width, int height, int maxlevels, int preferedMaxElements) {
        this(0f, 0f, width, width * height, maxlevels, preferedMaxElements);
    }

    /**
     * Create a quad tree starting at points (x,y) using a bounding box using passed width and area.
     *
     * @param x
     * @param y
     * @param width
     * @param area
     */
    public QuadTree(final double x, final double y, int width, int area) {
        this.bounds = new Rectangle(x, y, area, width);
    }

    /**
     * Create a quad tree starting at points (x,y) using a bounding box using top left and bottom right x,y points.
     *
     * @param x  top left x.
     * @param y  top left y.
     * @param x2 bottom right x.
     * @param y2 bottom right y.
     */
    public QuadTree(final double x, final double y, final double x2, final double y2) {
        this.bounds = new Rectangle(x, y, x2, y2);
    }


    /**
     * @param x
     * @param y
     * @param width
     * @param area
     * @param preferedMaxElements
     */
    public QuadTree(double x, double y, int width, int area, int maxlevels, int preferedMaxElements) {
        this(x, y, width, area);

        this.preferredMaxElements = preferedMaxElements;
        if (maxlevels > 0 && maxlevels < 24) {
            this.maxlevels = maxlevels;
        }
    }

    /**
     * initialise quad tree if using setters
     */
    public void initialise() {
        if( bounds == null ) {
            this.bounds = new Rectangle(topLeftX, topLeftY, area, width);
            if (maxlevels < 0 && maxlevels > 24) {
                this.maxlevels = 16;
            }
        }
    }

    /**
     * Insert element in to the correct position in the tree.
     *
     * @param element
     * @return
     */
    public boolean insert(T element) {
        Tuple<Double, Double> points = element.getCoordinates();

        if (!bounds.contains(points.getX(), points.getY())) {
            return false;
        }

        // If no subtrees and max elements have not been breached insert element
        if (hasChildren() && assignQuadSpace(element)) {
            return true;
        }

        try {
            writeLock.lock();
            if (nodes == null) {
                nodes = new HashSet<>();
            }
            if (nodes.size() < preferredMaxElements) {
                nodes.add(element);
                return true;
            } else if (maxlevels > 1 && split()) {
                // Add the element
                nodes.add(element);
                moveElements();
                return true;
            }
        } finally {
            writeLock.unlock();
        }
        return true;
    }

    /**
     * Insert key value pair for linear searches.
     *
     * @param key
     * @param element
     * @return
     */
    public boolean insert(Object key, T element) {
        Tuple<Double, Double> points = element.getCoordinates();

        if (!bounds.contains(points.getX(), points.getY())) {
            return false;
        }

        // If no subtrees and max elements have not been breached insert element
        if (hasChildren() && assignQuadSpace(element)) {
            return true;
        }

        try {
            writeLock.lock();
            if (nodeMap.size() < preferredMaxElements) {
                nodeMap.put(key, element);
                return true;
            } else if (maxlevels > 1 && split()) {
                // Add the element
                nodeMap.put(key, element);
                moveElements();
                return true;
            }
        } finally {
            writeLock.unlock();
        }
        return true;
    }


    /**
     * @param key
     * @return
     */
    public boolean remove(Object key) {
        if (key == null) {
            return false;
        }
        if (hasChildren()) {
            if (northWest.remove(key) || northEast.remove(key) || southEast.remove(key)) {
                return true;
            }
            return southWest.remove(key);
        } else {

            try {
                writeLock.lock();
                if (nodeMap != null) {
                    nodeMap.remove(key);
                } else {
                    nodes.remove(key);
                }
            } finally {
                writeLock.unlock();
            }
        }
        return false;
    }

    /**
     * Update quad location for given key and element
     *
     * @param key
     * @param element
     * @return
     */
    public boolean update(Object key, T element) {
        remove(key);
        return insert(key, element);
    }

    /**
     * A single step replace element
     *
     * @param element
     * @return
     */
    public boolean replace(T element, Object key) {
        if (element == null) {
            return false;
        }
        if (hasChildren()) {
            if (northWest.replace(element, key) || northEast.replace(element, key) || southEast.replace(element, key)) {
                return true;
            }
            return southWest.replace(element, key);
        } else {

            try {
                writeLock.lock();
                boolean addElement = false;

                // First determine if point in current quad
                Tuple<Double, Double> points = element.getCoordinates();
                if (bounds.contains(points.getX(), points.getY())) {
                    addElement = true;
                }

                if (nodeMap != null && addElement) {
                    nodeMap.replace(key, element);
                } else {
                    nodes.remove(element);
                    if (addElement) {
                        nodes.add(element);
                    }
                }
            } finally {
                writeLock.unlock();
            }
        }
        return false;
    }

    /**
     * Provide a search area and return any elements that reside within the area.
     *
     * @param searchArea
     * @return Collection<T> of elements.
     */
    public Collection<T> query(final Shape searchArea) {
        final Collection<T> results = Collections.synchronizedSet(new HashSet<>());

        if (hasChildren()) {
            if (northWest != null) {
                results.addAll(northWest.query(searchArea));
            }
            if (northEast != null) {
                results.addAll(northEast.query(searchArea));
            }
            if (southEast != null) {
                results.addAll(southEast.query(searchArea));
            }
            if (southWest != null) {
                results.addAll(southWest.query(searchArea));
            }
        } else if (nodes != null || nodeMap != null) {
            elementSearch(searchArea,results);
        }
        return results;
    }

    private void elementSearch(final Shape searchArea,final Collection<T> results){
        try {
            readLock.lock();
            Collection<T> elementsToSearch = (nodes != null) ? nodes : nodeMap.values();
            if (!elementsToSearch.isEmpty() && bounds.intersects(searchArea)) {
                elementsToSearch.parallelStream().forEach(e -> {
                    Tuple<Double, Double> points = e.getCoordinates();
                    if (searchArea.contains(points.getX(), points.getY())) {
                        results.add(e);
                    }
                });
            }
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Provides a method to search based upon x,y points when the quad tree is loaded using shapes.
     *
     * @param point
     * @return
     */
    public Collection<T> query(final Tuple<Double, Double> point) {
        final Collection<T> results = Collections.synchronizedSet(new HashSet<>());

        if (hasChildren()) {
            results.addAll(northWest.query(point));
            results.addAll(northEast.query(point));
            results.addAll(southEast.query(point));
            results.addAll(southWest.query(point));
        } else {
            try {
                readLock.lock();
                Collection<T> elementsToSearch = (nodes != null) ? nodes : nodeMap.values();
                elementsToSearch.parallelStream().forEach(n -> {
                    if (n instanceof Shape shape && (shape.contains(point.getX(), point.getY()))) {
                        results.add(n);
                    }
                });
            } finally {
                readLock.unlock();
            }
        }
        return results;
    }

    /**
     * Assign element to a quad space.
     *
     * @param element
     * @return
     */
    private boolean assignQuadSpace(final T element) {

        Tuple<Double, Double> elementCoords = element.getCoordinates();

        // Unroll loop
        if (optimisedFindQuad(elementCoords.getX(), elementCoords.getY()).insert(element)) {
            return true;
        }

        // Otherwise default case
        if (northWest.insert(element) || northEast.insert(element) || southEast.insert(element)) {
            return true;
        }
        return southWest.insert(element);
    }

    /**
     * Move elements down to the next level.
     */
    private void moveElements() {
        for (T e : nodes) {
            assignQuadSpace(e);
        }
        nodes = null;
        nodeMap = null;
    }

    private QuadTree<T> optimisedFindQuad(final double x2, final double y2) {
        QuadTree<T> quad;
        Tuple<Double, Double> quadTreeCoords = bounds.getCoordinates();

        // West
        if (quadTreeCoords.getX() <= x2) {
            // North
            if (quadTreeCoords.getY() <= y2) {
                quad = northWest;
            }
            // South
            else {
                quad = southWest;
            }
        }
        // East
        else {
            // North
            if (quadTreeCoords.getY() <= y2) {
                quad = northEast;
            }
            // South
            else {
                quad = southEast;
            }
        }
        return quad;
    }

    /**
     * Subdivide an area in to four new areas
     *
     * @return
     */
    private boolean split() {
        if (hasChildren()) {
            return false;
        }

        // Allows for any other type of shape.
        Shape newShape = bounds.subdivide();
        if (newShape instanceof Rectangle r) {
            int newLevel = maxlevels - 1;

            int maxXOffset = (int) (r.getWidth() + r.getX());
            int maxYOffset = (int) (r.getHeight() + r.getY());
            northWest = new QuadTree<>(r.getX(), r.getY(), r.getWidth(), (int) r.getArea(), newLevel, preferredMaxElements);

            if (r.getX() + r.getWidth() <= maxXOffset) {
                northEast = new QuadTree<>(r.getX() + r.getWidth(), r.getY(), r.getWidth(), (int) r.getArea(), newLevel, preferredMaxElements);
            }

            if (r.getX() + r.getWidth() <= maxXOffset && r.getY() + r.getHeight() <= maxYOffset) {
                southEast = new QuadTree<>(r.getX() + r.getWidth(), r.getY() + r.getHeight(), r.getWidth(), (int) r.getArea(), newLevel, preferredMaxElements);
            }
            southWest = new QuadTree<>(r.getX(), r.getY() + r.getHeight(), r.getWidth(), (int) r.getArea(), newLevel, preferredMaxElements);
            return true;
        }
        return false;
    }

    /**
     * Does this node have any children.
     *
     * @return true if subnodes false otherwise.
     */
    private boolean hasChildren() {
        return (northWest != null && northEast != null && southEast != null && southWest != null);
    }


    @JsonProperty(value = "top left coordinates")
    public void setTopLeftX(double[] point){
        if(point!= null && point.length == 2) {
            this.topLeftX = point[0];
            this.topLeftY = point[1];
        }
    }

    @JsonProperty(value = "topLeftX")
    public void setTopLeftX(double x){
        this.topLeftX = x;
    }

    @JsonProperty(value = "topLeftY")
    public void setTopLeftY(double y){
        this.topLeftY = y;
    }

    @JsonProperty(value = "width")
    public void setWidth(int width){
        this.width = width;
    }

    @JsonProperty(value = "area")
    public void setArea(int area){
        this.area = area;
    }

    /**
     * Get the number of subtree max levels used for precision.
     *
     * @return
     */
    public int getMaxlevels() {
        return maxlevels;
    }

    @JsonProperty(value = "max levels")
    public void setMaxlevels(int maxlevels) {
        this.maxlevels = maxlevels;
    }

    public int getSize() {
        return (nodes != null) ? nodes.size() : 0;
    }

    @JsonProperty(value = "preferred max elements")
    public void setPreferredMaxElements(int preferredMaxElements) {
        this.preferredMaxElements = preferredMaxElements;
    }

    public void validate() throws InvalidSpecificationException {
        if( width <=0) {
            throw new InvalidSpecificationException("width must be greater than zero");
        }

        if( area <= 0){
            throw new InvalidSpecificationException("area must be greater than zero");
        }

        if( maxlevels < 0 && maxlevels > 24 ){
            throw new InvalidSpecificationException("maxLevels must be greater than zero and less than 25");
        }

    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" -> ");

        if (hasChildren()) {
            stringBuilder.append("northWest: ").append(northWest.toString());
            stringBuilder.append("northEast: ").append(northEast.toString());
            stringBuilder.append("southEast: ").append(southEast.toString());
            stringBuilder.append("southWest: ").append(southWest.toString());
        } else if (nodes != null && !nodes.isEmpty()) {
            stringBuilder.append("Bounds -> ").append(bounds).append("\n");
            stringBuilder.append("Elements held: ").append(nodes.size()).append("\n");
            stringBuilder.append("Leaf Nodes\n").append("------------------");
        } else if (nodeMap != null && !nodeMap.isEmpty()) {
            stringBuilder.append("Bounds -> ").append(bounds).append("\n");
            stringBuilder.append("Elements held: ").append(nodeMap.size()).append("\n");
            stringBuilder.append("Leaf Nodes\n").append("------------------");
        }
        return stringBuilder.append("\n").toString();
    }
}
