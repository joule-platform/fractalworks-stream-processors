/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.country;

import java.util.List;
import java.util.Objects;

/**
 * Country domain class
 *
 * @author Lyndon Adams
 */
public class Country {

    String name;
    String capital;
    List<LocalRegion> regions;
    List<Town> towns;

    public Country() {
        // Required
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<LocalRegion> getRegions() {
        return regions;
    }

    public void setRegions(List<LocalRegion> regions) {
        this.regions = regions;
    }

    public List<Town> getTowns() {
        return towns;
    }

    public void setTowns(List<Town> towns) {
        this.towns = towns;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) &&
                Objects.equals(capital, country.capital) &&
                Objects.equals(regions, country.regions) &&
                Objects.equals(towns, country.towns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, capital, regions, towns);
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", regions=" + regions +
                ", towns=" + towns +
                '}';
    }
}
