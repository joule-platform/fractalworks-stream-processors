/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils;

import com.fractalworks.streams.core.data.Triple;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoDirection;
import com.fractalworks.streams.processors.geospatial.types.geometry.Rectangle;
import com.fractalworks.streams.processors.geospatial.types.geometry.Shape;

/**
 * Suite of Geo spatial math algorithms.
 * <p>
 * Created by Lyndon Adams
 */
public class GeoMath {

    private static final float PI_SCALED = (float) (180.0f / Math.PI);
    private static final float PI_180 = (float) Math.PI / 180;
    private static final float PI4X = (float) (4.0f * Math.PI);

    private static final GeoDirection[] coordNames = new GeoDirection[]{GeoDirection.N, GeoDirection.NE, GeoDirection.E, GeoDirection.SE, GeoDirection.S, GeoDirection.SW, GeoDirection.W, GeoDirection.NW, GeoDirection.N};

    private static final float[] cachedDetailLevel;

    static {
        cachedDetailLevel = new float[24];
        for (int i = 1; i < 24; i++) {
            cachedDetailLevel[i - 1] = (float) (256 * Math.exp(i));
        }
    }

    private GeoMath() {
    }

    /**
     * Convert WGS84 latitude/longitude points to quad tree integer points.
     *
     * @param latitude
     * @param longitude
     * @param level
     * @return Tuple<Integer, Integer>
     */
    public static Tuple<Double, Double> convertWGS84toQuadTile(final double latitude, final double longitude, final int level) {

        if (level < 1 && level > 23) {
            return null;
        }

        double detailLevel = cachedDetailLevel[level - 1];
        double sinLatitude = Math.sin(latitude * PI_180);

        double pixelX = ((longitude + 180) / 360) * detailLevel;
        double pixelY = (0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / PI4X) * detailLevel;

        double tileX = Math.floor(pixelX / 256);
        double tileY = Math.floor(pixelY / 256);
        return new Tuple<>(tileX, tileY);
    }

    /**
     * Get the meter distance between two spatial points.
     *
     * @param latA Point 1 latitude
     * @param lngA Point 1 longitude
     * @param latB Point 2 latitude
     * @param lngB Point 2 longitude
     * @return meters as a double value.
     */
    public static double distanceBetweenPointsByMeter(final double latA, final double lngA, final double latB, final double lngB) {
        double a1 = latA / PI_SCALED;
        double a2 = lngA / PI_SCALED;
        double b1 = latB / PI_SCALED;
        double b2 = lngB / PI_SCALED;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    /**
     * Determine if passed area is a perfect square.
     *
     * @param area
     * @return
     */
    public static boolean isPerfectSquare(long area) {
        if (area < 0) {
            return false;
        }

        switch ((int) (area & 0x3F)) {
            case 0x00, 0x01,0x04, 0x09, 0x10,0x11, 0x19,0x21,0x24, 0x29,0x31, 0x39:
                long sqrt;
                if (area < 410881L) {
                    int i;
                    float x2;
                    float y;

                    x2 = area * 0.5F;
                    y = area;
                    i = Float.floatToRawIntBits(y);
                    i = 0x5f3759df - (i >> 1);
                    y = Float.intBitsToFloat(i);
                    y = y * (1.5F - (x2 * y * y));

                    sqrt = (long) (1.0F / y);
                } else {
                    //Carmack hack gives incorrect answer for n >= 410881.
                    sqrt = (long) Math.sqrt(area);
                }
                return sqrt * sqrt == area;

            default:
                return false;
        }
    }

    /**
     * Get speed bewteen two point using passed time between points.
     *
     * @param latA Point 1 latitude
     * @param lngA Point 1 longitude
     * @param latB Point 2 latitude
     * @param lngB Point 2 longitude
     * @param time
     * @return
     */
    public static int getSpeedBetweenPoints(final float latA, final float lngA, final float latB, final float lngB, int time) {
        double distance = distanceBetweenPointsByMeter(latA, lngA, latB, lngB);
        return (int) distance / time;
    }

    /**
     * @param latStart Point 1 latitude
     * @param lngStart Point 1 longitude
     * @param latEnd   Point 2 latitude
     * @param lngEnd   Point 2 longitude
     * @param seconds
     * @return
     */
    public static Triple<Double, GeoDirection, Double> getSpeedAndDirectionBetweenPoints(final double latStart, final double lngStart, final double latEnd, final double lngEnd, long seconds) {
        double distance = distanceBetweenPointsByMeter(latStart, lngStart, latEnd, lngEnd);
        double speed = distance / seconds;

        // Direction
        double radians = Math.atan2((lngEnd - lngStart), (latEnd - latStart));
        double degrees = radians * PI_SCALED;

        int coordIndex = (int) Math.round(degrees / 45);
        if (coordIndex < 0) {
            coordIndex = coordIndex + 8;
        }
        return new Triple<>(speed, coordNames[coordIndex], degrees);
    }

    /**
     * Creat a bounding box using the passed x,y coordinates and width.
     *
     * @param x
     * @param y
     * @param width
     * @return
     */
    public static Shape createBoundingBox(final double x, final double y, final int width) {
        return new Rectangle(x - width / 2.0f, y - width / 2.0f, width * width, width);
    }
}
