/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 *
 */
@JsonRootName(value = "geoFence")
public class GeoFencePOJO {
    private int id;
    private String[] coordinates;
    private float radius;

    public GeoFencePOJO() {
        // REQUIRED
    }

    public int getId() {
        return id;
    }

    @JsonProperty(value = "id",required = true)
    public void setId(int id) {
        this.id = id;
    }

    public String[] getCoordinates() {
        return coordinates;
    }

    @JsonProperty(value = "coordinates",required = true)
    public void setCoordinates(String[] coordinates) {
        this.coordinates = coordinates;
    }

    public float getRadius() {
        return radius;
    }

    @JsonProperty(value = "radius",required = true)
    public void setRadius(float radius) {
        this.radius = radius;
    }
}
