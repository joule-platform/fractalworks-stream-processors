/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geometry;


import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.google.common.base.Objects;

/**
 * Rectangle implementation of a Shape.
 */
public class Rectangle implements Shape {

    private final double x;
    private final double y;
    private final double area;
    private final int width;
    private final int height;

    private final Tuple<Double, Double> rectangleCenter;

    /**
     * @param x
     * @param y
     * @param x2
     * @param y2
     */
    public Rectangle(double x, double y, double x2, double y2) {
        this.x = x;
        this.y = y;

        this.width = (int) (Math.abs(x) + Math.abs(x2));
        this.height = (int) (Math.abs(y) + Math.abs(y2));
        this.area = (double) width * height;
        rectangleCenter = new Tuple<>(this.x + this.width / 2.0f, this.y + this.height / 2.0f);
    }

    /**
     * Default constructor.
     *
     * @param x
     * @param y
     * @param area
     * @param width
     */
    public Rectangle(double x, double y, int area, int width) {
        this.x = x;
        this.y = y;
        this.area = area;
        this.width = width;

        int pw = (int) Math.ceil(Math.sqrt(area));
        if ((pw * pw) == area) {
            height = width;
        } else {
            height = area / width;
        }
        rectangleCenter = new Tuple<>(this.x + this.width / 2.0f, this.y + this.height / 2.0f);
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public boolean intersects(Shape shape) {
        if (shape instanceof Rectangle rectangle) {
            return intersectsRectangle(rectangle);
        } else if (shape instanceof GeoFence geoFence) {
            return intersectsGeoFence(geoFence);
        }
        return false;
    }

    private boolean intersectsGeoFence(final GeoFence f) {


        double w = this.width / 2f;
        double h = this.height / 2f;

        double dx = Math.abs(f.getCentreX() - rectangleCenter.getX());
        double dy = Math.abs(f.getCentreY() - rectangleCenter.getY());

        if (dx > (f.getRadius() + w) || dy > (f.getRadius() + h)) {
            return false;
        }


        Tuple<Double, Double> circleDistance = new Tuple<>(
                Math.abs(f.getCentreX() - this.x - w),
                Math.abs(f.getCentreY() - this.y - h));


        if (circleDistance.getX() <= w) {
            return true;
        }

        if (circleDistance.getY() <= h) {
            return true;
        }

        double cornerDistanceSq = Math.pow(circleDistance.getX() - w, 2) + Math.pow(circleDistance.getY() - h, 2);

        return cornerDistanceSq <= Math.pow(f.getRadius(), 2);
    }


    private boolean intersectsRectangle(final Rectangle r) {
        int tw = this.width;
        int th = this.height;
        int rw = r.width;
        int rh = r.height;
        if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
            return false;
        }
        double tx = this.x;
        double ty = this.y;
        double rx = r.x;
        double ry = r.y;
        rw += rx;
        rh += ry;
        tw += tx;
        th += ty;
        //      overflow || intersect
        return (rw < rx || rw > tx) &&
                (rh < ry || rh > ty) &&
                (tw < tx || tw > rx) &&
                (th < ty || th > ry);

    }

    @Override
    public boolean contains(double px, double py) {
        int w = width;
        int h = height;
        if ((w | h) < 0) {
            // At least one of the dimensions is negative...
            return false;
        }
        // Note: if either dimension is zero, tests below must return false...
        if (px < x || py < y) {
            return false;
        }
        w += x;
        h += y;
        //    overflow || intersect
        return (w < x || w > px) &&
                (h < y || h > py);
    }

    @Override
    public Tuple<Double, Double> getCoordinates() {
        return new Tuple<>(x, y);
    }


    /**
     * Provide clockwise points
     *
     * @return
     */
    @Override
    public Shape subdivide() {
        int newWidth;
        int newArea;

        // Perfect square
        if (width >= height) {
            newWidth = width / 2;
            newArea = newWidth * height;
        } else {
            // width < height
            newArea = width * (height / 2);
            newWidth = width;
        }
        return newWidth > 1 ? new Rectangle(x, y, newArea, newWidth) : null;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", area=" + area +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rectangle)) {
            return false;
        }

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.x, x) != 0) {
            return false;
        }
        if (Double.compare(rectangle.y, y) != 0) {
            return false;
        }
        if (Double.compare(rectangle.area, area) != 0) {
            return false;
        }
        if (width != rectangle.width) {
            return false;
        }
        return height == rectangle.height;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(x, y, area, width, height, rectangleCenter);
    }
}
