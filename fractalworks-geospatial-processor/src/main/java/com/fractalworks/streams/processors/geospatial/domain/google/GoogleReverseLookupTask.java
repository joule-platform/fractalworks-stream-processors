/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.google;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Calls Google APi to get location details using lat/lng tuple.
 * <p>
 * Uses the Google GeoCoding API (https://developers.google.com/maps/documentation/geocoding/overview)
 *
 * Example URL http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true
 * Created by Lyndon Adams
 */
public class GoogleReverseLookupTask implements Callable<GoogleGeoCoordinates> {

    private static final CloseableHttpClient httpclient = HttpClients.createMinimal();
    private static final String GOOGLEAPI_WITHKEY = "key=%s";
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private String googleAPI = "http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=true";
    private static final String SECURE_GOOGLE_API = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=true";
    private final double lat;
    private final double lng;
    private final ObjectMapper mapper = new ObjectMapper();

    public GoogleReverseLookupTask(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    /**
     * Set Google API key
     *
     * @param apiKey
     */
    public void setApiKey(String apiKey) {
        if (apiKey != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(SECURE_GOOGLE_API).append("&").append(String.format(GOOGLEAPI_WITHKEY, apiKey));
            googleAPI = builder.toString();
        }
    }

    @Override
    public GoogleGeoCoordinates call() throws Exception {

        Thread thread = Thread.currentThread();

        HttpGet httpget = new HttpGet(String.format(googleAPI, lat, lng));
        CloseableHttpResponse response = null;
        GoogleGeoCoordinates googleGeoLocation = null;
        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                googleGeoLocation = mapper.readValue(EntityUtils.toString(entity), GoogleGeoCoordinates.class);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, String.format("Task {%d} failed during google reverse geo lookup processing.", thread.getId()), e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, String.format("Task {%d} failed during google reverse geo lookup stream close process.", thread.getId()), e);
            }
        }
        return googleGeoLocation;
    }
}
