/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.ip;

import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.referencedata.serialization.ReferenceDataArrowParser;
import com.google.common.base.Splitter;
import org.apache.arrow.vector.FieldVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.jetbrains.annotations.NotNull;

public class IpCityArrowParser implements ReferenceDataArrowParser<IpToCity> {

    private static final String DOT_DELIMITER = ".";
    private static final Splitter IP_ADDRESS_SPLITTER = Splitter.on(DOT_DELIMITER);

    @Override
    public IpToCity translate(@NotNull VectorSchemaRoot row) throws TranslationException {
        IpToCity ipToCity= new IpToCity();
        for(FieldVector v : row.getFieldVectors()){
            var str = v.getObject(0).toString().strip();
            switch (v.getName().toLowerCase() ){
                case "fromip" ->  ipToCity.setFromIP( parseIPAddressToComponents(str));
                case "toip" -> ipToCity.setToIP( parseIPAddressToComponents(str));
                case "country" -> ipToCity.setCountry(str);
                case "area" -> ipToCity.setArea(str);
                case "city" -> ipToCity.setCity(str);
                default -> throw new TranslationException(String.format("Unknown field '%s'", v.getName()));
            }
        }
        ipToCity.ipRanges();
        return ipToCity;
    }

    private short[] parseIPAddressToComponents(String ipaddress) throws TranslationException {
        short[] asShort = new short[4];
        try {
            var components = IP_ADDRESS_SPLITTER.splitToList(ipaddress);
            for (int i = 0; i < 4; i++) {
                asShort[i] = Short.parseShort(components.get(i));
            }
        } catch (NumberFormatException e) {
            throw new TranslationException("Failure parsing IP to shorts.", e);
        }
        return asShort;
    }
}
