/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils.jackson;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fractalworks.streams.processors.geospatial.types.geometry.Point;

public class GeoJsonPointObjectMapper extends ObjectMapper {

    private static final String GROUP_ID = "com.fractalworks.streams.processors";
    private static final String ARTIFACT_ID = "geospatial-processor";

    public GeoJsonPointObjectMapper() {
        SimpleModule module = new SimpleModule("GeoJsonPointsModule", new Version(0, 0, 1, "alpha", GROUP_ID, ARTIFACT_ID));
        module.addSerializer(Point.class, new PointSerializer());
        module.addDeserializer(Point.class, new PointDeserializer());
        registerModule(module);
    }

}
