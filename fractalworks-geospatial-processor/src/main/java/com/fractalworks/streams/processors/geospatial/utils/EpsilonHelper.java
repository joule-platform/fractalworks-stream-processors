/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils;

import com.fractalworks.streams.processors.geospatial.types.geometry.Line;
import com.fractalworks.streams.processors.geospatial.types.geometry.Point;

import java.util.List;

public class EpsilonHelper {

    private EpsilonHelper() {
        // Hide constructor
    }

    /**
     * For each 3 consecutive points in the list this function calculates the distance
     * from the middle point to a line defined by the first and third point.
     * <p>
     * The result may be used to find a proper epsilon by calculating
     * maximum {@link #max(double[])} or average {@link #avg(double[])} from
     * all deviations.
     */
    public static <P extends Point> double[] deviations(List<P> points) {
        double[] deviations = new double[Math.max(0, points.size() - 2)];
        for (int i = 2; i < points.size(); i++) {
            P p1 = points.get(i - 2);
            P p2 = points.get(i - 1);
            P p3 = points.get(i);
            double dev = new Line<>(p1, p3).distance(p2);
            deviations[i - 2] = dev;
        }
        return deviations;
    }

    public static double sum(double[] values) {
        double sum = 0.0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum;
    }


    public static double avg(double[] values) {
        if (values.length > 0) {
            return sum(values) / values.length;
        } else {
            return 0.0;
        }
    }

    public static double max(double[] values) {
        double max = 0.0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        return max;
    }
}
