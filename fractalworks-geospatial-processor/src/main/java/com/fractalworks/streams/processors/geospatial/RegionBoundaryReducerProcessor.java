/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.processors.geospatial.types.geometry.Polygon;
import com.fractalworks.streams.processors.geospatial.types.country.LocalRegion;
import com.fractalworks.streams.processors.geospatial.utils.PolygonService;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Region boundary points reducer. Ideal for optimising geoIntesects calculations. Looks up a <code>LocalRegion</code> object using the
 * LOCAL_REGION_KEY against the passed <code>StreamEvent</code> to perform the calculation
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "regionBoundaryReducer")
public class RegionBoundaryReducerProcessor extends AbstractProcessor {


    public static final String LOCAL_REGION_KEY = "localRegion";

    private PolygonService polygonService = new PolygonService();

    public RegionBoundaryReducerProcessor() {
        logger = LoggerFactory.getLogger(RegionBoundaryReducerProcessor.class);
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        if (event.contains(LOCAL_REGION_KEY)) {
            LocalRegion localRegion = (LocalRegion) event.getValue(LOCAL_REGION_KEY);

            int initialPoints = localRegion.getBoundary().getExteriorRing().size();

            Polygon updatedBoundry = polygonService.optimisePolygonExternalRing(localRegion.getBoundary());

            if (updatedBoundry != null) {
                localRegion.setBoundary(updatedBoundry);
                if (logger.isInfoEnabled()) {
                    logger.info("Updated {} from {} points to {} points.", localRegion.getAreaName(), initialPoints, localRegion.getBoundary().getExteriorRing().size());
                }
            }
            metrics.incrementMetric(Metric.PROCESSED);
        } else {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        }
        return event;
    }

    /**
     * Override Polygon service with another implementation
     *
     * @param polygonService
     */
    public void setPolygonService(@Nonnull PolygonService polygonService) {
        this.polygonService = polygonService;
    }
}
