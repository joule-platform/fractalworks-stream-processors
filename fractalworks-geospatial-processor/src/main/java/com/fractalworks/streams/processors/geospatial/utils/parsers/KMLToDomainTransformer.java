/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils.parsers;

import com.fractalworks.streams.processors.geospatial.types.geo.GeoJsonPolygonFactory;
import com.fractalworks.streams.processors.geospatial.types.country.LocalRegion;
import de.micromata.opengis.kml.v_2_2_0.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Build a list of local regions from a passed KML file.
 *
 * @author Lyndon Adams
 */
public final class KMLToDomainTransformer {

    Logger logger = LoggerFactory.getLogger(KMLToDomainTransformer.class);

    /**
     * Parse a Kml file using the de.micromata.opengis lib to internal domain objects.
     *
     * @param kmlfile
     * @return List<LocalRegion> which represent local areas.
     */
    public List<LocalRegion> transformKmlToLocalRegions(final File kmlfile) {
        List<LocalRegion> boroughs = new LinkedList<>();
        Kml kml = Kml.unmarshal(kmlfile, false);

        Document doc = (Document) kml.getFeature();
        List<Feature> features = doc.getFeature();

        logger.info("Loaded {} local region polygons from {} file.", features.size(), kmlfile.getName());

        for (Feature feature : features) {

            LocalRegion region = new LocalRegion();
            Placemark placemark = (Placemark) feature;
            Polygon poly = (Polygon) placemark.getGeometry();

            Boundary boundary = poly.getOuterBoundaryIs();
            LinearRing linearRing = boundary.getLinearRing();
            List<Coordinate> coordinates = linearRing.getCoordinates();

            region.setAreaName(placemark.getName());
            region.setBoundary(GeoJsonPolygonFactory.createPolygon(coordinates));

            boroughs.add(region);
        }

        logger.info("Created {} named local regions objects from {} file.", boroughs.size(), kmlfile.getName());

        return boroughs;
    }
}
