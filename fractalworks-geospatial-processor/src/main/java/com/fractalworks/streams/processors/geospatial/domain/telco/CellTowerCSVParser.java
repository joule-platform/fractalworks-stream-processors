/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;

import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.referencedata.serialization.ReferenceDataCSVParser;
import com.google.common.base.Splitter;

import java.util.Iterator;

/**
 * CellTower CSV row parser.
 * <p>
 * Format
 * - radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal
 * <p>
 * Created by Lyndon Adams
 */
public class CellTowerCSVParser implements ReferenceDataCSVParser<CellTower> {

    private static final String COMMA_REG_EX = ",";

    /**
     * Parse provided line to CellTower.
     *
     * @param obj
     * @return CellTower
     */
    @Override
    public CellTower translate(String obj) throws TranslationException {
        CellTower cellTower = null;
        Splitter splitter = Splitter.on(COMMA_REG_EX);
        Iterator<String> it = splitter.split(obj).iterator();
        if (it.hasNext()) {
            RadioType radioType = RadioType.getEnum(it.next().intern());
            int mmc = Integer.parseInt(it.next());
            int net = Integer.parseInt(it.next());
            int area = Integer.parseInt(it.next());
            int cell = Integer.parseInt(it.next());

            String unitStr = it.next();
            int unit = (!unitStr.isEmpty()) ? Integer.valueOf(unitStr) : -1;

            double lon = Double.parseDouble(it.next());
            double lat = Double.parseDouble(it.next());
            int range = Integer.parseInt(it.next());
            int samples = Integer.parseInt(it.next());
            int changeable = Integer.parseInt(it.next());
            long created = Long.parseLong(it.next());
            long updated = Long.parseLong(it.next());

            String averageSignalStr = it.next();
            int averageSignal = !averageSignalStr.isEmpty() ? Integer.parseInt(averageSignalStr) : -1;
            cellTower = new CellTower();
            cellTower.setRadioType(radioType);
            cellTower.setMcc(mmc);
            cellTower.setNet(net);
            cellTower.setArea(area);
            cellTower.setCell(cell);
            cellTower.setUnit(unit);
            cellTower.setLon(lon);
            cellTower.setLat(lat);
            cellTower.setRange(range);
            cellTower.setSamples(samples);
            cellTower.setChangeable(changeable);
            cellTower.setCreated(created);
            cellTower.setUpdated(updated);
            cellTower.setAverageSignal(averageSignal);
        }
        return cellTower;
    }

}
