/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geometry;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fractalworks.streams.processors.geospatial.utils.jackson.PointDeserializer;
import com.fractalworks.streams.processors.geospatial.utils.jackson.PointSerializer;

import java.util.Arrays;

/**
 * GeoJson Point data structure. Longitude, latitude and altitude are set
 * and accessed by corresponding (x,y,z) setters and getters.
 *
 * @author Lyndon Adams
 */
@JsonDeserialize(using = PointDeserializer.class)
@JsonSerialize(using = PointSerializer.class)
public class Point {

    double[] coordinates;

    public Point() {
        super();
        coordinates = new double[3];
    }

    public Point(double[] points) {
        this.coordinates = points;
    }

    public Point(double x, double y) {
        this();
        coordinates[0] = x;
        coordinates[1] = y;
    }

    public Point(double x, double y, double z) {
        this(x, y);
        coordinates[2] = z;
    }

    public void setX(double x) {
        coordinates[0] = x;
    }

    public void setY(double y) {
        coordinates[1] = y;
    }

    public void setZ(double z) {
        coordinates[2] = z;
    }


    public double getX() {
        return coordinates[0];
    }

    public double getY() {
        return coordinates[1];
    }

    public double getZ() {
        return coordinates[2];
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public double[] gecCordinates() {
        return coordinates;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return Arrays.equals(coordinates, point.coordinates);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coordinates);
    }

    @Override
    public String toString() {
        return "Point{" +
                "points[x,y,z]=" + Arrays.toString(coordinates) +
                '}';
    }
}
