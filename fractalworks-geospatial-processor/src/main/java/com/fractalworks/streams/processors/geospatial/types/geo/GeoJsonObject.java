/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fractalworks.streams.processors.geospatial.types.geometry.*;
import gnu.trove.map.TMap;
import gnu.trove.map.hash.THashMap;

import java.util.Map;

@JsonTypeInfo(property = "type", use = Id.NAME)
@JsonSubTypes({
        @Type(Feature.class), @Type(Polygon.class), @Type(MultiPolygon.class), @Type(FeatureCollection.class),
        @Type(MultiPoint.class), @Type(MultiLineString.class), @Type(LineString.class)
})
@JsonInclude(Include.NON_NULL)
public abstract class GeoJsonObject {

    private Crs crs;
    private double[] bbox;
    @JsonInclude(Include.NON_EMPTY)
    private TMap<String, Object> properties;

    public Crs getCrs() {
        return crs;
    }

    public void setCrs(Crs crs) {
        this.crs = crs;
    }

    public double[] getBbox() {
        return bbox;
    }

    public void setBbox(double[] bbox) {
        this.bbox = bbox;
    }

    public void setProperty(String key, Object value) {
        if (properties == null) {
            properties = new THashMap<>();
        }
        properties.put(key, value);
    }

    @SuppressWarnings("unchecked")
    public <T> T getProperty(String key) {
        return (properties != null) ? (T) properties.get(key) : null;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(TMap<String, Object> properties) {
        this.properties = properties;
    }
}
