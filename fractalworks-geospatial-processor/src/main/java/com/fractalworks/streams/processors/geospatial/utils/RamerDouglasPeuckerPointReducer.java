/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils;

import com.fractalworks.streams.processors.geospatial.types.geometry.Line;
import com.fractalworks.streams.processors.geospatial.types.geometry.Point;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;


/**
 * Filters data using Ramer-Douglas-Peucker algorithm with specified tolerance.
 *
 * @author Lyndon Adams, Rzeźnik
 * @see <a href="http://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm">Ramer-Douglas-Peucker algorithm</a>
 */
public class RamerDouglasPeuckerPointReducer {

    private RamerDouglasPeuckerPointReducer() {
        // Required
    }

    /**
     * Reduces number of points in given series using Ramer-Douglas-Peucker algorithm.
     *
     * @param points  initial, ordered list of points (objects implementing the {@link Point} interface)
     * @param epsilon allowed margin of the resulting curve, has to be > 0
     */
    public static <P extends Point> List<P> reduce(@Nonnull List<P> points, double epsilon) {
        if (epsilon < 0) {
            throw new IllegalArgumentException("Epsilon cannot be less then 0.");
        }

        double furthestPointDistance = 0.0;
        int furthestPointIndex = 0;
        int psize = 0;

        psize = points.size();
        Point start = points.get(0);
        Point end = points.get(psize - 1);

        Line<Point> line = new Line<>(start, end);
        for (int i = 1; i < psize - 1; i++) {
            double distance = line.distance(points.get(i));
            if (distance > furthestPointDistance) {
                furthestPointDistance = distance;
                furthestPointIndex = i;
            }
        }

        if (furthestPointDistance > epsilon) {
            List<P> reduced1 = reduce(points.subList(0, furthestPointIndex + 1), epsilon);
            List<P> reduced2 = reduce(points.subList(furthestPointIndex, psize), epsilon);
            List<P> result = new ArrayList<>(reduced1);
            result.addAll(reduced2.subList(1, reduced2.size()));
            return result;
        } else {
            return (List<P>) line.asList();
        }
    }
}
