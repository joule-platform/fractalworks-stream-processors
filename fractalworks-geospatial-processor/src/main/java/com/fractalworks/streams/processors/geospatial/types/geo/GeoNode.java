/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geometry.Coordinates;
import com.fractalworks.streams.sdk.referencedata.ReferenceData;

import java.util.Objects;

/**
 * Geo node using latitude and longitude with
 * a <code>{@link ReferenceData}</code> object
 */
public class GeoNode<T extends ReferenceData>
        implements Coordinates, ReferenceData {

    protected Tuple<Double, Double> coordinates;
    private T value;

    public GeoNode() {
    }

    public GeoNode(double latitude, double longitude) {
        coordinates = new Tuple<>(latitude, longitude);
    }

    public GeoNode(double latitude, double longitude, T node) {
        this(latitude, longitude);
        this.value = node;
    }

    @Override
    public Object getKey() {
        return value.getKey();
    }

    @Override
    public GeoNode getData() {
        return this;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public Tuple<Double, Double> getCoordinates() {
        return coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoNode)) return false;
        GeoNode<?> geoNode = (GeoNode<?>) o;
        return Objects.equals(coordinates, geoNode.coordinates) &&
                Objects.equals(value, geoNode.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates, value);
    }

    @Override
    public String toString() {
        return "GeoNode{" +
                "points=" + coordinates +
                ", node=" + value +
                '}';
    }
}
