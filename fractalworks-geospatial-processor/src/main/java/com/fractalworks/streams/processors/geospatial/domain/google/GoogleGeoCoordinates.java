/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.google;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoCoordinates;

import java.util.Arrays;
import java.util.Objects;

/**
 * Google data structure from a reverse geo lookup. Json full location detail response returned
 * from the Google API call.
 * <p>
 * Created by Lyndon Adams
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "plus_code",
        "results",
        "status"
})
public class GoogleGeoCoordinates implements GeoCoordinates {


    private PlusCode plusCode;
    private String status;
    private LocationByType[] locationByType;

    // Internal variable
    private Tuple<Double, Double> coordinate;

    /**
     * Default constructor required for serialization.
     */
    public GoogleGeoCoordinates() {
        // Required for serialization
    }

    public LocationByType[] getLocationByType() {
        return locationByType;
    }

    public PlusCode getPlusCode() {
        return plusCode;
    }

    @JsonProperty("plus_code")
    public void setPlusCode(PlusCode plusCode) {
        this.plusCode = plusCode;
    }

    @JsonProperty("results")
    public void setLocationByType(LocationByType[] locationByType) {
        this.locationByType = locationByType;
    }

    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Tuple<Double, Double> getCoordinates() {
        if (coordinate == null && locationByType != null && locationByType.length > 0) {
            Location location = locationByType[0].getGeometry().getLocation();
            coordinate = new Tuple<>(Double.parseDouble(location.getLat()), Double.parseDouble((location.getLng())));
        }
        return coordinate;
    }

    @Override
    public String getFullAddress() {
        return (locationByType != null && locationByType.length > 0) ? locationByType[0].getFormattedAddress() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoogleGeoCoordinates)) return false;
        GoogleGeoCoordinates that = (GoogleGeoCoordinates) o;
        return Objects.equals(plusCode, that.plusCode) &&
                Objects.equals(status, that.status) &&
                Arrays.equals(locationByType, that.locationByType) &&
                Objects.equals(coordinate, that.coordinate);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(plusCode, status, coordinate);
        result = 31 * result + Arrays.hashCode(locationByType);
        return result;
    }
}