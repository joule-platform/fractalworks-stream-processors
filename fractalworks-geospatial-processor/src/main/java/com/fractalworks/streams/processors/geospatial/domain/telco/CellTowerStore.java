/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.storage.StoreType;

import java.util.EnumMap;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * CellTower map based upon the <code>{@link RadioType}</code> enumeration type
 * <p>
 * Created by Lyndon Adams
 */
@JsonRootName(value = "cellTowerStore")
public class CellTowerStore extends EnumMap<RadioType, QuadTree<GeoNode>> implements Store<CellTower> {

    private final AtomicInteger recordCount = new AtomicInteger();
    private int gridHeight = 1024;
    private int gridWidth = 1024;
    private int maxElementsPerLevel = 25;
    private int treeLevels = 6;

    public CellTowerStore() {
        super(RadioType.class);
    }

    public CellTowerStore(int gridWidth, int gridHeight) {
        this();
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;
    }

    @Override
    public void initialize() {
        for (RadioType rt : RadioType.values()) {
            put(rt, new QuadTree<>(gridWidth, gridHeight, treeLevels, maxElementsPerLevel));
        }
    }

    @Override
    public boolean load(CellTower cell) {
        boolean success = get(cell.getRadioType()).insert(cell);
        if (success) {
            recordCount.incrementAndGet();
        }
        return success;
    }

    /**
     * Get a RadioType based quadtree
     *
     * @param type
     * @return
     */
    public QuadTree<GeoNode> getRadioTypeTree(RadioType type) {
        return get(type);
    }


    public int getMaxElementsPerLevel() {
        return maxElementsPerLevel;
    }

    @JsonProperty(value = "maxElementsPerLevel")
    public void setMaxElementsPerLevel(int maxElementsPerLevel) {
        this.maxElementsPerLevel = maxElementsPerLevel;
    }

    public int getTreeLevels() {
        return treeLevels;
    }

    @JsonProperty(value = "treeLevels")
    public void setTreeLevels(int treeLevels) {
        this.treeLevels = treeLevels;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    @JsonProperty(value = "gridHeight")
    public void setGridHeight(int gridHeight) {
        this.gridHeight = gridHeight;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    @JsonProperty(value = "gridWidth")
    public void setGridWidth(int gridWidth) {
        this.gridWidth = gridWidth;
    }

    public long getRecordCount() {
        return recordCount.get();
    }

    @Override
    public StoreType getStoreType() {
        return StoreType.USER_DEFINED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CellTowerStore)) return false;
        if (!super.equals(o)) return false;
        CellTowerStore that = (CellTowerStore) o;
        return gridHeight == that.gridHeight &&
                maxElementsPerLevel == that.maxElementsPerLevel &&
                treeLevels == that.treeLevels &&
                gridWidth == that.gridWidth;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gridHeight, maxElementsPerLevel, treeLevels, gridWidth);
    }

    /*
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<RadioType,QuadTree<CellTower>> t : entrySet()) {
            sb.append(t.getKey()).append(" -> ").append(t.getValue()).append("\n");
        }
        return sb.toString();
    }

     */
}
