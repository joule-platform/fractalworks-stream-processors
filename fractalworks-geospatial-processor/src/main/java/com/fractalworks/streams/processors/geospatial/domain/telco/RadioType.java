/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;

/**
 * Radio type for mobile network
 * <p>
 * Created by Lyndon Adams
 */
public enum RadioType {

    GSM("GSM"), UMTS("UMTS"), LTE("LTE"), CDMA("CDMA"), UNKNOWN("UNKNOWN");

    private final String type;

    RadioType(String type) {
        this.type = type.intern();
    }


    public static RadioType getEnum(String type) {
        RadioType t;
        switch (type.toUpperCase()) {
            case "GSM":
                t = RadioType.GSM;
                break;
            case "UMTS":
                t = UMTS;
                break;
            case "LTE":
                t = LTE;
                break;
            case "CDMA":
                t = CDMA;
                break;
            default:
                t = UNKNOWN;
                break;
        }
        return t;
    }

    @Override
    public String toString() {
        return type;
    }
}
