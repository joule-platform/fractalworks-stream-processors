/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.telco;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.processors.geospatial.utils.GeoMath;

import java.util.Objects;

/**
 * CellTower data structure
 * <p>
 * Format
 * - radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal
 * <p>
 * Created by Lyndon Adams
 */
public class CellTower extends GeoNode {

    private RadioType radioType;
    private int mcc;
    private int net;
    private int area;
    private int cell;
    private int unit;
    private double lon;
    private double lat;
    private int range;
    private int samples;
    private int changeable;
    private long created;
    private long updated;
    private int averageSignal;

    private CellTowerKey key;

    public CellTower() {
        // Default implementation
    }

    public RadioType getRadioType() {
        return radioType;
    }

    public void setRadioType(RadioType radioType) {
        this.radioType = radioType;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getNet() {
        return net;
    }

    public void setNet(int net) {
        this.net = net;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getCell() {
        return cell;
    }

    public void setCell(int cell) {
        this.cell = cell;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Tuple<Double, Double> getLatLon() {
        return new Tuple<>(lat, lon);
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public int getChangeable() {
        return changeable;
    }

    public void setChangeable(int changeable) {
        this.changeable = changeable;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public int getAverageSignal() {
        return averageSignal;
    }

    public void setAverageSignal(int averageSignal) {
        this.averageSignal = averageSignal;
    }

    @Override
    public CellTowerKey getKey() {
        if (key == null) {
            key = new CellTowerKey(area, net, cell, mcc);
        }
        return key;
    }

    @Override
    public CellTower getData() {
        return this;
    }

    @Override
    public Tuple<Double, Double> getCoordinates() {
        if (coordinates == null) {
            coordinates = GeoMath.convertWGS84toQuadTile(lat, lon, 6);
        }
        return coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CellTower)) return false;
        CellTower cellTower = (CellTower) o;
        return mcc == cellTower.mcc &&
                net == cellTower.net &&
                area == cellTower.area &&
                cell == cellTower.cell &&
                unit == cellTower.unit &&
                Double.compare(cellTower.lon, lon) == 0 &&
                Double.compare(cellTower.lat, lat) == 0 &&
                range == cellTower.range &&
                samples == cellTower.samples &&
                changeable == cellTower.changeable &&
                created == cellTower.created &&
                updated == cellTower.updated &&
                averageSignal == cellTower.averageSignal &&
                radioType == cellTower.radioType &&
                Objects.equals(coordinates, cellTower.coordinates) &&
                Objects.equals(key, cellTower.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(radioType, mcc, net, area, cell, unit, lon, lat, range, samples, changeable, created, updated, averageSignal, coordinates, key);
    }

    @Override
    public String toString() {
        return "CellTower{" +
                "radioType=" + radioType +
                ", mcc=" + mcc +
                ", net=" + net +
                ", area=" + area +
                ", cell=" + cell +
                ", unit=" + unit +
                ", lon=" + lon +
                ", lat=" + lat +
                ", range=" + range +
                ", samples=" + samples +
                ", changeable=" + changeable +
                ", created=" + created +
                ", updated=" + updated +
                ", averageSignal=" + averageSignal +
                ", coordinates=" + coordinates +
                ", key=" + key +
                '}';
    }
}
