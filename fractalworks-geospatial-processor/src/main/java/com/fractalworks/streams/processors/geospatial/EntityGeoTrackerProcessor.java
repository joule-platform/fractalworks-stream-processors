/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFencePOJO;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoTrackingInfo;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import com.gs.collections.impl.map.mutable.ConcurrentHashMap;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Stateful based geo tracking using geofencing. State is held per tracking object for entry, dwelling and exit.
 * As events enter, dwell and exit geofence an alert are generated. The event is tracked once it has entered a geofence
 */
@JsonRootName(value = "entity geo tracker")
public class EntityGeoTrackerProcessor extends AbstractProcessor {

    @JsonIgnore
    private final Map<Object, GeoTrackingInfo> state = new ConcurrentHashMap<>();

    private TimeUnit timeUnit = TimeUnit.SECONDS;
    private long minDwellingTime = 15;

    @JsonIgnore
    private QuadTree<GeoFence> geoFenceSearchTree = new QuadTree<>(-180f, -90f, 360, 64800);

    private final Map<Integer,GeoFence> geofences = new ConcurrentHashMap<>();
    private String trackingEventKey = "id";
    private String responseField = "geoTrackingInfo";
    private String latitudeField = "latitude";
    private String longitudeField = "longitude";
    private float geofenceRadius = 4.0f;


    public EntityGeoTrackerProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        geoFenceSearchTree.initialise();

        // Initialise, add geofences to quad tree and apply change listeners
        for (GeoFence f : geofences.values()) {
            addGeoFence(f);
        }

        if( dataStores != null){
            Map<String, ? extends DataStoreQueryInterface<?>> ds = getDataStores();
            for(Map.Entry<String, ? extends DataStoreQueryInterface<?>> e : ds.entrySet() ) {
                loadGeoFenceData( e.getValue());
            }
        }
        if( logger.isInfoEnabled()) {
            logger.info("Initialization completed");
        }
    }

    /**
     * Load geofence data from a linked data store
     *
     * @param dataStore
     * @throws ProcessorException
     */
    private void loadGeoFenceData(final DataStoreQueryInterface<?> dataStore) throws ProcessorException {
        Store store = dataStore.getStore();
        Optional<Collection<Object>> data;
        try {
            data = store.getInitialImage( dataStore.getInitialImageQuery());
        } catch (StorageException e) {
            throw new ProcessorException(String.format("Failure loading geofence initial image data from %s", dataStore.getStoreName()), e);
        }
        if (data.isPresent()) {
            if(logger.isInfoEnabled())
                logger.info(String.format("Adding %d geofences from linked data store %s", data.get().size(), dataStore.getStoreName()));
            // Add geofences
            for (Object obj : data.get()) {
                GeoFence fence;
                if (obj instanceof GeoFence objFence) {
                    fence = objFence;
                } else if (obj instanceof ReferenceDataObject ref) {
                    float radius = (ref.get("radius") != null) ? (float)ref.get("radius") : geofenceRadius;
                    fence = new GeoFence((int) ref.getKey(), (float) ref.get(latitudeField), (float) ref.get(longitudeField), radius);
                } else {
                    throw new ProcessorException(String.format("Unknown data type from geofence reference data store. Received [%s] expected either GeoFence or ReferenceDataObject class types", obj.getClass().getName()));
                }
                geofences.put(fence.getId(), fence);
                addGeoFence(fence);
            }
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        if (!geofences.isEmpty()) {
            // Discard processing
            if (event.getValue(trackingEventKey) == null && event.getValue(latitudeField) == null && event.getValue(longitudeField) == null) {
                metrics.incrementMetric(Metric.DISCARDED);
                return event;
            }

            // Event type is being tracked
            if (!state.containsKey(event.getValue(trackingEventKey))) {
                Object trackingKey = event.getValue(trackingEventKey);
                state.put(trackingKey, new GeoTrackingInfo(trackingKey, minDwellingTime, timeUnit));
            }

            Object latRaw = event.getValue(latitudeField);
            Object lngRaw = event.getValue(longitudeField);
            if (latRaw instanceof Double || latRaw instanceof Float || lngRaw instanceof Double || lngRaw instanceof Float) {

                // Perform geofence search
                GeoTrackingInfo info = searchAndTrackAlgorithm(event, ((Number) latRaw).floatValue(), ((Number) lngRaw).floatValue());

                // Add GeoInfo to event
                event.addValue(uuid, responseField, info);
                metrics.incrementMetric(Metric.PROCESSED);

            } else {
                metrics.incrementMetric(Metric.DISCARDED);
            }
        } else {
            metrics.incrementMetric(Metric.IGNORED);
        }
        return event;
    }

    /**
     * Search and track algorithm
     *
     * @param event
     * @param entityLatitude
     * @param entityLongitude
     * @return
     */
    private GeoTrackingInfo searchAndTrackAlgorithm(final StreamEvent event, final double entityLatitude, final double entityLongitude ){
        // Perform geofence search
        Collection<GeoFence> foundGeoFences = geoFenceSearchTree.query(new Tuple<>(entityLatitude, entityLongitude));
        Object trackingKey = event.getValue(trackingEventKey);

        // Update stateful tracking
        GeoTrackingInfo info = state.get(trackingKey);
        info.setCurrentLatLng(entityLatitude, entityLongitude, event.getEventTime());
        info.setCurrentGeofenceIds(foundGeoFences);

        // Replace current state
        state.replace(trackingKey, info);
        return info;
    }

    /**
     * Add a single geofence and location change listener. On a location
     * change the geo search tree is updated with new positions
     *
     * @param f
     */
    public void addGeoFence(GeoFence f) {
        geofences.putIfAbsent( f.getId(), f);
        f.initialise();
        geoFenceSearchTree.insert(f.getId(), f);
        f.registerLocationChangeListener(geofence -> geoFenceSearchTree.update(geofence.getId(), geofence));
    }

    /**
     * Remove single geofence
     *
     * @param f
     */
    public void removeGeoFence(GeoFence f) {
        geoFenceSearchTree.remove(f.getId());
        geofences.remove(f.getId());
    }

    public String getResponseField() {
        return responseField;
    }

    public String getTrackingEventKey() {
        return trackingEventKey;
    }

    @JsonProperty(value = "entity key", required = true)
    public void setTrackingEventKey(String trackingEventKey) {
        this.trackingEventKey = trackingEventKey;
    }

    @JsonProperty(value = "geo tracking info")
    public void setResponseField(String responseField) {
        this.responseField = responseField;
    }

    public Map<Integer, GeoFence> getGeofences() {
        return geofences;
    }

    @JsonProperty(value = "geofences")
    public void setGeofences(List<GeoFencePOJO> userGeofences) {
        userGeofences.forEach( v -> {
            String[] coords = v.getCoordinates();
            geofences.put( v.getId(), new GeoFence( v.getId(), Double.parseDouble(coords[0]), Double.parseDouble(coords[1]), v.getRadius() ));
        });
    }

    public void setGeofences(Map<Integer, GeoFence> userGeofences) {
        userGeofences.forEach(geofences::put);
    }

    public String getLatitudeField() {
        return latitudeField;
    }

    @JsonProperty(value = "latitude field")
    public void setLatitudeField(String latitudeField) {
        this.latitudeField = latitudeField;
    }

    public String getLongitudeField() {
        return longitudeField;
    }

    @JsonProperty(value = "longitude field")
    public void setLongitudeField(String longitudeField) {
        this.longitudeField = longitudeField;
    }

    public long getMinDwellingTime() {
        return minDwellingTime;
    }

    @JsonProperty(value = "dwelling time")
    public void setMinDwellingTime(long minDwellingTime) {
        this.minDwellingTime = minDwellingTime;
    }

    @JsonProperty(value = "timeUnit")
    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public float getGeofenceRadius() {
        return geofenceRadius;
    }

    @JsonProperty(value = "default radius")
    public void setGeofenceRadius(float geofenceRadius) {
        this.geofenceRadius = geofenceRadius;
    }

    /**
     * Set QuadTree for processor
     *
     * @param geoFenceSearchTree
     */
    @JsonProperty(value = "spatial index")
    public void setGeoFenceSearchTree(QuadTree<GeoFence> geoFenceSearchTree) {
        this.geoFenceSearchTree = geoFenceSearchTree;
    }

    public QuadTree<GeoFence> getGeoFenceSearchTree() {
        return geoFenceSearchTree;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (trackingEventKey == null || trackingEventKey.isEmpty()) {
            throw new InvalidSpecificationException("trackingEventKey must be provided.");
        }

        if (responseField == null || responseField.isEmpty()) {
            throw new InvalidSpecificationException("responseField must be provided.");
        }

        if (latitudeField == null || latitudeField.isEmpty()) {
            throw new InvalidSpecificationException("latitudeField must be provided.");
        }

        if (longitudeField == null || longitudeField.isEmpty()) {
            throw new InvalidSpecificationException("longitudeField must be provided.");
        }

        if(timeUnit == null){
            throw new InvalidSpecificationException("timeUnit must be provided.");
        }

        if(!timeUnit.equals(TimeUnit.SECONDS) || !timeUnit.equals(TimeUnit.MINUTES) || !timeUnit.equals(TimeUnit.HOURS) ){
            throw new InvalidSpecificationException("Supported timeUnit: SECONDS, MINUTES or HOURS.");
        }

        if(geofences.isEmpty() ){
            throw new InvalidSpecificationException("geofences must be provided.");
        }

        if( geoFenceSearchTree != null) geoFenceSearchTree.validate();
    }

    @Override
    public String toString() {
        return "GeoFenceAlertingProcessor{" +
                "state=" + state +
                ", geoFenceSearchTree=" + geoFenceSearchTree +
                ", geofences=" + geofences +
                ", trackingEventKey='" + trackingEventKey + '\'' +
                ", responseField='" + responseField + '\'' +
                ", latitudeField='" + latitudeField + '\'' +
                ", longitudeField='" + longitudeField + '\'' +
                ", minDwellingTime=" + minDwellingTime +
                "} " + super.toString();
    }
}
