/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.processors.geospatial.types.geometry.Point;
import com.fractalworks.streams.processors.geospatial.types.geometry.Polygon;

import java.util.ArrayList;
import java.util.List;

/**
 * Polygon reusable service
 *
 * @author Lyndon Adams
 */
public class PolygonService {

    private boolean clonePolygon = false;
    private double epsilon = 0.001;

    /**
     * @param polygon
     * @return
     */
    public Polygon optimisePolygonExternalRing(Polygon polygon) {

        Polygon updatedPolygon = (clonePolygon) ? deepCopyPolygon(polygon) : polygon;

        List<List<Point>> listOfPoints = updatedPolygon.getCoordinates();

        if (listOfPoints != null && listOfPoints.size() == 1) {

            int size = listOfPoints.get(0).size();

            List<Point> points = listOfPoints.get(0).subList(0, size - 1);

            List<Point> newPoints = RamerDouglasPeuckerPointReducer.reduce(points, epsilon);

            List<List<Point>> listPoints = new ArrayList<>();
            listPoints.add(newPoints);
            updatedPolygon.setCoordinates(listPoints);

        }
        return updatedPolygon;
    }

    public boolean isClonePolygon() {
        return clonePolygon;
    }

    public void setClonePolygon(boolean clonePolygon) {
        this.clonePolygon = clonePolygon;
    }

    /**
     * @param polygon
     * @return
     */
    private Polygon deepCopyPolygon(final Polygon polygon) {
        return polygon;
    }

    /**
     * @param epsilon
     */
    @JsonProperty(value = "epsilon", required = false)
    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }
}
