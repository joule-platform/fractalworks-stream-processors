/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.types.geo;

import com.fractalworks.streams.core.data.Triple;
import com.fractalworks.streams.processors.geospatial.utils.GeoMath;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import gnu.trove.map.hash.THashMap;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Geo tracking information value object holds current geo fences for a given tagged event type field.
 */
public class GeoTrackingInfo {

    private final Map<Integer, GeoFenceConstants> geoFenceOccupancyState = new UnifiedMap<>();
    private final Map<Integer, Long> geoFenceMinDwellingTime = new UnifiedMap<>();

    // Unique Id which is being tracked
    private final Object trackingTag;
    private Triple<Double, GeoDirection, Double> currenSpeedAndDirection;
    private Triple<Double, GeoDirection, Double> previousSpeedAndDirection;
    private double distanceTraveled;
    private double previousDistanceTraveled;
    private double previousLat = Double.NaN;
    private double previousLng = Double.NaN;
    private double currentLat = Double.NaN;
    private double currentLng = Double.NaN;
    private long previousUpdatedTimestamp;
    private long currentUpdatedTimestamp;
    private long minDwellingTime = 5000;

    public GeoTrackingInfo(Object trackingTag) {
        this.trackingTag = trackingTag;
    }

    public GeoTrackingInfo(Object trackingTag, long minDwellingTime, TimeUnit unit) {
        this(trackingTag);
        this.minDwellingTime = unit.toMillis(minDwellingTime) ;
    }

    /**
     * Set current lat,lng position.
     *
     * @param currentLat
     * @param currentLng
     * @param timestamp
     */
    public void setCurrentLatLng(double currentLat, double currentLng, long timestamp) {
        if (this.currentLat != Double.NaN) {
            this.previousLat = this.currentLat;
            this.previousLng = this.currentLng;
            this.previousUpdatedTimestamp = currentUpdatedTimestamp;
        }
        this.currentLat = currentLat;
        this.currentLng = currentLng;
        this.currentUpdatedTimestamp = timestamp;

        if (previousLat != Double.NaN) {
            Triple<Double, GeoDirection, Double> newCurrentSpeedAndDirection = GeoMath.getSpeedAndDirectionBetweenPoints(previousLat, previousLng, currentLat, currentLng, (currentUpdatedTimestamp - previousUpdatedTimestamp));

            previousSpeedAndDirection = currenSpeedAndDirection;
            currenSpeedAndDirection = newCurrentSpeedAndDirection;

            previousDistanceTraveled = distanceTraveled;
            distanceTraveled = GeoMath.distanceBetweenPointsByMeter(previousLat, previousLng, currentLat, currentLng);
        }
    }

    /**
     * Build initial GeoFence view
     *
     * @param currentGeofences
     */
    private void buildInitialGeoFenceView(final Collection<GeoFence> currentGeofences) {
        currentGeofences.forEach(geoFence -> {
            geoFenceOccupancyState.put(geoFence.getId(), GeoFenceConstants.ENTERED);
            geoFenceMinDwellingTime.put(geoFence.getId(), 0L);
        });
    }

    /**
     * Update geofences and remove from passed map
     *
     * @param foundGeoFences
     * @return
     */
    private List<Integer> updateGeoFenceState(Map<Integer, GeoFence> foundGeoFences) {

        List<Integer> itemsToRemoveFromState = new ArrayList<>();

        // Iterate over current held geofence states
        for (Map.Entry<Integer, GeoFenceConstants> geoEntry : geoFenceOccupancyState.entrySet()) {
            Integer fenceId = geoEntry.getKey();

            // Still in fence
            if (foundGeoFences.containsKey(fenceId)) {

                switch (geoEntry.getValue()) {
                    case ENTERED:
                        Long dwellingTime = geoFenceMinDwellingTime.get(fenceId);
                        if (minDwellingTime <= dwellingTime + (currentUpdatedTimestamp - previousUpdatedTimestamp)) {
                            geoFenceOccupancyState.replace(fenceId, GeoFenceConstants.DWELLING);
                        }
                        geoFenceMinDwellingTime.compute(fenceId, (key, val) -> val + (currentUpdatedTimestamp - previousUpdatedTimestamp));
                        break;
                    case DWELLING:
                        geoFenceMinDwellingTime.compute(fenceId, (key, val) -> val + (currentUpdatedTimestamp - previousUpdatedTimestamp));
                        break;
                    default:
                        break;
                }
                foundGeoFences.remove(fenceId);

            } else {
                // Not-Exists in map but no point no longer said GeoFence
                switch (geoEntry.getValue()) {
                    case ENTERED, DWELLING:
                        geoFenceOccupancyState.replace(fenceId, GeoFenceConstants.EXITED);
                        break;
                    case EXITED:
                        geoFenceOccupancyState.replace(fenceId, GeoFenceConstants.EXPIRED);
                        itemsToRemoveFromState.add(fenceId);
                        break;
                    case EXPIRED:
                        itemsToRemoveFromState.add(fenceId);
                        break;
                    default:
                        break;
                }
                // Does not remove or perform correct lookup because the passed collection is not int but a GeoFence
                foundGeoFences.remove(fenceId);
            }
        }
        return itemsToRemoveFromState;
    }

    /**
     * Set the geofences this point is found within.
     *
     * @param currentGeofences
     */
    public void setCurrentGeofenceIds(Collection<GeoFence> currentGeofences) {

        List<Integer> itemsToRemoveFromState;

        // Before setting need some additional information created such entry/exit/
        if (geoFenceOccupancyState.isEmpty()) {
            buildInitialGeoFenceView(currentGeofences);
        } else {

            Map<Integer, GeoFence> foundGeoFences = new THashMap<>(currentGeofences.size());
            for (GeoFence f : currentGeofences) {
                foundGeoFences.put(f.getId(), f);
                geoFenceMinDwellingTime.put(f.getId(), 0L);
            }

            // Updated geofence states
            itemsToRemoveFromState = updateGeoFenceState(foundGeoFences);

            // Add new geofences from the remaining geofences
            for (GeoFence f : foundGeoFences.values()) {
                geoFenceOccupancyState.put(f.getId(), GeoFenceConstants.ENTERED);
            }

            // Now perform clean up on internal maps
            for (Integer i : itemsToRemoveFromState) {
                geoFenceOccupancyState.remove(i);
                geoFenceMinDwellingTime.remove(i);
            }
        }
    }

    /**
     * Get the current geofences this geo element has resided or resides within.
     *
     * @return Map<Integer, GeoFenceConstants>
     */
    public Map<Integer, GeoFenceConstants> getGeoFenceOccupancyState() {
        return geoFenceOccupancyState;
    }

    /**
     * Get the current direction this geo element is travelling.
     *
     * @return
     */
    public GeoDirection getCurrentDirection() {
        return (currenSpeedAndDirection != null) ? currenSpeedAndDirection.getY() : GeoDirection.UNKNOWN;
    }

    /**
     * Get the previous direction this geo element was travelling.
     *
     * @return
     */
    public GeoDirection getPreviousDirection() {
        return (previousSpeedAndDirection != null) ? previousSpeedAndDirection.getY() : GeoDirection.UNKNOWN;
    }

    /**
     * Current speed and direction.
     *
     * @return
     */
    public Triple<Double, GeoDirection, Double> getCurrenSpeedAndDirection() {
        return currenSpeedAndDirection;
    }

    /**
     * Previous speed and direction.
     *
     * @return
     */
    public Triple<Double, GeoDirection, Double> getPreviousSpeedAndDirection() {
        return previousSpeedAndDirection;
    }

    /**
     * Current distance travelled.
     *
     * @return
     */
    public double getDistanceTravelled() {
        return distanceTraveled;
    }

    public double getPreviousLat() {
        return previousLat;
    }

    public double getPreviousLng() {
        return previousLng;
    }

    public double getCurrentLat() {
        return currentLat;
    }

    public double getCurrentLng() {
        return currentLng;
    }

    public Object getTrackingTag() {
        return trackingTag;
    }

    public long getPreviousUpdatedTimestamp() {
        return previousUpdatedTimestamp;
    }

    public long getCurrentUpdatedTimestamp() {
        return currentUpdatedTimestamp;
    }

    public void setMinDwellingTime(long minDwellingTime) {
        this.minDwellingTime = minDwellingTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoTrackingInfo)) return false;
        GeoTrackingInfo that = (GeoTrackingInfo) o;
        return Double.compare(that.distanceTraveled, distanceTraveled) == 0 &&
                Double.compare(that.previousDistanceTraveled, previousDistanceTraveled) == 0 &&
                Double.compare(that.previousLat, previousLat) == 0 &&
                Double.compare(that.previousLng, previousLng) == 0 &&
                Double.compare(that.currentLat, currentLat) == 0 &&
                Double.compare(that.currentLng, currentLng) == 0 &&
                previousUpdatedTimestamp == that.previousUpdatedTimestamp &&
                currentUpdatedTimestamp == that.currentUpdatedTimestamp &&
                minDwellingTime == that.minDwellingTime &&
                Objects.equals(geoFenceOccupancyState, that.geoFenceOccupancyState) &&
                Objects.equals(geoFenceMinDwellingTime, that.geoFenceMinDwellingTime) &&
                Objects.equals(trackingTag, that.trackingTag) &&
                Objects.equals(currenSpeedAndDirection, that.currenSpeedAndDirection) &&
                Objects.equals(previousSpeedAndDirection, that.previousSpeedAndDirection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(geoFenceOccupancyState, geoFenceMinDwellingTime, trackingTag, currenSpeedAndDirection, previousSpeedAndDirection, distanceTraveled, previousDistanceTraveled, previousLat, previousLng, currentLat, currentLng, previousUpdatedTimestamp, currentUpdatedTimestamp, minDwellingTime);
    }

    @Override
    public String toString() {
        return "GeoTrackingInfo{" +
                "trackingTag=" + trackingTag +
                ", currentSpeedAndDirection=" + currenSpeedAndDirection +
                ", previousSpeedAndDirection=" + previousSpeedAndDirection +
                ", distanceTraveled=" + distanceTraveled +
                ", previousDistanceTraveled=" + previousDistanceTraveled +
                ", previousLat=" + previousLat +
                ", previousLng=" + previousLng +
                ", currentLat=" + currentLat +
                ", currentLng=" + currentLng +
                ", geoFenceOccupancyState=" + geoFenceOccupancyState +
                ", previousUpdatedTimestamp=" + previousUpdatedTimestamp +
                ", currentUpdatedTimestamp=" + currentUpdatedTimestamp +
                '}';
    }
}
