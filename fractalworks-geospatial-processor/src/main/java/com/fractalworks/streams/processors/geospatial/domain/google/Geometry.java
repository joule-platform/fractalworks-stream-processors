/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.google;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Geometry class that has a bounding area and viewpoort for a given location
 * <p>
 * Created by Lyndon Adams
 */
public class Geometry {

    private Bounds bounds;
    private Location location;
    private String locationType;
    private Bounds viewport;

    /**
     * Default constructor required for serialization.
     */
    public Geometry() {
        // Required for serialization
    }

    public Bounds getBounds() {
        return bounds;
    }

    @JsonProperty("bounds")
    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    @JsonProperty("location_type")
    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public Bounds getViewport() {
        return viewport;
    }

    @JsonProperty("viewport")
    public void setViewport(Bounds viewport) {
        this.viewport = viewport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Geometry)) return false;
        Geometry geometry = (Geometry) o;
        return Objects.equals(bounds, geometry.bounds) &&
                Objects.equals(location, geometry.location) &&
                Objects.equals(locationType, geometry.locationType) &&
                Objects.equals(viewport, geometry.viewport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bounds, location, locationType, viewport);
    }
}
