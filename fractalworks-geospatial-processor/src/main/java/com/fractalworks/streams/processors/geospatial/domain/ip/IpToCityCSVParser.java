/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.domain.ip;

import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.referencedata.serialization.ReferenceDataCSVParser;
import com.google.common.base.Splitter;

import java.util.Iterator;
import java.util.List;

/**
 * IP Address to City CSV row parser
 * <p>
 * Created by Lyndon Adams
 */
public class IpToCityCSVParser implements ReferenceDataCSVParser<IpToCity> {

    private static final String DOT_DELIMITER = ".";
    private String delimiter = ",";

    public IpToCityCSVParser() {
    }

    public IpToCityCSVParser(String delimiter) {
        this.delimiter = delimiter;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /**
     * Parse string to a IpToCity object.
     *
     * @param obj Format { IP from, IP to, country, area, city }
     * @return
     * @throws Exception
     */
    @Override
    public IpToCity translate(String obj) throws TranslationException {
        Splitter splitter = Splitter.on(delimiter);
        Iterator<String> it = splitter.split(obj).iterator();
        if (it.hasNext()) {

            String fromStr = it.next().strip();
            String toStr = it.next().strip();
            String country = it.next().strip();
            String area = (it.hasNext()) ? it.next().strip() : null;
            String city = (it.hasNext()) ? it.next().strip() : null;
            String misc = (it.hasNext()) ? it.next().strip() : null;

            Splitter splitterFromTo = Splitter.on(DOT_DELIMITER);
            List<String> from = splitterFromTo.splitToList(fromStr);
            List<String> to = splitterFromTo.splitToList(toStr);

            if (from.size() != 4 && to.size() != 4) {
                return null;
            }

            short[] fromAsShort = new short[4];
            short[] toAsShort = new short[4];
            try {
                for (int i = 0; i < 4; i++) {
                    fromAsShort[i] = Short.parseShort(from.get(i));
                    toAsShort[i] = Short.parseShort(to.get(i));
                }
            } catch (NumberFormatException e) {
                throw new TranslationException("Failure parsing IP to shorts.", e);
            }

            return new IpToCity(fromAsShort, toAsShort, country, area, city, misc);
        }
        return null;
    }

}
