/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.geospatial.domain.ip.IpToCity;
import com.fractalworks.streams.processors.geospatial.domain.ip.IpCityArrowParser;
import com.fractalworks.streams.processors.geospatial.domain.ip.IpToCityStore;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataLoaderSpecification;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.util.file.ReferenceDataFileProcessingTask;
import org.apache.arrow.dataset.file.FileFormat;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Properties;

/**
 * Take an IP address and perform a geolocation search
 * <p>
 * Created by lyndon
 */
@JsonRootName(value = "ip geo resolver")
public class IPAddressToGeoLocationProcessor extends AbstractProcessor {

    public static final String GEO_COUNTRY = "country";
    public static final String GEO_AREA = "area";
    public static final String GEO_CITY = "city";

    private ReferenceDataLoaderSpecification referenceDataLoaderSpecification;

    private NavigableMap<Long, IpToCity> ipToCities;
    private String ipAddressField;
    private String resultFieldName = "ipGeoLocation";

    public IPAddressToGeoLocationProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        // Load reference data and build IP search tree
        if (ipToCities == null) {
            if(referenceDataLoaderSpecification.getParser() == null) {
                referenceDataLoaderSpecification.setParser(IpCityArrowParser.class);
            }
            referenceDataLoaderSpecification.initialize();
            ipToCities = new IpToCityStore();

            try {


                File f = new File(referenceDataLoaderSpecification.getSource());
                var task = new ReferenceDataFileProcessingTask<IpToCity>((Store)ipToCities, f.getName(), f.getAbsolutePath(), FileFormat.CSV);
                task.setParser(referenceDataLoaderSpecification.getParser());
                var status = task.call();
                if( status.processCount() == 0 ){
                    throw new ProcessorException("Failed to load IP Address reference data");
                }
            } catch (Exception e) {
                throw new ProcessorException("Failure during reference data loading.", e);
            }
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        String ipAddress = (String) event.getValue(ipAddressField);
        if (ipAddress != null) {
            long ipAsLong = IpToCity.ipToLong(ipAddress);

            Map.Entry<Long, IpToCity> higherEntry = ipToCities.higherEntry(ipAsLong);
            Map.Entry<Long, IpToCity> lowerEntry = ipToCities.lowerEntry(ipAsLong);

            IpToCity result = null;
            if (higherEntry.getValue().isWithinRange(ipAsLong)) {
                result = higherEntry.getValue();
            } else if (lowerEntry.getValue().isWithinRange(ipAsLong)) {
                result = lowerEntry.getValue();
            }

            if (result != null) {
                var map = new HashMap<>(3);
                map.put(GEO_COUNTRY, result.getCountry());
                map.put(GEO_AREA, result.getArea());
                map.put(GEO_CITY, result.getCity());
                event.addValue(uuid, resultFieldName,map);
            }
            metrics.incrementMetric(Metric.PROCESSED);

        } else {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        }
        return event;
    }

    /**
     * Set search result field name.
     *
     * @param resultFieldName
     */
    @JsonProperty(value = "response field")
    public void setResultFieldName(@Nonnull String resultFieldName) {
        this.resultFieldName = resultFieldName;
    }


    @JsonProperty(value = "ip field")
    public void setIpAddressField(@Nonnull String ipAddressField) {
        this.ipAddressField = ipAddressField;
    }


    /**
     * Set ip address reference data search tree
     *
     * @param ipToCities
     */
    @JsonIgnore
    public void setSearchTree(@Nonnull NavigableMap<Long, IpToCity> ipToCities) {
        this.ipToCities = ipToCities;
    }

    /**
     * Set mapping ntities .
     *
     * @param referenceDataLoaderSpecification ReferenceDataLoaderSpecification
     */
    @JsonProperty(value = "mapping entities")
    public void setMappingEntities(@Nonnull final ReferenceDataLoaderSpecification referenceDataLoaderSpecification) {
        this.referenceDataLoaderSpecification = referenceDataLoaderSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (ipAddressField == null || ipAddressField.isEmpty()) {
            throw new InvalidSpecificationException("ipAddressField cannot be null nor empty.");
        }


        if (referenceDataLoaderSpecification == null) {
            throw new InvalidSpecificationException("mapping entries must be provided.");
        }

        if (referenceDataLoaderSpecification.getSource() == null) {
            throw new InvalidSpecificationException("mapping entries file source must be provided.");
        }
    }

}
