/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.processors.geospatial.types.SpatialEntityStore;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geometry.Shape;
import com.fractalworks.streams.processors.geospatial.utils.GeoMath;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataLoaderSpecification;
import com.fractalworks.streams.sdk.util.file.ReferenceDataFileProcessingTask;
import org.apache.arrow.dataset.file.FileFormat;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Properties;

/**
 * Geospatial search processor that finds geo elements within a search area.
 * <p>
 * The result is a collection of geo based nodes.
 * <p>
 * Created by Lyndon Adams
 */
@JsonRootName(value = "geo search")
public class GeoSearchProcessor extends AbstractProcessor {

    private String resultFieldName = "geoSearchResult";
    private String latitudeField = "latitude";
    private String longitudeField = "longitude";
    private int searchArea = 8;
    private boolean convertWGS84toTileCoords = true;

    private QuadTree<GeoNode> searchTree;

    private ReferenceDataLoaderSpecification referenceDataLoaderSpecification;

    private SpatialEntityStore spatialEntityStore;

    public GeoSearchProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        if (searchTree == null) {
            searchTree = new QuadTree<>();
        }
        searchTree.initialise();

        if(referenceDataLoaderSpecification !=null) {
            referenceDataLoaderSpecification.initialize();
            loadSpatialEntities();
        }
    }

    /**
     * Load spatial entities within spatial index
     *
     * @throws ProcessorException
     */
    private void loadSpatialEntities() throws ProcessorException {
        try {
            spatialEntityStore = new SpatialEntityStore(searchTree);
            File f = new File(referenceDataLoaderSpecification.getSource());
            var task = new ReferenceDataFileProcessingTask<>(spatialEntityStore, f.getName(), f.getAbsolutePath(), FileFormat.CSV);
            task.setMoveFileAfterProcessing(false);
            task.setParser(referenceDataLoaderSpecification.getParser());
            var status = task.call();
            System.out.println(searchTree.getSize());
            if(logger.isInfoEnabled()){
                logger.info("Loaded {} spatial entities in {}", status.processCount(), name);
            }
        } catch (Exception e) {
            throw new ProcessorException(e);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        if (event.contains(latitudeField) && event.contains(longitudeField)) {
            double lat = (double) event.getValue(latitudeField);
            double lng = (double) event.getValue(longitudeField);

            // Get lat/lng convert to standard x,y
            Tuple<Double, Double> convertedPoints = (convertWGS84toTileCoords) ? GeoMath.convertWGS84toQuadTile(lat, lng, searchTree.getMaxlevels()) : new Tuple<>(lat, lng);

            // Create search area
            Shape searchSpace = GeoMath.createBoundingBox(convertedPoints.getX(), convertedPoints.getY(), searchArea);

            // look up possible matching geofence containing x,y
            event.addValue(uuid, resultFieldName, searchTree.query(searchSpace));

            metrics.incrementMetric(Metric.PROCESSED);
        } else {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        }
        return event;
    }

    public String getResultFieldName() {
        return resultFieldName;
    }

    /**
     * Set search result field name.
     *
     * @param resultFieldName
     */
    @JsonProperty(value = "response field")
    public void setResultFieldName(@Nonnull String resultFieldName) {
        this.resultFieldName = resultFieldName;
    }

    public String getLatitudeField() {
        return latitudeField;
    }

    @JsonProperty(value = "latitude field")
    public void setLatitudeField(@Nonnull String latitudeField) {
        this.latitudeField = latitudeField;
    }

    public String getLongitudeField() {
        return longitudeField;
    }

    @JsonProperty(value = "longitude field")
    public void setLongitudeField(@Nonnull String longitudeField) {
        this.longitudeField = longitudeField;
    }

    public int getSearchArea() {
        return searchArea;
    }

    @JsonProperty(value = "search area")
    public void setSearchArea(int searchArea) {
        this.searchArea = searchArea;
    }

    /**
     * Set search tree.
     *
     * @param searchTree QuadTree.
     */
    @JsonProperty(value = "spatial index")
    public void setSearchTree(@Nonnull final QuadTree<GeoNode> searchTree) {
        this.searchTree = searchTree;
    }

    /**
     * Set spatial entities .
     *
     * @param searchTree QuadTree.
     */
    @JsonProperty(value = "spatial entities")
    public void setSpatialEntities(@Nonnull final ReferenceDataLoaderSpecification referenceDataLoaderSpecification) {
        this.referenceDataLoaderSpecification = referenceDataLoaderSpecification;
    }

    /**
     * Set a boolean to either convert all WGS84 points to tile coordinates.
     *
     * @param convertWGS84toTileCoords true to convert false otherwise.
     */
    @JsonProperty(value = "convertWGS84toTileCoords")
    public void setConvertWGS84toTileCoords(boolean convertWGS84toTileCoords) {
        this.convertWGS84toTileCoords = convertWGS84toTileCoords;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (resultFieldName == null || resultFieldName.isEmpty()) {
            throw new InvalidSpecificationException("response field cannot be null or empty.");
        }

        if (latitudeField == null || latitudeField.isEmpty()) {
            throw new InvalidSpecificationException("latitude field cannot be null or empty.");
        }

        if (longitudeField == null || longitudeField.isEmpty()) {
            throw new InvalidSpecificationException("longitude field cannot be null or empty.");
        }
        referenceDataLoaderSpecification.validate();
    }
}
