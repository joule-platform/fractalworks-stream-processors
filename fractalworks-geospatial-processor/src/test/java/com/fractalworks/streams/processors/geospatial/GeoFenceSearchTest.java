/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoTrackingInfo;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Test to ensure entry/exit geo fencing works.
 * <p>
 * London Bounding box
 * <Polygon><outerBoundaryIs>
 * <coordinates>-
 *      0.5479431152,51.250741915
 *      0.3172302246,51.250741915
 *      0.3172302246,51.7006509825
 *      -0.5479431152,51.7006509825
 * </coordinates>
 * </outerBoundaryIs>
 * </Polygon>
 * <p>
 * England
 * <Polygon><outerBoundaryIs>
 * <coordinates>-
 *      5.7348632813,50.1909677656
 *      1.845703125,50.1909677656
 *      1.845703125,55.727110085
 *      -5.7348632813,55.727110085
 * </coordinates>
 * </outerBoundaryIs></Polygon>
 * <p>
 * Created by lyndon on 02/02/2017.
 */
public class GeoFenceSearchTest {

    public static UUID uuid = UUID.randomUUID();
    public double[][] journey = {
            {51.456821, -0.164746},
            {51.456596, -0.1659662},
            {51.458077, -0.1667799},
            {51.460794, -0.1675542},
            {51.46088, -0.1676482},
            {51.460964, -0.1676752},
            {51.462416, -0.1698427},
            {51.463659, -0.1700357},
            {51.463559, -0.1707977},
            {51.463552, -0.1712917}
    };
    QuadTree<GeoFence> geoFences;
    String trackingKey = "imsi";

    public final GeoFence bellevilleRoadGeoFence = new GeoFence(1, 51.456821f, -0.164746f, 2f);
    public final GeoFence batterseaRiseGeoFence = new GeoFence(2, 51.460774f, -0.1692377f, 150f);
    public final GeoFence claphamJunctionGeoFence = new GeoFence(3, 51.463474f, -0.1700213f, 50f);


    @BeforeEach
    public void insertGeoFences() {
        geoFences = new QuadTree<>(-180f, -90f, 360, 64800);
    }

    @Test
    public void searchForGeoFenceInclusion() throws StreamsException {
        EntityGeoTrackerProcessor processor = new EntityGeoTrackerProcessor();
        processor.setGeoFenceSearchTree(geoFences);
        processor.setTrackingEventKey(trackingKey);
        processor.setMinDwellingTime(2500);
        processor.addGeoFence(bellevilleRoadGeoFence);
        processor.addGeoFence(batterseaRiseGeoFence);
        processor.addGeoFence(claphamJunctionGeoFence);

        processor.initialize(null);

        List<Map<Integer, GeoFenceConstants>> geofencesVisited = new ArrayList<>();

        long timeStamp = System.currentTimeMillis();
        for (int i = 0; i < journey.length; i++) {

            StreamEvent evt = new StreamEvent("geoTest");
            evt.setEventTime(timeStamp);
            evt.addValue(uuid, trackingKey, "1234567890");
            evt.addValue(uuid, "latitude", journey[i][0]);
            evt.addValue(uuid, "longitude", journey[i][1]);

            evt = processor.apply(evt, null);

            GeoTrackingInfo geoTrackingInfo = ((GeoTrackingInfo) evt.getValue("geoTrackingInfo"));
            if (geoTrackingInfo != null && !geoTrackingInfo.getGeoFenceOccupancyState().isEmpty()) {
                geofencesVisited.add(geoTrackingInfo.getGeoFenceOccupancyState());
                System.out.println(geoTrackingInfo.getGeoFenceOccupancyState());
            }
            timeStamp += 3250;
        }

        System.out.println("-- "+ geofencesVisited.size());
        assertEquals(8, geofencesVisited.size());
    }
}