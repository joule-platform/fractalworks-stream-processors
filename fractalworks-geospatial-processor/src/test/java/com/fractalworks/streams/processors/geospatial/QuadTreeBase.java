/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geometry.Coordinates;

import java.util.Collection;

/**
 * QuadTree base class
 */
public abstract class QuadTreeBase {

    public void printElelemnts(Collection<Element> results) {
        for (Element e : results) {
            System.out.println(e);
        }
        System.out.println("Size: " + results.size());
    }

    protected class Element implements Coordinates {

        Tuple<Double, Double> xy;
        String str;

        public Element(Tuple<Double, Double> xy) {
            this.xy = xy;
            str = String.format("X: %f  Y: %f", xy.getX(), xy.getY());
        }

        @Override
        public Tuple getCoordinates() {
            return xy;
        }

        @Override
        public String toString() {
            return "Element{" +
                    "xy=" + xy +
                    ", str='" + str + '\'' +
                    '}';
        }
    }
}
