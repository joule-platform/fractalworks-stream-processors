/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.reversegeo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.geospatial.ReverseGeoLookUpProcessor;
import com.fractalworks.streams.processors.geospatial.domain.google.GoogleGeoCoordinates;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by lyndon on 09/01/2017.
 */
public class GoogleReverseLocationLookUpTest {

    public static UUID uuid = UUID.randomUUID();

    @Test
    public void buildComponentFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "{\"enabled\":true,\"latitudeField\":\"lat\",\"longitudeField\":\"lng\"}";
        ReverseGeoLookUpProcessor obj = mapper.readValue(jsonInString, ReverseGeoLookUpProcessor.class);
        assertEquals(ReverseGeoLookUpProcessor.class, obj.getClass());
    }

    @Test
    public void singleLocationLookUp() throws StreamsException {

        ReverseGeoLookUpProcessor reverseGeoLookUpProcessor = new ReverseGeoLookUpProcessor();
        reverseGeoLookUpProcessor.setApiKey("AIzaSyDCKmFA5bTBjAp4beYMp7sRRVqfBtNeuI8");
        reverseGeoLookUpProcessor.initialize(null);

        StreamEvent evt = new StreamEvent("geo");
        evt.addValue(uuid, "latitude", 51.456821);
        evt.addValue(uuid, "longitude", -0.164746);

        for (int i = 0; i < 5; i++) {
            long start = System.currentTimeMillis();
            StreamEvent r = reverseGeoLookUpProcessor.apply(evt, null);
            GoogleGeoCoordinates location = (GoogleGeoCoordinates) r.getValue("location");

            System.out.println(i + ": " + location.getFullAddress() + " Time: " + (System.currentTimeMillis() - start));
        }
    }

}
