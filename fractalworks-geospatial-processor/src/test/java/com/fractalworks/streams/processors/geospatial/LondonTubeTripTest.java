/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.functions.EventFunction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LondonTubeTripTest {

    public static UUID uuid = UUID.randomUUID();
    public double[][] journey = {
            {-0.227694,51.502196,0},
            {-0.227726,51.502142,0},
            {-0.227211,51.502263,0},
            {-0.226868,51.502329,0},
            {-0.226288,51.502476,0},
            {-0.225967,51.50257,0},
            {-0.22498,51.502744,0},
            {-0.221997,51.503665,0},
            {-0.219658,51.504119,0},
            {-0.218713905,51.50470697,0},
            {-0.217985,51.505203,0},
            {-0.214613,51.504934,0},
            {-0.213417,51.505313,0},
            {-0.212202,51.505526,0},
            {-0.209808,51.506044,0},
            {-0.205898,51.507139,0},
            {-0.205512,51.507249,0},
            {-0.205184,51.507336,0},
            {-0.202212,51.50819,0},
            {-0.200555,51.508591,0},
            {-0.199214,51.508725,0},
            {-0.197139,51.509007,0},
            {-0.19689,51.50905,0},
            {-0.196616,51.509077,0},
            {-0.196441,51.509097,0},
            {-0.196389,51.509107,0},
            {-0.196347,51.509115,0},
            {-0.195894,51.509212,0},
            {-0.195528,51.509282,0},
            {-0.194636,51.50944,0},
            {-0.193745,51.509617,0},
            {-0.192799,51.509768,0},
            {-0.192195,51.509891,0},
            {-0.191173,51.510021,0},
            {-0.189894,51.510112,0},
            {-0.188789,51.510177,0},
            {-0.188028,51.510223,0},
            {-0.187269,51.510283,0},
            {-0.187,51.510443,0},
            {-0.187116,51.510496,0},
            {-0.185474,51.510426,0} };

    QuadTree<GeoFence> geoFences;
    String trackingKey = "imsi";

    public final GeoFence hollandParkGeoFence = new GeoFence(107, 51.506645, -0.20406286, 150);
    public final GeoFence shepherdsBushGeoFence = new GeoFence(106, 51.503845, -0.2172389, 150);
    public final GeoFence queenswayGeoFence = new GeoFence(119, 51.509743, -0.18553847, 150);

    static EntityGeoTrackerProcessor processor;
    static GeoFenceOccupancyStateTrigger trigger;

    @BeforeAll
    public static void setup() throws ProcessorException, IOException {

        ClassLoader classLoader = LondonTubeTripTest.class.getClassLoader();
        File file = new File(classLoader.getResource("dsl/validLondonTubeTrip.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("dsl");
        processor = readMapper.readValue(file, EntityGeoTrackerProcessor.class);
        processor.initialize(null);

        trigger = new GeoFenceOccupancyStateTrigger();
        trigger.setGeoTrackerField("geoTrackingInfo");
        trigger.setPlugIn(TestTriggerFunction.class );
        trigger.initialize( null);
    }

    @Test
    public void runJourneyTest() throws StreamsException {
        long time = System.currentTimeMillis();
        for (int i = 0; i < journey.length; i++) {

            UUID uuid = UUID.randomUUID();

            // Build event
            StreamEvent evt = new StreamEvent("tubeStationVisits");
            evt.setEventTime(time);
            evt.addValue(uuid, trackingKey, "1234567890");
            evt.addValue(uuid, "latitude", journey[i][1]);
            evt.addValue(uuid, "longitude", journey[i][0]);

            try {
                // Process and receive response
                evt = processor.apply(evt, null);
                System.out.println(evt);
                trigger.apply( evt, null);

            } catch (StreamsException ex) {
                ex.printStackTrace(System.out);
            }
            // 5 second delay between points
            time += 5000;

        }
        assertTrue(true);
    }

    public static class TestTriggerFunction implements EventFunction {

        public TestTriggerFunction() {
        }

        @Override
        public void onEvent(StreamEvent event, Context context) {
            GeoFenceConstants state = (GeoFenceConstants) context.getValue("state");
            int geofenceId = (int) context.getValue("geofenceId");
            Object trackingTag = context.getValue("trackingTag");
            String message = "N/A";

            switch( state){
                case ENTERED:
                    System.out.println(String.format("imsi[%s] - Welcome to geofence %d", trackingTag,geofenceId));
                    break;
                case DWELLING:
                    System.out.println(String.format("imsi[%s] - Enjoy just ask from geofence %d", trackingTag,geofenceId));
                    break;
                case EXITED:
                    System.out.println(String.format("imsi[%s] - See you next time from geofence %d", trackingTag,geofenceId));
                    break;
                default:
                    break;
            }

        }
    }
}
