/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoTrackingInfo;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test to ensure entry/exit goe fencing works with geofences central positions changing.
 * This is to emulate multiple objects moving in space. Callback logic applied to GeoFence ]
 * that updates the internal quadtree
 * <p>
 * London Bounding box
 * <Polygon><outerBoundaryIs>
 * <coordinates>-
 * 0.5479431152,51.250741915
 * 0.3172302246,51.250741915
 * 0.3172302246,51.7006509825
 * -0.5479431152,51.7006509825
 * </coordinates>
 * </outerBoundaryIs>
 * </Polygon>
 * <p>
 * England
 * <Polygon><outerBoundaryIs>
 * <coordinates>-
 * 5.7348632813,50.1909677656
 * 1.845703125,50.1909677656
 * 1.845703125,55.727110085
 * -5.7348632813,55.727110085
 * </coordinates>
 * </outerBoundaryIs></Polygon>
 * <p>
 * Created by Lyndon Adams
 */
public class VisitedInfectedPersonTest {

    public static UUID uuid = UUID.randomUUID();
    public double[][] journey = {
            {51.456821, -0.164746},
            {51.456596, -0.1659662},
            {51.458077, -0.1667799},
            {51.460774f, -0.1692377f},
            {51.460794, -0.1675542},
            {51.46088, -0.1676482},
            {51.460964, -0.1676752},
            {51.462416, -0.1698427},
            {51.463659, -0.1700357},
            {51.463559, -0.1707977},
            {51.463474f, -0.1700213f},
            {51.463552, -0.1712917}
    };
    String trackingKey = "imsi";

    GeoFence infectedPerson1 = new GeoFence(1, 51.456821f, -0.164746f, 2f);
    GeoFence infectedPerson2 = new GeoFence(2, 51.460774f, -0.1692377f, 2f);
    GeoFence infectedPerson3 = new GeoFence(3, 51.463474f, -0.1700213f, 2f);
    GeoFence infectedPerson4 = new GeoFence(4, 56.463474f, -0.1700213f, 2f);

    Map<Integer,GeoFence> fences;

    @BeforeEach
    public void insertInfectedPersons() {
        fences = new HashMap<>();
        fences.put(infectedPerson1.getId(), infectedPerson1);
        fences.put(infectedPerson2.getId(), infectedPerson2);
        fences.put(infectedPerson3.getId(),infectedPerson2);
        fences.put(infectedPerson4.getId(),infectedPerson2);
    }

    @Test
    public void searchForGeoFenceInclusion() throws StreamsException {
        EntityGeoTrackerProcessor processor = new EntityGeoTrackerProcessor();
        processor.setTrackingEventKey(trackingKey);
        processor.setMinDwellingTime(2000);
        processor.setGeofences(fences);
        processor.initialize(null);

        List<Map<Integer, GeoFenceConstants>> geofencesVisited = new ArrayList<>();

        long timeStamp = System.currentTimeMillis();

        // Perform a forward journey
        for (int i = 0; i < journey.length; i++) {

            StreamEvent evt = new StreamEvent("geoTest");
            evt.setEventTime(timeStamp);
            evt.addValue(uuid, trackingKey, "1234567890");
            evt.addValue(uuid, "latitude", journey[i][0]);
            evt.addValue(uuid, "longitude", journey[i][1]);

            processor.apply(evt, null);

            GeoTrackingInfo geoTrackingInfo = ((GeoTrackingInfo) evt.getValue("geoTrackingInfo"));
            if (geoTrackingInfo != null && !geoTrackingInfo.getGeoFenceOccupancyState().isEmpty()) {
                geofencesVisited.add(geoTrackingInfo.getGeoFenceOccupancyState());
                System.out.println("Forward Journey: " + geoTrackingInfo.getGeoFenceOccupancyState());
            }
            timeStamp += 3250;
        }

        // Update persons locations
        infectedPerson1.updateCoordinates(51.463474f, -0.1700213f);
        infectedPerson2.updateCoordinates(51.456821f, -0.164746f);
        infectedPerson3.updateCoordinates(51.460774f, -0.1692377f);

        // Suddenly infectedPerson4 appeared in the same geofence as infectedPerson3
        infectedPerson4.updateCoordinates(51.460774f, -0.1692377f);

        System.out.println("-- Geofence positions now changed --");

        // Perform a walk backwards
        for (int i = journey.length - 1; i > 0; i--) {

            StreamEvent evt = new StreamEvent("geoTest");
            evt.setEventTime(timeStamp);
            evt.addValue(uuid, trackingKey, "1234567890");
            evt.addValue(uuid, "latitude", journey[i][0]);
            evt.addValue(uuid, "longitude", journey[i][1]);

            evt = processor.apply(evt, null);

            GeoTrackingInfo geoTrackingInfo = ((GeoTrackingInfo) evt.getValue("geoTrackingInfo"));
            if (geoTrackingInfo != null && !geoTrackingInfo.getGeoFenceOccupancyState().isEmpty()) {
                geofencesVisited.add(geoTrackingInfo.getGeoFenceOccupancyState());
                System.out.println("Reverse Journey: " + geoTrackingInfo.getGeoFenceOccupancyState());
            }
            timeStamp += 3250;
        }
        assertEquals(6,geofencesVisited.size());
    }
}
