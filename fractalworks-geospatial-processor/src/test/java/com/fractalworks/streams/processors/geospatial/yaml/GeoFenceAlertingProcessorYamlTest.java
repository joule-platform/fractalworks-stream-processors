/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.yaml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.processors.geospatial.EntityGeoTrackerProcessor;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Lyndon Adams
 */
public class GeoFenceAlertingProcessorYamlTest {

    @Test
    public void testValidGeoFenceAlertingProcessor() throws IOException, ProcessorException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeoFenceAlertingProcessor.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("dsl");
        EntityGeoTrackerProcessor processor = readMapper.readValue(file, EntityGeoTrackerProcessor.class);
        processor.initialize( null);
        assertNotNull(processor);
    }
}