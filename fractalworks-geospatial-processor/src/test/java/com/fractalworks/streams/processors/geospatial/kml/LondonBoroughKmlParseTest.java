/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.kml;

import com.fractalworks.streams.processors.geospatial.types.country.LocalRegion;
import com.fractalworks.streams.processors.geospatial.utils.parsers.KMLToDomainTransformer;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Simple test to ensure a London borough KML file can be parse in to a domain object ready
 * to be saved in to a store.
 */
public class LondonBoroughKmlParseTest {

    private final String filename = "kml/londonboroughs.kml";

    @Test
    public void parseKmlFileToDomainTest() {
        URL url = ClassLoader.getSystemResource(filename);
        String filePath = url.getPath();

        KMLToDomainTransformer transformer = new KMLToDomainTransformer();

        List<LocalRegion> boroughs = transformer.transformKmlToLocalRegions(new File(filePath));
        assertEquals( 33, boroughs.size());
    }
}

