/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.Triple;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoDirection;
import com.fractalworks.streams.processors.geospatial.utils.GeoMath;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the GeoMath functions
 * <p>
 * Created by Lyndon Adams
 */
public class GeoMathTest {

    @Test
    public void getSpeedAndDirectionBetweenPointsTest() {

        int time = 60;
        float lat_start = 51.456821f;
        float lng_start = -0.164746f;

        // South 51.4420535,-0.1618208
        float lat_end = 51.4420535f;
        float lng_end = -0.1618208f;

        Triple<Double, GeoDirection, Double> speedDirection = GeoMath.getSpeedAndDirectionBetweenPoints(lat_start, lng_start, lat_end, lng_end, time);
        assertEquals(GeoDirection.S, speedDirection.getY());


        // North East
        lat_end = 51.4734366f;
        lng_end = -0.1446331f;

        speedDirection = GeoMath.getSpeedAndDirectionBetweenPoints(lat_start, lng_start, lat_end, lng_end, time);
        assertEquals(GeoDirection.NE, speedDirection.getY());

        // East 51.4523843,-0.1241197
        lat_end = 51.4523843f;
        lng_end = -0.1241197f;

        speedDirection = GeoMath.getSpeedAndDirectionBetweenPoints(lat_start, lng_start, lat_end, lng_end, time);
        assertEquals(GeoDirection.E, speedDirection.getY());
    }

    @Test
    public void testlatLngConversion() {
        int level = 6;
        double latitude = 53.998888;
        double longitude = -2.791294;

        Tuple<Double, Double> tile = GeoMath.convertWGS84toQuadTile(latitude, longitude, level);
        assertEquals( Double.valueOf( 198.000f), tile.getX());
        assertEquals( Double.valueOf(129.000f), tile.getY());
    }

    @Test
    public void distanceBetweenPoints() {

        float hlatitude = 51.456821f;
        float nlongitude = -0.164746f;

        // Clapham Junction Stn Falcon Road (Stop H), London SW11, UK
        float slatitude = 51.463751f;
        float slongitude = -0.167763f;
        double distanceMeters = GeoMath.distanceBetweenPointsByMeter(hlatitude, nlongitude, slatitude, slongitude);
        assertEquals( Double.valueOf(797.9392804187437), distanceMeters, 0.0d);
    }
}
