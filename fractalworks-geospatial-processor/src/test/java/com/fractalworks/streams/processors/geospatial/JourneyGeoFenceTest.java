/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoTrackingInfo;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test that performs a journey from London Waterloo to St Pauls
 *
 * @author Lyndon Adams
 */
public class JourneyGeoFenceTest {

    List<Tuple<Float, Float>> journey;
    Map<Integer, Person> contactPersons = new HashMap<>();
    EntityGeoTrackerProcessor processor;
    long time = System.currentTimeMillis();
    String trackingKey = "metPerson";
    private final String trip = "-0.1125892,51.5015798,0 -0.1125892,51.5015798,0 -0.1126267,51.50154590000001,0 -0.1126215,51.5015447,0 -0.112614,51.5015428,0 -0.1126122,51.5015422,0 -0.1126034,51.50153899999999,0 -0.1126017,51.5015383,0 -0.1125952,51.5015352,0 -0.1125922,51.5015335,0 -0.112578,51.50152270000001,0 -0.112577,51.50152169999999,0 -0.1125732,51.5015172,0 -0.1125723,51.501516,0 -0.1125701,51.5015125,0 -0.1125677,51.5015076,0 -0.1125668,51.5015051,0 -0.1125656,51.50150010000001,0 -0.1125654,51.50149890000001,0 -0.1125673,51.50148249999999,0 -0.1124758,51.50150840000001,0 -0.1124302,51.5015226,0 -0.1123899,51.5015376,0 -0.1123537,51.5015552,0 -0.1122842,51.501596,0 -0.1122425,51.5016223,0 -0.1121638,51.501672,0 -0.1120424,51.5017521,0 -0.1118792,51.50186540000001,0 -0.111851,51.5018863,0 -0.1118148,51.5019095,0 -0.1117782,51.5019308,0 -0.111731,51.501956,0 -0.1116954,51.5019726,0 -0.1116651,51.5019848,0 -0.1116087,51.5020028,0 -0.1115682,51.5020144,0 -0.1115203,51.5020256,0 -0.1114693,51.5020346,0 -0.1114247,51.5020408,0 -0.1113766,51.50204519999999,0 -0.1113143,51.5020472,0 -0.1112718,51.5020462,0 -0.1112457,51.5020419,0 -0.1111597,51.50202289999999,0 -0.1111084,51.50201,0 -0.1110463,51.5019924,0 -0.1109299,51.5019557,0 -0.1107451,51.5018942,0 -0.1104902,51.50180949999999,0 -0.110434,51.50188559999999,0 -0.1104097,51.5019155,0 -0.1103672,51.50196080000001,0 -0.110333,51.5019927,0 -0.1103009,51.5020207,0 -0.1102456,51.5020618,0 -0.1099524,51.5022253,0 -0.110216,51.5024427,0 -0.1103149,51.502523,0 -0.1104395,51.5026321,0 -0.1105678,51.5027473,0 -0.1106672,51.5028326,0 -0.1108294,51.5029814,0 -0.1110218,51.503158,0 -0.111185,51.5033064,0 -0.1113169,51.5034265,0 -0.1114824,51.503574,0 -0.1116478,51.5037214,0 -0.1116891,51.5037582,0 -0.1117512,51.5038105,0 -0.111869,51.503905,0 -0.111928,51.50395280000001,0 -0.1121031,51.5040947,0 -0.1121682,51.5041337,0 -0.1122545,51.5041866,0 -0.1123194,51.5042218,0 -0.1124171,51.5042727,0 -0.1125456,51.5043282,0 -0.1126169,51.50435169999999,0 -0.1127075,51.5043803,0 -0.1128772,51.5044238,0 -0.1129788,51.50442160000001,0 -0.1130437,51.5044276,0 -0.1131477,51.50445070000001,0 -0.1132898,51.5044644,0 -0.1133823,51.5044803,0 -0.1134589,51.50449,0 -0.1135272,51.5044937,0 -0.1135647,51.504497,0 -0.1135989,51.50450119999999,0 -0.113668,51.5045103,0 -0.1137632,51.5045362,0 -0.1138276,51.5045579,0 -0.1138772,51.5045763,0 -0.1139194,51.5045974,0 -0.1139241,51.5045997,0 -0.1139644,51.5046239,0 -0.114014,51.50465730000001,0 -0.1140435,51.5046857,0 -0.1140717,51.504714,0 -0.1140918,51.5047441,0 -0.1141197,51.50479080000001,0 -0.1140999,51.5049789,0 -0.1141083,51.5050715,0 -0.1141037,51.5052565,0 -0.1140987,51.5053162,0 -0.1140969,51.5053429,0 -0.1140998,51.5053549,0 -0.1141038,51.5053714,0 -0.1141173,51.5054096,0 -0.1141388,51.5054473,0 -0.1141704,51.5054859,0 -0.1144109,51.5057525,0 -0.1144193,51.5057608,0 -0.1146773,51.5060422,0 -0.1147538,51.5061284,0 -0.1148303,51.5062145,0 -0.1148735,51.506263,0 -0.1149438,51.50634160000001,0 -0.1155754,51.5070478,0 -0.1157248,51.5072131,0 -0.1158067,51.50730679999999,0 -0.1158886,51.5074004,0 -0.11643,51.50802999999999,0 -0.1167312,51.5083802,0 -0.1169835,51.5086615,0 -0.1171244,51.5088186,0 -0.117347,51.50906810000001,0 -0.1174365,51.5091689,0 -0.117508,51.5092377,0 -0.1175732,51.5093075,0 -0.1176473,51.50936059999999,0 -0.1176808,51.5093917,0 -0.1179643,51.5096952,0 -0.1180256,51.5097544,0 -0.1181328,51.5098718,0 -0.1182361,51.5099909,0 -0.118374,51.5101409,0 -0.1187232,51.51057260000001,0 -0.1189301,51.51082279999999,0 -0.1190368,51.51098509999999,0 -0.1191463,51.51115160000001,0 -0.1192243,51.5112517,0 -0.1192537,51.511295,0 -0.1192914,51.5113432,0 -0.1191957,51.5114002,0 -0.1191359,51.5114487,0 -0.1191115,51.5114719,0 -0.1190856,51.5115024,0 -0.1190685,51.5115307,0 -0.1190558,51.5115565,0 -0.1190413,51.5115929,0 -0.1190325,51.5116408,0 -0.1190218,51.51170280000001,0 -0.1190098,51.5117471,0 -0.118987,51.51180699999999,0 -0.1189885,51.5118305,0 -0.1189865,51.51187449999999,0 -0.1189825,51.5119334,0 -0.1189685,51.5120061,0 -0.1189441,51.5120957,0 -0.1189247,51.5121481,0 -0.1188835,51.5122323,0 -0.1188328,51.5123225,0 -0.1187972,51.5123772,0 -0.1187542,51.51243460000001,0 -0.1187421,51.5124508,0 -0.1186803,51.5125278,0 -0.1185876,51.5126211,0 -0.1184974,51.512697,0 -0.1183295,51.5128157,0 -0.1181222,51.5129535,0 -0.1178973,51.5130706,0 -0.1175012,51.51317540000001,0 -0.1171832,51.51326569999999,0 -0.1170446,51.5132971,0 -0.1169238,51.5133159,0 -0.1167869,51.5133321,0 -0.1166735,51.5133402,0 -0.1165131,51.5133408,0 -0.1163215,51.5133362,0 -0.1161491,51.5133238,0 -0.1160135,51.5133091,0 -0.1158185,51.5132723,0 -0.1156179,51.51322860000001,0 -0.1155069,51.51320269999999,0 -0.1154032,51.5131761,0 -0.1153,51.513156,0 -0.1148573,51.5131068,0 -0.1147301,51.5131187,0 -0.1143102,51.5132184,0 -0.1142836,51.5132247,0 -0.1138173,51.5133054,0 -0.1135166,51.5133614,0 -0.1132382,51.5134213,0 -0.1131616,51.5134344,0 -0.1131335,51.513438,0 -0.1131233,51.5134381,0 -0.1131148,51.5134372,0 -0.1131012,51.5134334,0 -0.1130249,51.5134091,0 -0.1130038,51.51341459999999,0 -0.1125872,51.5135233,0 -0.1124701,51.5135527,0 -0.1123683,51.51357669999999,0 -0.1120112,51.513661,0 -0.1118129,51.51369969999999,0 -0.1116092,51.5137394,0 -0.1115211,51.5137594,0 -0.1113361,51.513791,0 -0.1111839,51.5138147,0 -0.1110428,51.5138372,0 -0.1108564,51.5138651,0 -0.1104924,51.5139211,0 -0.1103719,51.5139435,0 -0.1102427,51.5139674,0 -0.1101727,51.5139803,0 -0.1100797,51.5139993,0 -0.1099926,51.5140177,0 -0.1097295,51.5140783,0 -0.1094287,51.514139,0 -0.109269,51.5141491,0 -0.1091917,51.514155,0 -0.1089933,51.5141685,0 -0.1088286,51.51418339999999,0 -0.1086488,51.5141942,0 -0.1084838,51.514204,0 -0.1082528,51.5142157,0 -0.1079636,51.5142243,0 -0.1077259,51.5142292,0 -0.1074031,51.5142246,0 -0.1072459,51.5142209,0 -0.107116,51.5142175,0 -0.1069655,51.51421249999999,0 -0.1068522,51.514214,0 -0.1065634,51.51420739999999,0 -0.1065118,51.5142062,0 -0.106226,51.5142022,0 -0.1061299,51.5141972,0 -0.1059828,51.5141898,0 -0.1058931,51.5141872,0 -0.1057528,51.5142126,0 -0.105475,51.5142092,0 -0.1052199,51.514211,0 -0.1052077,51.5142108,0 -0.1051292,51.51420960000001,0 -0.1049877,51.5142083,0 -0.1048583,51.514207,0 -0.1043548,51.5141946,0 -0.104026,51.5141869,0 -0.103759,51.5141821,0 -0.1036052,51.514175,0 -0.1034612,51.51416770000001,0 -0.1033841,51.5141594,0 -0.1033325,51.5141562,0 -0.1032852,51.5141569,0 -0.1032527,51.5141565,0 -0.1031001,51.5141383,0 -0.103065,51.5141284,0 -0.1030179,51.514108,0 -0.1029394,51.51407129999999,0 -0.1028759,51.5140602,0 -0.1028096,51.5140504,0 -0.1026561,51.5140259,0 -0.1025242,51.5140035,0 -0.1019666,51.51390880000001,0 -0.1019508,51.5139061,0 -0.1014822,51.5138445,0 -0.1014358,51.5138383,0 -0.1011978,51.513814,0 -0.1010035,51.5137962,0 -0.1007068,51.513776,0 -0.1006328,51.5137666,0 -0.1005587,51.51376,0 -0.1005006,51.51375259999999,0 -0.100476,51.51374910000001,0 -0.1004475,51.513743,0 -0.1003934,51.5137285,0 -0.1003371,51.5137019,0 -0.1002298,51.51364210000001,0 -0.1001744,51.51361229999999,0 -0.1001191,51.5135834,0 -0.1001035,51.5135747,0 -0.1000753,51.5135601,0 -0.09997490000000001,51.5135132,0 -0.0999456,51.513502,0";

    @BeforeEach
    public void loadJourney() throws ProcessorException {
        // Build journey map
        journey = new ArrayList<>();
        String[] tokens = trip.split(" ");
        for (int i = 0; i < tokens.length; i++) {
            String[] co = tokens[i].split(",");
            journey.add(new Tuple<>(Float.parseFloat(co[1]), Float.parseFloat(co[0])));
        }

        // Create set of persons whom Lyndon comes in contact
        contactPersons.put("Jennie".hashCode(), new Person("Jennie", journey.get(10)));
        contactPersons.put("India".hashCode(), new Person("India", journey.get(20)));

        // Process setup
        processor = new EntityGeoTrackerProcessor();
        processor.setTrackingEventKey(trackingKey);
        processor.setMinDwellingTime(2500);
        processor.initialize(null);

        // Add persons geofence
        contactPersons.forEach((integer, person) -> {
            processor.addGeoFence(person.getGeoFence());
        });
    }

    @Test
    public void runJourneyTest() throws StreamsException {
        Person lyndon = new Person("Lyndon", journey.get(0));
        List<Map<Integer, GeoFenceConstants>> geofencesVisited = new ArrayList<>();

        journey.forEach(e -> {
            lyndon.updateLocation(e);

            UUID uuid = UUID.randomUUID();

            // Build event
            StreamEvent evt = new StreamEvent("meetingTracing");
            evt.setEventTime(time);
            evt.addValue(uuid, trackingKey, lyndon);
            evt.addValue(uuid, "latitude", e.getX());
            evt.addValue(uuid, "longitude", e.getY());

            try {
                // Process and receive response
                evt = processor.apply(evt, null);

                // This is a enricher processor
                if (evt.contains(processor.getResponseField())) {
                    GeoTrackingInfo geoTrackingInfo = ((GeoTrackingInfo) evt.getValue(processor.getResponseField()));

                    // A store look up
                    Person trackingPerson = (Person) evt.getValue(trackingKey);
                    if (!geoTrackingInfo.getGeoFenceOccupancyState().isEmpty()) {
                        Map<Integer, GeoFenceConstants> c = geoTrackingInfo.getGeoFenceOccupancyState();
                        c.forEach((id, geoInfo) -> {
                            if (geoInfo.equals(GeoFenceConstants.ENTERED)) {
                                trackingPerson.addInfected(id);
                            }
                        });
                    }
                }
            } catch (StreamsException ex) {
                ex.printStackTrace(System.out);
            }

            // 5 second delay between points
            time += 5000;
        });

        lyndon.cameInContact().forEach(id -> {
            System.out.println(String.format("Lyndon met %s", contactPersons.get(id).getName()));
        });

        assertEquals( 2, lyndon.cameInContact().size());
    }

    /**
     * Internal class to represent a person using a GeoFence for location
     */
    class Person {
        String name;
        GeoFence f;
        Set<Integer> peopleInfected = new HashSet<>();

        public Person(String name, Tuple<Float, Float> initialLocation) {
            this.name = name;
            this.f = new GeoFence(name.hashCode(), initialLocation.getX(), initialLocation.getY(), 4f);
        }

        public void updateLocation(Tuple<Float, Float> location) {
            f.updateCoordinates(location.getX(), location.getY());
        }

        public GeoFence getGeoFence() {
            return f;
        }

        public void addInfected(Integer id) {
            peopleInfected.add(id);
        }

        public Set<Integer> cameInContact() {
            return peopleInfected;
        }

        public String getName() {
            return name;
        }
    }
}
