/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.domain.telco.CellTower;
import com.fractalworks.streams.processors.geospatial.domain.telco.CellTowerArrowParser;
import com.fractalworks.streams.processors.geospatial.domain.telco.CellTowerStore;
import com.fractalworks.streams.processors.geospatial.domain.telco.RadioType;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataLoaderSpecification;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.util.file.ReferenceDataFileProcessingTask;
import org.apache.arrow.dataset.file.FileFormat;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Lyndon Adams
 */
public class GeoSearchProcessorTest {
    private static final String CELLTOWER_FILE = "src/test/resources/data/mini_cells.csv";
    public static UUID uuid = UUID.randomUUID();


    @Test
    public void validateSearchByCellTowers() throws Exception {

        var searchTree = new QuadTree<GeoNode>(512,512, 6, 601);
        searchTree.initialise();

        ReferenceDataLoaderSpecification referenceDataLoaderSpecification = new ReferenceDataLoaderSpecification();
        referenceDataLoaderSpecification.setParser( CellTowerArrowParser.class);
        referenceDataLoaderSpecification.setSource(CELLTOWER_FILE);

        GeoSearchProcessor geoSearchUMTSProcessor = new GeoSearchProcessor();
        geoSearchUMTSProcessor.setSearchArea(2);
        geoSearchUMTSProcessor.setSearchTree(searchTree);
        geoSearchUMTSProcessor.setSpatialEntities(referenceDataLoaderSpecification);
        geoSearchUMTSProcessor.initialize(null);

        StreamEvent evt = new StreamEvent("geoTest");
        evt.addValue(uuid, "latitude", 52.497415);
        evt.addValue(uuid, "longitude", 13.3503);

        StreamEvent responseEvt = geoSearchUMTSProcessor.apply(evt, null);
        if (responseEvt.contains(geoSearchUMTSProcessor.getResultFieldName())) {
            Collection<?> geoResults = (Collection<?>) responseEvt.getValue(geoSearchUMTSProcessor.getResultFieldName());
            //assertTrue(String.format("GeoSearch results mismatch - Expected 480 received %d", geoResults.size()), geoResults.size() == 480);
            System.out.println("GeoSearch results: " + geoResults.size());
            for (Object o : geoResults) {
                CellTower c = (CellTower) o;
                System.out.println(c);
                assertEquals(RadioType.UMTS, c.getRadioType(), String.format("CellTower mismatch - Expected UMTS recieved %s", c.getRadioType()));
            }

        } else {
            assertTrue(false, "Failed to gain geoSerach response");
        }

    }

    @Test
    public void searchNearByCellTowers() throws Exception {

        var cellTowerStore = new CellTowerStore(512, 512);
        cellTowerStore.setTreeLevels(6);
        cellTowerStore.setMaxElementsPerLevel(601);
        cellTowerStore.initialize();

        // Load celltowers, quadtrees partitioned by RadioType
        File f = new File(CELLTOWER_FILE);
        var task = new ReferenceDataFileProcessingTask<CellTower>((Store)cellTowerStore, f.getName(), f.getAbsolutePath(), FileFormat.CSV);
        task.setMoveFileAfterProcessing(false);
        task.setParser(new CellTowerArrowParser());
        var status = task.call();
        assertEquals(499999, status.processCount());

        GeoSearchProcessor geoSearchUMTSProcessor = new GeoSearchProcessor();
        geoSearchUMTSProcessor.setSearchTree(cellTowerStore.getRadioTypeTree(RadioType.UMTS));
        geoSearchUMTSProcessor.setSearchArea(2);
        geoSearchUMTSProcessor.initialize(null);

        StreamEvent evt = new StreamEvent("geoTest");
        evt.addValue(uuid, "latitude", 52.497415);
        evt.addValue(uuid, "longitude", 13.3503);

        StreamEvent responseEvt = geoSearchUMTSProcessor.apply(evt, null);
        if (responseEvt.contains(geoSearchUMTSProcessor.getResultFieldName())) {
            Collection<?> geoResults = (Collection<?>) responseEvt.getValue(geoSearchUMTSProcessor.getResultFieldName());
            //assertTrue(String.format("GeoSearch results mismatch - Expected 480 received %d", geoResults.size()), geoResults.size() == 480);
            System.out.println("GeoSearch results: " + geoResults.size());
            for (Object o : geoResults) {
                CellTower c = (CellTower) o;
                System.out.println(c);
                assertEquals(RadioType.UMTS, c.getRadioType(), String.format("CellTower mismatch - Expected UMTS received %s", c.getRadioType()));
            }

        } else {
            assertTrue(false, "Failed to gain geoSerach response");
        }
    }

    @Test
    public void validGeoSearchProcessorYamlTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/validSearchProcessor.yaml").getFile());
        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        GeoSearchProcessor processor = readMapper.readValue(file, GeoSearchProcessor.class);
        processor.validate();
        processor.initialize(null);
        assertNotNull(processor);

        StreamEvent evt = new StreamEvent("geoTest");
        evt.addValue(uuid, "latitude", 52.497415);
        evt.addValue(uuid, "longitude", 13.3503);

        StreamEvent responseEvt = processor.apply(evt, null);
        if (responseEvt.contains(processor.getResultFieldName())) {
            Collection<?> geoResults = (Collection<?>) responseEvt.getValue(processor.getResultFieldName());
            //assertTrue(String.format("GeoSearch results mismatch - Expected 480 received %d", geoResults.size()), geoResults.size() == 480);
            System.out.println("GeoSearch results: " + geoResults.size());
            for (Object o : geoResults) {
                CellTower c = (CellTower) o;
                System.out.println(c);
                assertEquals(RadioType.UMTS, c.getRadioType(), String.format("CellTower mismatch - Expected UMTS recieved %s", c.getRadioType()));
            }

        } else {
            assertTrue(false, "Failed to gain geoSerach response");
        }

    }

}