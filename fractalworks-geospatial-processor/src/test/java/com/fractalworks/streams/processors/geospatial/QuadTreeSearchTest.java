/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;


import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geometry.Rectangle;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * QuadTree search tests
 * <p>
 * Created by Lyndon Adams
 */
public class QuadTreeSearchTest extends QuadTreeBase {

    @Test
    public void rectanglePointContainsTest() {
        Rectangle rectangle = new Rectangle(0, 0, 16, 8);
        assertTrue(rectangle.contains(2, 1));
    }

    @Test
    public void intersectQueryTest() {
        QuadTree<Element> quadTree = new QuadTree<>(2, 2, 8, 64, 2, 4);
        for (int x = 2; x < 11; x++) {
            for (int y = 2; y < 11; y++) {
                quadTree.insert(new Element(new Tuple<>((double) x, (double) y)));
            }
        }
        System.out.println(quadTree);

        Rectangle searchArea = new Rectangle(0, 0, 128, 16);
        Collection<Element> results = quadTree.query(searchArea);

        printElelemnts(results);
        assertEquals(8, results.size());
    }

    @Test
    public void withinQueryTest() {
        QuadTree<Element> quadTree = new QuadTree<>(4, 64, 8, 2);
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                quadTree.insert(new Element(new Tuple<>((double) x, (double) y)));
            }
        }

        Rectangle searchArea = new Rectangle(2, 2, 8, 2);
        Collection<Element> results = quadTree.query(searchArea);

        printElelemnts(results);
        assertEquals(2, results.size());
    }

    @Test
    public void encompassesQueryTest() {

        QuadTree<Element> quadTree = new QuadTree<>(8, 8, 8, 64, 2, 4);
        for (int x = 8; x < 17; x++) {
            for (int y = 8; y < 17; y++) {
                quadTree.insert(new Element(new Tuple<>((double) x, (double) y)));
            }
        }
        System.out.println(quadTree);

        Rectangle searchArea = new Rectangle(0, 0, 576, 24);
        Collection<Element> results = quadTree.query(searchArea);
        printElelemnts(results);
        assertEquals(8, results.size() );
    }
}
