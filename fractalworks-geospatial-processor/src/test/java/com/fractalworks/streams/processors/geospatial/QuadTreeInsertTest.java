/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geometry.Rectangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * QuadTree insert test
 * <p>
 * Created by lyndon Adams
 */
public class QuadTreeInsertTest extends QuadTreeBase {

    @Test
    public void rectanglePointContainsTest() {
        Rectangle rectangle = new Rectangle(0, 0, 16, 8);
        assertTrue(rectangle.contains(2, 1));
    }

    @Test
    public void insertPerfectSquareTest() {
        int width = 6;
        int area = width * width;
        QuadTree<Element> quadTree = new QuadTree<>(4, area, width, 3);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < width; y++) {
                quadTree.insert(new Element(new Tuple<>((double) x, (double) y)));
            }
        }

        assertTrue(true);
        System.out.println(quadTree);
    }

    @Test
    public void insertTallRectangleTest() {
        int width = 6;
        int area = 12 * width;
        QuadTree<Element> quadTree = new QuadTree<>(18, area, width, 3);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < 12; y++) {
                quadTree.insert(new Element(new Tuple<>((double) x, (double) y)));
            }
        }
        assertTrue(true);
        System.out.println(quadTree);
    }

}
