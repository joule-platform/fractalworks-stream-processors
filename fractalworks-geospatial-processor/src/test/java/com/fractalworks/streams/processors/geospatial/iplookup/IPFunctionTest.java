/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial.iplookup;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.processors.geospatial.IPAddressToGeoLocationProcessor;
import com.fractalworks.streams.processors.geospatial.domain.ip.IpToCity;
import com.fractalworks.streams.processors.geospatial.domain.ip.IpToCityCSVParser;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataLoaderSpecification;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


/**
 * Created by Lyndon Adams
 */
public class IPFunctionTest {

    public static UUID uuid = UUID.randomUUID();
    public static String fileSource = "src/test/resources/data/mini_ipaddress.csv";

    /**
     * First Octet:    217
     * Second Octet:   110
     * Third Octet:    18
     * Fourth Octet:   206
     * To calculate the decimal address from a dotted string, perform the following calculation.
     * <p>
     * (first octet * 256³) + (second octet * 256²) + (third octet * 256) + (fourth octet)
     * =   (first octet * 16777216) + (second octet * 65536) + (third octet * 256) + (fourth octet)
     * =   (217 * 16777216) + (110 * 65536) + (18 * 256) + (206)
     * =   3647869646
     */
    @Test
    public void convertIpToLongTest() {
        short[] ipAddress = new short[]{217, 110, 18, 206};
        long value = IpToCity.ipToLong(ipAddress);
        long expected = 3647869646l;
        int diff = (int) (expected - value);
        assertEquals(0, diff);
    }

    @Test
    public void parseLingTest() throws Exception {
        String line = "\"1.1.0.0\",\"1.1.1.0\",\"AU\",\"Queensland\",\"Brisbane\"";

        IpToCityCSVParser parser = new IpToCityCSVParser();
        IpToCity converted = parser.translate(line);
        assertNotNull(converted);
    }

    @Test
    public void iPAddressToGeoLocationProcessorTest() throws StreamsException {
        ReferenceDataLoaderSpecification referenceDataLoaderSpecification = new ReferenceDataLoaderSpecification();
        referenceDataLoaderSpecification.setSource(fileSource);


        IPAddressToGeoLocationProcessor processor = new IPAddressToGeoLocationProcessor();
        processor.setIpAddressField("ipAddress");
        processor.setMappingEntities(referenceDataLoaderSpecification);
        processor.initialize(null);

        StreamEvent event = new StreamEvent("geoLocation");
        event.addValue(uuid, "ipAddress", "1.42.231.12");
        var response = processor.apply(event, null);
        var country = ((Map)response.getValue("ipGeoLocation")).get("country");
        assertEquals("AU", country);
    }
}
