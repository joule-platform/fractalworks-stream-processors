/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;


import com.fractalworks.streams.processors.geospatial.types.geo.GeoFence;
import com.fractalworks.streams.processors.geospatial.types.QuadTree;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoNode;
import com.fractalworks.streams.sdk.referencedata.ReferenceData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Lyndon Adams
 */
public class FriendsNearMeTest {

    static Friend jennie = new Friend(1, "jennie");
    static Friend india = new Friend(2, "india");
    static Friend sarah = new Friend(3, "sarah");
    static Friend dave = new Friend(4, "dave");
    static Friend helen = new Friend(5, "helen");
    static Friend sam = new Friend(6, "sam");
    static Friend josh = new Friend(7, "josh");
    static Friend finley = new Friend(8, "finley");
    static Friend freyer = new Friend(9, "freyer");
    static Friend andrew = new Friend(10, "andrew");
    static Friend lyndon = new Friend(11, "lyndon");


    static QuadTree<GeoNode<Friend>> friendsTree;

    @BeforeAll
    public static void setUp() {

        jennie.addFriend(2);
        jennie.addFriend(11);

        lyndon.addFriend(1);
        lyndon.addFriend(2);

        india.addFriend(1);
        india.addFriend(11);

        GeoNode<Friend>[] peoplePoints = new GeoNode[]{
                new GeoNode<>(51.456821f, -0.164746f, jennie),
                new GeoNode<>(51.456821f, -0.164746f, india),
                new GeoNode<>(51.457018f, -0.1622072f, sarah),
                new GeoNode<>(51.457018f, -0.1622072f, dave),
                new GeoNode<>(51.467766f, -0.2987533f, helen),
                new GeoNode<>(51.467766f, -0.2987533f, sam),
                new GeoNode<>(51.467766f, -0.2987533f, josh),
                new GeoNode<>(51.457018f, -0.1622072f, finley),
                new GeoNode<>(51.457018f, -0.1622072f, freyer),
                new GeoNode<>(51.467766f, -0.2987533f, andrew),
                new GeoNode<>(51.456821f, -0.164746f, lyndon)
        };

        // Create and build tree
        friendsTree = new QuadTree<>(-180f, -90f, 360, 64800);
        Arrays.stream(peoplePoints).forEach(friendsTree::insert);
        System.out.println(friendsTree);
    }

    @Test
    public void search() {
        GeoFence meHomeFence = new GeoFence(51.456821f, -0.164746f, 2);
        Collection<GeoNode<Friend>> results = friendsTree.query(meHomeFence);
        results.forEach(System.out::println);
        assertEquals(3, results.size());

        GeoFence meHomeMidPointFence = new GeoFence(51.457018f, -0.1622072f, 2);
        results = friendsTree.query(meHomeMidPointFence);
        for (GeoNode<Friend> p : results) {
            System.out.println("Result 2 after moving to Sarah house: " + p);
        }
        assertEquals(4, results.size());
    }

    static class Friend implements ReferenceData {
        Integer id;
        String name;

        Set<Integer> friendsId;

        public Friend(Integer id) {
            this.id = id;
            friendsId = new HashSet<>();
        }

        public Friend(Integer id, String name) {
            this(id);
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void addFriend(int id) {
            friendsId.add(id);
        }


        @Override
        public Integer getKey() {
            return null;
        }

        @Override
        public Friend getData() {
            return this;
        }

        @Override
        public String toString() {
            return "Friend{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", friendsId=" + friendsId +
                    '}';
        }
    }
}
