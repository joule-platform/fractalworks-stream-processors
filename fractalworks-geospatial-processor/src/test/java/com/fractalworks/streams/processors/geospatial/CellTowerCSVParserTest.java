/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.geospatial;

import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.processors.geospatial.domain.telco.*;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.util.file.ReferenceDataFileProcessingTask;
import org.apache.arrow.dataset.file.FileFormat;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Created by Lyndon Adams
 */
public class CellTowerCSVParserTest {

    private static final String CELLTOWER_FILE = "src/test/resources/data/mini_cells.csv";

    @Test
    public void parseLineToCellTower() throws TranslationException {
        String line = "GSM,262,2,12000,37915,,10.825504,47.356703,0,1,1,1484631020,1484631020,-57";

        CellTower cellTower = null;

        CellTowerCSVParser parser = new CellTowerCSVParser();
        long s = System.currentTimeMillis();
        for (int k = 0; k < 10; k++) {
            s = System.currentTimeMillis();
            for (int i = 0; i < 10000; i++) {
                cellTower = parser.translate(line);
            }
        }
        System.out.println("Time taken: " + (System.currentTimeMillis() - s));
        assertNotNull(cellTower);
    }

    @Test
    public void parseFile() throws Exception {
        CellTowerStore cellTowerStore = new CellTowerStore(250, 250);
        cellTowerStore.setMaxElementsPerLevel(10000);
        cellTowerStore.setTreeLevels(5);
        cellTowerStore.initialize();

        File f = new File(CELLTOWER_FILE);
        var task = new ReferenceDataFileProcessingTask<CellTower>((Store)cellTowerStore, f.getName(), f.getAbsolutePath(), FileFormat.CSV);
        task.setMoveFileAfterProcessing(false);
        task.setParser(new CellTowerArrowParser());
        var status = task.call();
        assertEquals(RadioType.values().length, cellTowerStore.size() );
        assertEquals(499999, status.processCount());
    }
}
