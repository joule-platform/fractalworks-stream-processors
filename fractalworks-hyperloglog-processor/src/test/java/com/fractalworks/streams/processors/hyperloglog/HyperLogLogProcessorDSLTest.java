/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.hyperloglog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

/**
 * HypeLogLog processor DSL tests
 *
 * @author Lyndon Adams
 */
class HyperLogLogProcessorDSLTest {

    @ParameterizedTest
    @ValueSource(strings = {"dsl/valid/defaultHyperLogLogCounter.yaml",
            "dsl/valid/hash32HyperLogLogCounter.yaml",
            "dsl/valid/hash64HyperLogLogCounter.yaml"})
    void validDefaultHyperLogLogYamlTest(String filename) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        HyperLogLogProcessor processor = readMapper.readValue(file, HyperLogLogProcessor.class);

        Assertions.assertNotNull(processor);
    }
}
