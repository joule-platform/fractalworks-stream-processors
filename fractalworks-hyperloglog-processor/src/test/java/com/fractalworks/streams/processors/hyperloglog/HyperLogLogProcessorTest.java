/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.hyperloglog;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * HypeLogLog processor functionality test
 *
 * @author Lyndon Adams
 */
public class HyperLogLogProcessorTest {

    private final static String[] symbols = {"IBM", "CISCO", "MSFT", "VMW"};

    @Test
    public void processorTest() throws StreamsException {
        HyperLogLogProcessor processor = new HyperLogLogProcessor();
        processor.setName("distinctSymbols");
        processor.setFields(Arrays.asList("symbol"));
        processor.initialize(null);

        StreamEvent evt = null;

        for (String s : symbols) {
            evt = new StreamEvent();
            evt.addValue("symbol", s);
            processor.apply(evt, null);
        }

        assertEquals(symbols.length,processor.getCount("symbol") );
    }

    @Test
    public void processorDuplicateTest() throws StreamsException {
        HyperLogLogProcessor processor = new HyperLogLogProcessor();
        processor.setName("distinctSymbols");
        processor.setFields(Arrays.asList("symbol"));
        processor.initialize(null);

        for (int i = 0; i < 10; i++) {
            for (String s : symbols) {
                StreamEvent evt = new StreamEvent();
                evt.addValue("symbol", s);
                processor.apply(evt, null);
            }
        }
        assertEquals(symbols.length,processor.getCount("symbol") );
    }

    @Test
    public void processorUseHash32Test() throws StreamsException {
        HyperLogLogProcessor processor = new HyperLogLogProcessor();
        processor.setName("distinctSymbols");
        processor.setFields(Arrays.asList("symbol"));
        processor.setHashType(HyperLogLogProcessor.HASHTYPE.HASH32);
        processor.initialize(null);

        StreamEvent evt = null;

        for (String s : symbols) {
            evt = new StreamEvent();
            evt.addValue("symbol", s);
            processor.apply(evt, null);
        }
        assertEquals(symbols.length,processor.getCount("symbol") );
    }

    @Test
    public void processorUseHash64Test() throws StreamsException {
        HyperLogLogProcessor processor = new HyperLogLogProcessor();
        processor.setName("distinctSymbols");
        processor.setFields(Arrays.asList("symbol"));
        processor.setHashType(HyperLogLogProcessor.HASHTYPE.HASH64);
        processor.initialize(null);

        StreamEvent evt = null;

        for (String s : symbols) {
            evt = new StreamEvent();
            evt.addValue("symbol", s);
            evt = processor.apply(evt, null);
        }
        assertEquals(symbols.length,processor.getCount("symbol") );
    }
}
