/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.hyperloglog;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.util.helpers.Murmur3;
import net.agkn.hll.HLL;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;


/**
 * HyperLogLog counter for streaming events. Provide a list of fields to perform distinct counters upon.
 * <p>
 * - log2m default 11
 * - Register default: 5 (range 1 to 8)
 * <p>
 * Uses https://github.com/aggregateknowledge/java-hll implementation
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "hyperloglog")
public class HyperLogLogProcessor extends AbstractProcessor {

    @JsonIgnore
    private Map<String, HLL> fieldHLLMap;

    private HASHTYPE hashType = HASHTYPE.DEFAULT;
    private int log2m = 11;
    private int registryWidth = 5;

    // Fields to perform counting upon
    private List<String> fields;

    public HyperLogLogProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);

        fieldHLLMap = new HashMap<>();
        for (String f : fields) {
            fieldHLLMap.put(f, new HLL(log2m, registryWidth));
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        if (enabled) {
            // Add event to HLL maps
            add(event);

            // Add current counts to passed event
            event.addValue(uuid, name, getCounts());
            metrics.incrementMetric(Metric.PROCESSED);
        }
        return event;
    }

    /**
     * Add <code>StreamEvent</code> required fields to HLL
     *
     * @param event to process
     */
    public void add(StreamEvent event) {
        for (String f : fields) {
            Object o = event.getValue(f);
            if (o != null) {
                long hashCode = (hashType == HASHTYPE.DEFAULT) ? o.hashCode() : overrideHashCode(o);
                fieldHLLMap.get(f).addRaw(hashCode);
            }
        }
    }

    /**
     * Apply override hash code
     *
     * @param value
     * @return
     */
    private long overrideHashCode(Object value) {
        long hash = 0L;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(value);
            oos.flush();
            byte[] objBytes = bos.toByteArray();

            switch (hashType) {
                case HASH32:
                    hash = Murmur3.hash32(objBytes);
                    break;
                case HASH64:
                    hash = Murmur3.hash64(objBytes);
                    break;

                default:
                    break;
            }

        } catch (IOException e) {
            logger.error(String.format("Unable to build hashcode for %s ", value), e);
        }
        return hash;
    }

    /**
     * Clear the HLL
     */
    public void clear(String field) {
        if (fieldHLLMap.containsKey(field)) {
            fieldHLLMap.get(field).clear();
        }
    }

    /**
     * Clear the HLL
     */
    public void clearAll() {
        for (HLL hll : fieldHLLMap.values()) {
            hll.clear();
        }
    }

    /**
     * Get distinct count for a field
     *
     * @return
     */
    @JsonIgnore
    public long getCount(String field) {
        return (fieldHLLMap.containsKey(field)) ? fieldHLLMap.get(field).cardinality() : 0;
    }

    /**
     * Get distinct count
     *
     * @return
     */
    @JsonIgnore
    public Map<String, Long> getCounts() {
        Map<String, Long> hllCounts = new HashMap<>();
        for (Map.Entry<String, HLL> e : fieldHLLMap.entrySet()) {
            hllCounts.put(e.getKey(), e.getValue().cardinality());
        }
        return hllCounts;
    }

    public List<String> getFields() {
        return fields;
    }

    @JsonProperty(required = true, value = "fields")
    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public int getRegistryWidth() {
        return registryWidth;
    }

    @JsonProperty(required = false, value = "registry width")
    public void setRegistryWidth(int registryWidth) {
        this.registryWidth = registryWidth;
    }

    public int getLog2m() {
        return log2m;
    }

    @JsonProperty(required = false, value = "log2m")
    public void setLog2m(int log2m) {
        this.log2m = log2m;
    }

    public HASHTYPE getHashType() {
        return hashType;
    }

    @JsonProperty(required = false, value = "hashType")
    public void setHashType(HASHTYPE hashType) {
        this.hashType = hashType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HyperLogLogProcessor)) return false;
        HyperLogLogProcessor that = (HyperLogLogProcessor) o;
        return log2m == that.log2m &&
                registryWidth == that.registryWidth &&
                Objects.equals(hashType, that.hashType) &&
                Objects.equals(fieldHLLMap, that.fieldHLLMap) &&
                Objects.equals(fields, that.fields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashType, log2m, registryWidth, fieldHLLMap, fields);
    }

    @Override
    public String toString() {
        return "HyperLogLogProcessor{" +
                "name=" + name +
                "hashType=" + hashType +
                ", log2m=" + log2m +
                ", registryWidth=" + registryWidth +
                ", fieldHLLMap=" + fieldHLLMap +
                ", fields=" + fields +
                "} ";
    }

    public enum HASHTYPE {DEFAULT, HASH32, HASH64}
}
