package com.fractalworks.streams.ml.feature.plugins.scripting;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidScriptEvaluationException;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import com.fractalworks.streams.sdk.analytics.stateful.AnalyticsProcessor;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import org.slf4j.LoggerFactory;

@JsonRootName(value = "macro")
public class UserScripting extends AbstractFeatureEngineeringFunction {

    private AnalyticsProcessor analyticsProcessor;
    private String language = "js";
    private String expression;

    public UserScripting() {
        super();
        logger = LoggerFactory.getLogger(this.getClass().getName() + ":" + field);
    }

    @Override
    public void initialize() throws CustomPluginException {
        super.initialize();
        analyticsProcessor = new AnalyticsProcessor();
        analyticsProcessor.setExpression( expression);
        analyticsProcessor.setLanguage( language );
        analyticsProcessor.setVariables( variables);
        analyticsProcessor.setExpandAllAttributes( true);
        try {
            analyticsProcessor.initialize(null);
        } catch (ProcessorException e) {
            throw new CustomPluginException("Scripting processor failed",e);
        }
    }

    @Override
    public Number compute(StreamEvent event) throws CustomPluginException {
        Number value = Double.NaN;
        try {
            value = analyticsProcessor.compute(event, null);
        } catch (InvalidScriptEvaluationException  e) {
            throw new CustomPluginException("Failed expression ", e);
        }
        return value;
    }

    public String getLanguage() {
        return language;
    }

    @JsonProperty(value = "language")
    public UserScripting setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getExpression() {
        return expression;
    }

    @JsonProperty(value = "expression")
    public UserScripting setExpression(String expression) {
        this.expression = expression;
        return this;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( expression!=null && language == null){
            throw new InvalidSpecificationException("language must be provided");
        }
    }
}
