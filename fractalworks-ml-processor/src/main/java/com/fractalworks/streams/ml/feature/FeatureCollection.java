/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Feature engineering function collection
 *
 * @author Lyndon Adams
 */
public class FeatureCollection {

    @JsonIgnore
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName() );

    private List<String> asRawValue;

    protected Map<String, Feature> computableFeatures;

    protected Map<String, Number> variables;

    public FeatureCollection() {
        // REQUIRED
    }

    /**
     * Initializes the feature collection by initializing all the individual features.
     *
     * @param properties the properties used for initialization
     * @throws CustomPluginException if an error occurs during initialization
     */
    public void initialize(Properties properties) throws CustomPluginException {
        if(computableFeatures!=null) {
            for (Map.Entry<String, Feature> entry : computableFeatures.entrySet()) {
                Feature feature = entry.getValue();
                feature.initialize();
            }
        }
    }

    /**
     * Computes a map of features for a given StreamEvent.
     *
     * @param event the StreamEvent to compute features for
     * @return a map containing the computed features
     * @throws CustomPluginException if an error occurs during computation
     */
    public Map<String,Object> compute(StreamEvent event) throws CustomPluginException{
        var results = new ConcurrentHashMap<String,Object>();

        if (asRawValue != null) {
            asRawValue.stream().parallel().forEach(f -> {
                if (event.contains(f)) {
                    // Numbers are immutatable
                    results.put(f, event.getValue(f));
                }
            });
        }
        if(computableFeatures!=null) {
            AtomicReference<CustomPluginException> hasException = new AtomicReference<>();
            computableFeatures.entrySet().stream().parallel().forEach(e-> {
                try {
                    results.put(e.getKey(),e.getValue().compute(event, null));
                } catch (CustomPluginException ex) {
                    logger.error("Feature processing failed for user plugin", ex);
                    hasException.set(ex);
                }
            });
            if(hasException.get()!=null){
                throw hasException.get();
            }
        }
        return results;
    }

    @JsonProperty(value = "as values")
    public FeatureCollection setAsRawValue(List<String> asRawValue) {
        this.asRawValue = asRawValue;
        return this;
    }

    /**
     * Retrieves the raw values of the feature.
     *
     * @return a List of Strings representing the raw values of the feature
     */
    public List<String> getAsRawValue() {
        return asRawValue;
    }

    /**
     * Sets the computable features for the FeatureCollection.
     *
     * @param computableFeatures a map containing the computable features, with feature names as keys and Feature objects as values
     */
    @JsonProperty(value = "compute", required = true)
    public void setFeatures(Map<String, Feature> computableFeatures) {
        this.computableFeatures = computableFeatures;
        // Validate features
        for (Map.Entry<String, Feature> featureEntry : computableFeatures.entrySet()) {
            featureEntry.getValue().setField(featureEntry.getKey());
        }
    }

    /**
     * Sets the variables for the FeatureCollection.
     *
     * @param variables the map containing the variables, with variable names as keys and numbers as values
     * @return the updated FeatureCollection object
     */
    @JsonProperty(value = "variables")
    public FeatureCollection setVariables(Map<String, Number> variables) {
        this.variables = variables;
        return this;
    }

    /**
     * Retrieves the variables used in the feature collection.
     *
     * @return a map containing the variables used in the feature collection, with variable names as keys and numbers as values
     */
    public Map<String, Number> getVariables() {
        return variables;
    }

    public void validate() throws InvalidSpecificationException {
        if( asRawValue == null || computableFeatures == null || computableFeatures.isEmpty()){
            throw new InvalidSpecificationException("features set must be provided and cannot be empty");
        }
        for (Map.Entry<String, Feature> featureEntry : computableFeatures.entrySet()) {
            featureEntry.getValue().validate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeatureCollection that = (FeatureCollection) o;
        return Objects.equals(asRawValue, that.asRawValue) && Objects.equals(computableFeatures, that.computableFeatures) && Objects.equals(variables, that.variables);
    }

    @Override
    public int hashCode() {
        return Objects.hash(asRawValue, computableFeatures, variables);
    }
}