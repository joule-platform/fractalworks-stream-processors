/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature.plugins.transform;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;

/**
 * Log transformation is a data transformation method in which it replaces each variable x with a log(x)
 * where x is a positive number and greater than zero
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "log transform")
public class LogTransform extends AbstractFeatureEngineeringFunction {

    public LogTransform() {
        super();
    }

    @Override
    public Number compute(StreamEvent event) {
        var value = (Number) event.getValue(sourceField);
        return (value != null && value.doubleValue() > 0.0) ? Math.log(value.doubleValue()+1) : Double.NaN;
    }
}
