/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;

/**
 * Machine learning feature builder using a passed StreamEvent
 *
 * Available capabilities:
 *  - As raw value
 *  - Custom engineering function
 *  - Analytical expression
 *
 * @author Lyndon Adams
 */
public class Feature {

    private String field;

    // Feature engineer function
    private FeatureEngineeringFunction engineeringFunction;

    public Feature() {
        // Required constructor
    }

    public void initialize() throws CustomPluginException {
        if( engineeringFunction != null){
            engineeringFunction.setField(field);
            engineeringFunction.initialize();
        }
    }

    public Number compute(StreamEvent event, Context context) throws CustomPluginException {
        return engineeringFunction.compute( event);
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    @JsonProperty(value = "function")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setEngineeringFunction(FeatureEngineeringFunction engineeringFunction) {
        this.engineeringFunction = engineeringFunction;
    }

    @JsonProperty(value = "scripting")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setScriptingFunction(FeatureEngineeringFunction scriptingFunction) {
        this.engineeringFunction = scriptingFunction;
    }

    public void validate() throws InvalidSpecificationException {
        if(field == null || field.isBlank()){
            throw new InvalidSpecificationException("field must be provided");
        }
        if( engineeringFunction != null) {
            engineeringFunction.validate();
        }
    }
}