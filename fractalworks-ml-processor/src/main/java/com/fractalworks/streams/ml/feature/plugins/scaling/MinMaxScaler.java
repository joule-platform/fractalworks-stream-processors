/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.ml.feature.plugins.scaling;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;

/**
 * Transform features by scaling each feature to a given range. This estimator scales and translates each feature
 * individually such that it is in the given range on the training set, e.g., between zero and one. This scaler
 * shrinks the data within the range of -1 to 1 if there are negative values.
 * We can set the range like [0,1] or [0,5] or [-1,1].
 *
 * This Scaler responds well if the standard deviation is small and when a distribution is not Gaussian and
 * is sensitive to outliers.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "minmax scaler")
public class MinMaxScaler extends AbstractFeatureEngineeringFunction {

    private Double min = Double.NaN;
    private Double max = Double.NaN;
    private double[] interval;

    public MinMaxScaler() {
        super();
    }

    @Override
    public void initialize() throws CustomPluginException {
        super.initialize();
        if(variables!=null && !variables.isEmpty()){
            if(variables.containsKey("min")){
                min = variables.get("min").doubleValue();
            }
            if(variables.containsKey("max")){
                max = variables.get("max").doubleValue();
            }
        }
    }

    @Override
    public Number compute(StreamEvent event) {
        Number value = (Number) event.getValue(sourceField);
        var computedFeature = Double.NaN;
        if(value!=null) {
            var fieldValue = value.doubleValue();

            // Is the value an outlier
            if(fieldValue < min || fieldValue > max){
                return computedFeature;
            }

            if(interval!=null) {
                computedFeature = interval[0] + ((fieldValue - min) * (interval[1] - interval[0])) / (max - min);
            } else {
                computedFeature = (fieldValue - min) / (max - min);
            }

            // Reset
            min =  (fieldValue < min) ? fieldValue : min;   // NOSONAR
            max =  (fieldValue > max) ? fieldValue : max;   // NOSONAR
        }
        return computedFeature;
    }

    public double getMin() {
        return min;
    }

    @JsonProperty(value = "min")
    public MinMaxScaler setMin(double min) {
        this.min = min;
        return this;
    }

    public double getMax() {
        return max;
    }

    @JsonProperty(value = "max")
    public MinMaxScaler setMax(double max) {
        this.max = max;
        return this;
    }

    public double[] getInterval() {
        return interval;
    }

    @JsonProperty(value = "interval")
    public MinMaxScaler setInterval(double[] interval) {
        this.interval = interval;
        return this;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(min.isNaN()  || variables != null && !variables.containsKey("min"))
            throw new InvalidSpecificationException("Unable to initialise MinMaxScaler plugin. min setting must be provided");

        if(max.isNaN() && variables == null || variables != null && !variables.containsKey("max"))
            throw new InvalidSpecificationException("Unable to initialise MinMaxScaler plugin. max setting must be provided");
    }
}