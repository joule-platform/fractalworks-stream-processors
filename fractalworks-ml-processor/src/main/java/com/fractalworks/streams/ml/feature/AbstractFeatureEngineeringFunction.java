/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import org.slf4j.Logger;

import java.util.Map;
import java.util.Properties;

/**
 * Abstract feature engineering class
 *
 * @author Lyndon Adams
 */
public abstract class AbstractFeatureEngineeringFunction implements FeatureEngineeringFunction {

    @JsonIgnore
    protected Logger logger;

    protected Properties properties;

    protected Map<String, Number> variables;

    protected String field;

    protected String sourceField;

    public String getField() {
        return field;
    }

    @Override
    public void setField(String field) {
        this.field = field;
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @JsonProperty(value = "variables")
    public void setVariables(Map<String, Number> variables) {
        this.variables = variables;
    }

    public String getSourceField() {
        return sourceField;
    }

    @JsonProperty(value = "source field")
    public void setSourceField(String sourceField) {
        this.sourceField = sourceField;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if(sourceField==null || sourceField.isEmpty())
            throw new InvalidSpecificationException("Source field must be provided");
    }
}
