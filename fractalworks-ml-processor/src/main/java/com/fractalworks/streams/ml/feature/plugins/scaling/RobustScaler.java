/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.ml.feature.plugins.scaling;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;

/**
 * RobustScaler is a median-based scaling method. The formula of RobustScaler is (Xi-Xmedian) / Xiqr,
 * so it is not affected by outliers.
 *
 * Since it uses the interquartile range, it absorbs the effects of outliers while scaling.
 * The interquartile range (Q3 — Q1) has half the data point. If you have outliers that might affect
 * your results or statistics and don’t want to remove them, RobustScaler is the best choice.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "robust scaler")
public class RobustScaler extends AbstractFeatureEngineeringFunction {

    public static final String MEDIAN_FIELD = "median";
    public static final String IQR_FIELD = "iqr";
    public static final String Q1_FIELD = "q1";
    public static final String Q3_FIELD = "q3";
    
    private Double median = Double.NaN;
    private Double iqr = Double.NaN;
    private Double q3 = Double.NaN;
    private Double q1 = Double.NaN;

    public RobustScaler() {
        super();
    }

    @Override
    public void initialize() throws CustomPluginException {
        super.initialize();
        if(variables!=null){
            if(variables.containsKey(MEDIAN_FIELD)){
                median = variables.get(MEDIAN_FIELD).doubleValue();
            }
            if(variables.containsKey(IQR_FIELD)){
                iqr = variables.get(IQR_FIELD).doubleValue();
            }
            if(variables.containsKey(Q3_FIELD)){
                q3 = variables.get(Q3_FIELD).doubleValue();
            }
            if(variables.containsKey(Q1_FIELD)){
                q1 = variables.get(Q1_FIELD).doubleValue();
            }
        }
        if(!iqr.isNaN()) {
            iqr = q3 - q1;
        }
    }

    @Override
    public Number compute(StreamEvent event) throws CustomPluginException {
        Number value = (Number) event.getValue(sourceField);
        var computedFeature = Double.NaN;
        if (value != null) {
            var fieldValue = value.doubleValue();
            computedFeature = (fieldValue - median) / iqr;
        }
        return computedFeature;
    }


    public Double getMedian() {
        return median;
    }

    @JsonProperty(value = MEDIAN_FIELD)
    public RobustScaler setMedian(Double median) {
        this.median = median;
        return this;
    }

    public Double getIqr() {
        return iqr;
    }

    @JsonProperty(value = IQR_FIELD)
    public RobustScaler setIqr(Double iqr) {
        this.iqr = iqr;
        return this;
    }

    public Double getQ3() {
        return q3;
    }

    @JsonProperty(value = Q3_FIELD)
    public RobustScaler setQ3(Double q3) {
        this.q3 = q3;
        return this;
    }

    public Double getQ1() {
        return q1;
    }

    @JsonProperty(value = Q1_FIELD)
    public RobustScaler setQ1(Double q1) {
        this.q1 = q1;
        return this;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(variables == null)
            throw new InvalidSpecificationException("Unable to initialise RobustScaler plugin. Requires parameters must be provided");
        if(median.isNaN() || !variables.containsKey(MEDIAN_FIELD) )
            throw new InvalidSpecificationException("Unable to initialise RobustScaler plugin. median setting must be provided");
        if(q1.isNaN() && iqr.isNaN() && !variables.containsKey(Q1_FIELD) && !variables.containsKey(IQR_FIELD))
            throw new InvalidSpecificationException("Unable to initialise RobustScaler plugin. q1 setting must be provided");
        if(q3.isNaN() && iqr.isNaN() && !variables.containsKey(Q3_FIELD) && !variables.containsKey(IQR_FIELD))
            throw new InvalidSpecificationException("Unable to initialise RobustScaler plugin. q3 setting must be provided");
    }
}