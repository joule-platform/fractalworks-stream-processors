/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature.plugins.transform;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import org.joda.time.DateTime;

/**
 * Provide the day of week from the provided date object to a number between 1 and 7,
 * where start of week is Monday(1)
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "day-of-week transform")
public class DayOfWeekTransform extends AbstractFeatureEngineeringFunction {

    public DayOfWeekTransform() {
        super();
    }

    @Override
    public Number compute(StreamEvent event) {
        var dayOfWeek = -1;
        if( event.contains(sourceField)){
            var dateTime = event.getValue(sourceField);
            if(dateTime instanceof java.time.LocalDate dt){
                dayOfWeek = dt.getDayOfWeek().getValue();
            } else if (dateTime instanceof java.sql.Date dt) {
                dayOfWeek = dt.toLocalDate().getDayOfWeek().getValue();
            } else if (dateTime instanceof DateTime dt) {
                dayOfWeek = dt.getDayOfWeek();
            }
        }
        return dayOfWeek;
    }
}
