/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.ml.feature.plugins.scaling;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;

/**
 * The Standard Scaler assumes data is normally distributed within each feature
 * and scales them such that the distribution centered around 0, with a standard
 * deviation of 1.
 *
 * Centering and scaling happen independently on each feature by computing the
 * relevant statistics on the samples in the training set. If data is not normally
 * distributed, this is not the best Scaler to use.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "standard scaler")
public class StandardScaler extends AbstractFeatureEngineeringFunction {

    public static final String POPULATION_MEAN = "population mean";
    public static final String POPULATION_STD = "population std";
    Double populationMean = Double.NaN;
    Double populationSTD = Double.NaN;

    public StandardScaler() {
        super();
    }

    @Override
    public void initialize() throws CustomPluginException {
        super.initialize();
        if(variables!=null && !variables.isEmpty()){
            if(variables.containsKey(POPULATION_MEAN)){
                populationMean = variables.get(POPULATION_MEAN).doubleValue();
            }
            if(variables.containsKey(POPULATION_STD)){
                populationSTD = variables.get(POPULATION_STD).doubleValue();
            }
        }
    }

    @Override
    public Number compute(StreamEvent event) throws CustomPluginException {
        Number value = (Number) event.getValue(sourceField);
        var computedFeature = Double.NaN;
        if(value!=null) {
            var fieldValue = value.doubleValue();
            computedFeature = (fieldValue - populationMean) / populationSTD;
        }
        return computedFeature;
    }

    public Double getPopulationMean() {
        return populationMean;
    }

    @JsonProperty(value = POPULATION_MEAN)
    public StandardScaler setPopulationMean(Double populationMean) {
        this.populationMean = populationMean;
        return this;
    }

    public Double getPopulationSTD() {
        return populationSTD;
    }

    @JsonProperty(value = POPULATION_STD)
    public StandardScaler setPopulationSTD(Double populationSTD) {
        this.populationSTD = populationSTD;
        return this;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(variables == null)
            throw new InvalidSpecificationException("Unable to initialise StandardScaler plugin. Required variables must be provided");
        if(populationMean.isNaN() && !variables.containsKey(POPULATION_MEAN))
            throw new InvalidSpecificationException("Unable to initialise StandardScaler plugin. populationMean setting must be provided");

        if(populationSTD.isNaN() || !variables.containsKey(POPULATION_STD))
            throw new InvalidSpecificationException("Unable to initialise StandardScaler plugin. populationSTD setting must be provided");
    }
}
