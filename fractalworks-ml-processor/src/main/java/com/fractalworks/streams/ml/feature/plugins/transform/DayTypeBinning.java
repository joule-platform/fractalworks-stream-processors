/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature.plugins.transform;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;

/**
 * Categorise a day into one of two categories where a weekday (Mon-Fri) is assigned as 1 and 2 for (Sat-Sun)
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "day binning")
public class DayTypeBinning extends DayOfWeekTransform {
    enum DAY_CATEGORY { WEEKDAY, WEEKEND}

    public DayTypeBinning() {
        super();
    }

    @Override
    public Number compute(StreamEvent event) {
        var dayOfWeek = super.compute(event);
        return (dayOfWeek.intValue() >5) ? DAY_CATEGORY.WEEKEND.ordinal() : DAY_CATEGORY.WEEKDAY.ordinal();
    }
}
