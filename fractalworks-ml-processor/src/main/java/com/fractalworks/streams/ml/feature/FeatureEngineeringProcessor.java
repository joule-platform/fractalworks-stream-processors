/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;

import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

/**
 * Machine learning feature engineering processor
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "feature engineering")
public class FeatureEngineeringProcessor extends AbstractProcessor {

    public static final String VERSION_STR = "feature_version";
    public static final String PROCESSOR_STR = "feature_processor";

    private boolean versioned = true;

    private FeatureCollection features;

    public FeatureEngineeringProcessor() {
        // Required constructor
    }

    @Override
    public void initialize(Properties properties) throws ProcessorException {
        super.initialize(properties);
        try {
            features.initialize(properties);
        }catch(CustomPluginException e){
            throw new ProcessorException("Custom feature engineering plugin failed",e);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws ProcessorException {
        try {
            Map<String, Object> featureMap = features.compute(event);

            // Tag the feature map with audit information
            if (versioned) featureMap.put(PROCESSOR_STR, name);
            featureMap.put(VERSION_STR, UUID.randomUUID());

            // Add to event
            event.addValue(uuid, name, featureMap);
            metrics.incrementMetric(Metric.PROCESSED);
        }catch (CustomPluginException e){
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw new ProcessorException("Custom feature engineering plugin processing failed",e);
        }
        return event;
    }

    public FeatureCollection getFeatures() {
        return features;
    }

    @JsonProperty(value = "features", required = true)
    public void setFeatures(FeatureCollection features) {
        this.features = features;
    }

    public boolean isVersioned() {
        return versioned;
    }

    @JsonProperty(value = "versioned", required = true)
    public void setVersioned(boolean versioned) {
        this.versioned = versioned;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( name == null || name.isBlank()){
            throw new InvalidSpecificationException("name provided and cannot be blank");
        }
        features.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeatureEngineeringProcessor that = (FeatureEngineeringProcessor) o;
        return versioned == that.versioned && Objects.equals(features, that.features);
    }

    @Override
    public int hashCode() {
        return Objects.hash(versioned, features);
    }
}
