/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.ml.feature.plugins.scaling;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;

/**
 * MaxScaler sets the data between -1 and 1. It scales data according to the absolute maximum, so it is not suitable for outliers.
 * It needs pre-processing like handling with outliers
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "max scaler")
public class MaxScaler extends AbstractFeatureEngineeringFunction {

    private Double absoluteMax = Double.NaN;

    public MaxScaler() {
        super();
    }

    @Override
    public void initialize() throws CustomPluginException {
        super.initialize();
        if (variables != null && variables.containsKey("absolute max")) {
            absoluteMax = variables.get("absoluteMax").doubleValue();
        }
    }

    @Override
    public Number compute(StreamEvent event) throws CustomPluginException {
        var value = (Number) event.getValue(sourceField);
        var computedFeature = Double.NaN;
        if (value != null) {
            var fieldValue = value.doubleValue();
            computedFeature = fieldValue / absoluteMax;
        }
        return computedFeature;
    }

    public Double getAbsoluteMax() {
        return absoluteMax;
    }

    @JsonProperty(value = "absolute max")
    public MaxScaler setAbsoluteMax(Double absoluteMax) {
        this.absoluteMax = Math.abs(absoluteMax);
        return this;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (absoluteMax.isNaN() && variables == null || !variables.containsKey("absolute max"))
            throw new InvalidSpecificationException("Unable to initialise MaxScaler plugin. 'absolute max' setting must be provided");
    }
}

