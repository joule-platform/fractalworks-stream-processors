/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.sdk.functions.CustomPlugin;

import java.util.Map;

/**
 * Feature engineering function interface
 *
 * @author Lyndon Adams
 */
public interface FeatureEngineeringFunction extends CustomPlugin {

    /**
     * Sets the field for the FeatureEngineeringFunction.
     *
     * @param field the field to be set
     */
    void setField(String field);

    /**
     * Computes a number based on the given stream event.
     *
     * @param event the stream event containing the necessary data
     * @return the computed number
     * @throws CustomPluginException if an error occurs during computation
     */
    Number compute(StreamEvent event) throws CustomPluginException;

    /**
     * Sets the variables to be used in the feature engineering process.
     *
     * @param variables the map containing the variables, with variable names as keys and numbers as values
     */
    void setVariables(Map<String, Number> variables);

    /**
     * Sets the source field for the feature engineering function.
     *
     * @param sourceFields the name of the source field to be used for feature engineering
     */
    void setSourceField(String sourceFields);
}
