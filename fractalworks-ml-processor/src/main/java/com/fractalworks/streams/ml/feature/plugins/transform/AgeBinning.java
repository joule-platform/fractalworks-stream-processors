/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature.plugins.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.ml.feature.AbstractFeatureEngineeringFunction;

import java.time.LocalDate;
import java.time.Period;

/**
 * Age binning
 * <p/>
 * Default age bins
 * 0 ->  {0,9},
 * 1 ->  {10,19}
 * 2 ->  {20,29}
 * 3 ->  {30,39}
 * 4 ->  {40,49}
 * 5 ->  {50,59}
 * 6 ->  {60,69}
 * 7 ->  {70,79}
 * 8 ->  {80,89}
 * 9 ->  {90,99}
 * 10 -> {100,109}
 * 11 -> {110,119}
 *
 * @author Lyndon Adams
 *
 */
@JsonRootName(value = "age binning")
public class AgeBinning extends AbstractFeatureEngineeringFunction {

    private boolean asDate = false;
    private LocalDate baselineDate = LocalDate.now();


    private int[][] bins = {
            {0,9},
            {10,19},
            {20,29},
            {30,39},
            {40,49},
            {50,59},
            {60,69},
            {70,79},
            {80,89},
            {90,99},
            {100,109},
            {110,119}};

    public AgeBinning() {
        super();
    }

    @Override
    public Number compute(StreamEvent event) {
        var val = Integer.MIN_VALUE;
        if( event.contains(sourceField)){
            var age = (!asDate) ? (Integer) event.getValue(sourceField) : convertoAge(event.getValue(sourceField));
            for(int idx=0; idx < bins.length; idx++) {
                int lower = bins[idx][0];
                int upper = bins[idx][1];
                if (age >= lower && age <= upper) {
                    val = idx + 1;
                    break;
                }
            }
        }
        return val;
    }

    private Integer convertoAge(Object date){
        var dateOfBirthToAge = 0;
        LocalDate dob = null;
        if(date instanceof java.time.LocalDate dt){
            dob = dt;
        } else if (date instanceof java.sql.Date dt) {
            dob = dt.toLocalDate();
        }

        if(dob != null ) {
            dateOfBirthToAge = Period.between(dob, baselineDate).getYears();
        }
        return dateOfBirthToAge;
    }

    @JsonProperty(value = "bins")
    public AgeBinning setBins(int[][] bins) {
        this.bins = bins;
        return this;
    }

    @JsonProperty(value = "as date")
    public AgeBinning setAsDate(boolean asDate) {
        this.asDate = asDate;
        return this;
    }

    @JsonProperty(value = "base date")
    public AgeBinning setBaselineDate(String baseDate) {
        baselineDate = LocalDate.parse(baseDate);
        return this;
    }
}
