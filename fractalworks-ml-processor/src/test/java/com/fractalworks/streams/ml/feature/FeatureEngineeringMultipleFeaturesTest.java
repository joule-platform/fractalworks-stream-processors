/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test feature engineering using multiple feature requirements
 *
 * @author Lyndon Adams
 */
public class FeatureEngineeringMultipleFeaturesTest {

    @Test
    public void multipleFeatureTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/validDSLFeatureEngineeringProcessor.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        FeatureEngineeringProcessor processor = readMapper.readValue(file, FeatureEngineeringProcessor.class);
        assertNotNull(processor);
        processor.initialize(null);

        StreamEvent evt = new StreamEvent("test");
        evt.addValue("current_age", 23);
        evt.addValue("spend", 10);

        StreamEvent responseEvt = processor.apply( evt, null);
        Map<String, Object> retailProfilingFeatures = (Map<String, Object>) responseEvt.getValue("retailProfilingFeatures");
        assertEquals(3, retailProfilingFeatures.get("age_bin"));
        assertEquals( 1-10/133.00, retailProfilingFeatures.get("spend_ratio"));
    }

}
