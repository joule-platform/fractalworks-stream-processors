package com.fractalworks.streams.ml.feature.plugins;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.ml.feature.plugins.transform.AgeBinning;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AgeBinningTest {

    @Test
    public void ageBinningWithAgeProvideUsingDefaults(){
        AgeBinning binning = new AgeBinning();
        binning.setSourceField("age");

        StreamEvent event = new StreamEvent();
        event.addValue(UUID.randomUUID(),"age",3);
        assertEquals(1, binning.compute(event));;
    }

    @Test
    public void ageBinningWithDateProvideUsingDefaults(){
        AgeBinning binning = new AgeBinning();
        binning.setSourceField("dateOfBirth");
        binning.setAsDate(true);

        StreamEvent event = new StreamEvent();
        event.addValue(UUID.randomUUID(),"dateOfBirth", LocalDate.parse("1974-06-30"));
        assertEquals(5, binning.compute(event));;
    }

    @Test
    public void ageBinningWithCustomBins(){
        AgeBinning binning = new AgeBinning();
        binning.setSourceField("dateOfBirth");
        binning.setAsDate(true);
        binning.setBins(new int[][]{ {0,18}, {19,21}, {22, 40}, {41, 55}, {56,76}});

        StreamEvent event = new StreamEvent();
        event.addValue(UUID.randomUUID(),"dateOfBirth", LocalDate.parse("1974-06-30"));
        assertEquals(4, binning.compute(event));;
    }
}
