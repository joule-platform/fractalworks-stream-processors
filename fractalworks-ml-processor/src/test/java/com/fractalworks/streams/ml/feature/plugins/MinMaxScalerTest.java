package com.fractalworks.streams.ml.feature.plugins;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.ml.feature.plugins.scaling.MinMaxScaler;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MinMaxScalerTest {

    @Test
    public void scaleField() throws CustomPluginException, InvalidSpecificationException {
        MinMaxScaler scalingPlugin = new MinMaxScaler();
        scalingPlugin.setMin(10.00);
        scalingPlugin.setMax(11.00);
        scalingPlugin.setField("scaled_price");
        scalingPlugin.setSourceField("price");
        scalingPlugin.validate();
        scalingPlugin.initialize();

        var prices = new double[]{10.45, 10.35, 10.22, 10.19, 10.23, 10.89};

        StreamEvent event = new StreamEvent();
        event.addValue(UUID.randomUUID(),"price",0.0);
        for(int i=0; i<prices.length; i++){
            event.replaceValue(UUID.randomUUID(),"price",prices[i]);
            var scaledPrice = scalingPlugin.compute(event);
            System.out.println(scaledPrice);
        }
        assertTrue(true);
    }

    @Test
    public void scaleFieldWithInterval(){
        MinMaxScaler scalingPlugin = new MinMaxScaler();
        scalingPlugin.setMin(10.00);
        scalingPlugin.setMax(12.78);
        scalingPlugin.setField("scaled_price");
        scalingPlugin.setSourceField("price");
        scalingPlugin.setInterval(new double[]{9.0,13.0});

        var prices = new double[]{10.45, 10.35, 10.22, 10.19, 10.23, 10.89};

        StreamEvent event = new StreamEvent();
        event.addValue(UUID.randomUUID(),"price",0.0);
        for(int i=0; i<prices.length; i++){
            event.replaceValue(UUID.randomUUID(),"price",prices[i]);
            var scaledPrice = scalingPlugin.compute(event);
            System.out.println(scaledPrice);
        }
        assertTrue(true);
    }
}
