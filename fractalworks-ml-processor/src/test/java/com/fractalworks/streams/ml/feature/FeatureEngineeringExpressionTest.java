/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.ml.feature;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test feature engineering using expression based process
 *
 * @author Lyndon Adams
 */
public class FeatureEngineeringExpressionTest {

    @Test
    public void expressionTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/featureEngineeringProcessorExpressionTest.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");

        FeatureEngineeringProcessor processor = readMapper.readValue(file, FeatureEngineeringProcessor.class);
        assertNotNull(processor);
        processor.initialize(null);


        StreamEvent evt = new StreamEvent("test");
        evt.addValue("sensor", 23);

        var valueExpected = (23-1)/2.71828;
        StreamEvent responseEvt = processor.apply( evt, null);
        var retailProfilingFeatures = (Map<String, Object>) responseEvt.getValue("retailProfilingFeatures");
        assertEquals( valueExpected, retailProfilingFeatures.get("sensor_ratio"));
    }

}
