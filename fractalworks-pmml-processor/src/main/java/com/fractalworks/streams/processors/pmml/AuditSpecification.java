/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.processors.pmml;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;

import java.util.Objects;

/**
 * Audit specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "audit configuration")
public class AuditSpecification {

    private String targetSchema;
    private int queueCapacity = 1000;

    // 30 second data flush
    private long flushFrequency = 60;

    public AuditSpecification() {
        // REQUIRED
    }

    public String getTargetSchema() {
        return targetSchema;
    }

    @JsonProperty(
            value = "target schema",
            required = true
    )
    public AuditSpecification setTargetSchema(String targetSchema) {
        this.targetSchema = targetSchema;
        return this;
    }


    public int getQueueCapacity() {
        return queueCapacity;
    }

    @JsonProperty(
            value = "queue capacity",
            required = false
    )
    public AuditSpecification setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
        return this;
    }

    public long getFlushFrequency() {
        return flushFrequency;
    }

    @JsonProperty(
            value = "flush frequency",
            required = false
    )
    public AuditSpecification setFlushFrequency(long flushFrequency) {
        this.flushFrequency = flushFrequency;
        return this;
    }

    public void validate() throws InvalidSpecificationException {
        if( targetSchema ==null || targetSchema.isEmpty()){
            throw new InvalidSpecificationException("target schema must be provided");
        }
        if( flushFrequency<=0){
            throw new InvalidSpecificationException("flush frequency must be greater than zero");
        }
        if( queueCapacity<=250){
            throw new InvalidSpecificationException("queue capacity must be greater than zero");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditSpecification that = (AuditSpecification) o;
        return queueCapacity == that.queueCapacity && flushFrequency == that.flushFrequency && Objects.equals(targetSchema, that.targetSchema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetSchema, queueCapacity, flushFrequency);
    }
}
