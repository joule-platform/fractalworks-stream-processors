/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.pmml;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.processors.embeddedsql.SQLTapInMemoryProcessor;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.referencedata.S3ReferenceDataEvent;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import org.dmg.pmml.FieldName;
import org.jetbrains.annotations.NotNull;
import org.jpmml.evaluator.*;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * PMML event evaluator
 *
 * Based upon https://jitpack.io/p/jpmml/jpmml-evaluator directions
 * @author Lyndon Adams
 */
@JsonRootName(value = "pmml predictor")
public class PMMLProcessor extends AbstractProcessor {

    private String responseField;
    private String pmmlFile;
    private String modelStoreName;
    private boolean flattenResults = false;

    private SQLTapInMemoryProcessor sqlTapInMemoryProcessor;

    private AuditSpecification auditspec;

    private Evaluator modelEvaluator;
    private String featuresField;

    private AtomicInteger modelReplacementCounter = new AtomicInteger();

    private CountDownLatch pauseLatch = new CountDownLatch(1);

    public PMMLProcessor() {
        super();
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        if( modelStoreName!=null && dataStores!=null){
            var dataStore = dataStores.get(modelStoreName);
            if(dataStore != null){
                initialiseRemoteMLModel((DataStoreQueryInterface<ReferenceDataObject>) dataStore);
            }
            else
                throw new ProcessorException(String.format("%s does not exist in configuration",modelStoreName));

        } else {
            initialiseLocalMLModel(pmmlFile);
        }

        // Test the model works in this environment
        try {
            modelEvaluator.verify();
        }catch(EvaluationException | InvalidMarkupException | UnsupportedMarkupException e){
            throw new ProcessorException("Model failed during environment verification", e);
        }

        // Default features field
        if( featuresField == null) {
            featuresField = name + "_features";
        }

        // Audit prediction along with features used
        if(auditspec!=null){
            initialiseAuditing(auditspec);
        }
        pauseLatch.countDown();
    }

    private void initialiseLocalMLModel(String modelFile) throws ProcessorException {
        try (FileInputStream inputStream = new FileInputStream(modelFile)){
            loadModel(inputStream);
        } catch (IOException e) {
            throw new ProcessorException("Failure during PMML model loading", e);
        }
    }

    private void initialiseRemoteMLModel(DataStoreQueryInterface<ReferenceDataObject> dataStore) throws ProcessorException {
        try {
            // Get model from store
            Optional<ReferenceDataObject> response = dataStore.getStore().getInitialImage(pmmlFile);
            if(response.isPresent()){
                var loadedModel = replaceModel(response.get().getData(),modelStoreName);
                if(loadedModel){
                    if(logger.isInfoEnabled()){
                        logger.info(String.format("Loaded %s from %s data store",pmmlFile,modelStoreName));
                    }
                }else {
                    throw new ProcessorException(String.format("Failed to load %s from %s data store",pmmlFile,modelStoreName));
                }
            } else {
                throw new ProcessorException(String.format("Failed to retrieve %s from %s data store",pmmlFile,modelStoreName));
            }
        } catch (StorageException e) {
            throw new ProcessorException(String.format("Failed to load initial image from %s data store", modelStoreName),e);
        }

        // Register Model update process
        registerForModelUpdates(dataStore);
    }

    private void registerForModelUpdates(DataStoreQueryInterface<?> dataStore){
        dataStore.registerForNotifications((event, context) -> {
            if(event instanceof S3ReferenceDataEvent s3ReferenceDataEvent){
                CompletableFuture
                        .supplyAsync(() -> getModelFromS3(s3ReferenceDataEvent))
                        .thenApply(r -> {
                            var hasReplaced = false;
                            if(r.isPresent()) {
                                hasReplaced = replaceModel(r.get().getData(), s3ReferenceDataEvent.getBucket());
                                if (hasReplaced) {
                                    modelReplacementCounter.incrementAndGet();
                                } else
                                    if(logger.isErrorEnabled())
                                        logger.error(String.format("Failed to replace %s from %s data store",pmmlFile,modelStoreName));
                            }
                            return hasReplaced;
                        });}
        });
    }

    private void initialiseAuditing(AuditSpecification auditSpec) throws ProcessorException {
        sqlTapInMemoryProcessor = new SQLTapInMemoryProcessor();
        sqlTapInMemoryProcessor.setSchemaName(auditSpec.getTargetSchema());
        sqlTapInMemoryProcessor.setCloneEvent(false);
        sqlTapInMemoryProcessor.setFlushEventFreq(auditSpec.getFlushFrequency());
        sqlTapInMemoryProcessor.initialize(null);
    }

    private Optional<ReferenceDataObject> getModelFromS3(S3ReferenceDataEvent s3ReferenceDataEvent){
        DataStoreQueryInterface dataStore = dataStores.get(modelStoreName);

        // Prepare query parameters
        Map<String, String> params = new HashMap<>();
        params.put("versionId", s3ReferenceDataEvent.getVersionId());
        return dataStore.getStore().query(s3ReferenceDataEvent.getObjectname(), params);
    }

    private boolean replaceModel(final Object modelFile,final String location) {
        boolean hasReplaced = false;
        if(modelFile instanceof InputStream inputStream){
            hasReplaced = loadModel(inputStream);
        }else if(modelFile instanceof File file){
            try (FileInputStream inputStream = new FileInputStream(file)){
                hasReplaced = loadModel(inputStream);
            }catch (IOException e){
                if(logger.isErrorEnabled())
                    logger.error(String.format("Failed to install model from %s", location),e);
            }
        } else {
            if(logger.isErrorEnabled())
                logger.error(String.format("Failed to install model from %s", location));
        }
        return hasReplaced;
    }

    private boolean loadModel(InputStream inputStream) {
        boolean success = true;
        try {
            var tmpModelEvaluator = new LoadingModelEvaluatorBuilder()
                    .load(inputStream)
                    .build();

            // Pause event processing
            pauseLatch = new CountDownLatch(1);
            modelEvaluator = tmpModelEvaluator;

        } catch (SAXException | JAXBException e) {
            if(logger.isErrorEnabled())
                logger.error("Failed to load/replace model. Reverting to previous installed model", e);
            success = false;
        } finally {
            // Restart
            pauseLatch.countDown();
        }
        return success;
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {

        // Pause processing if pause latch is activated
        if(pauseLatch!=null) {
            try {
                pauseLatch.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        try {
            if( featuresField != null && !event.contains(featuresField)){
                if(logger.isErrorEnabled())
                    logger.error(String.format("%s defined for features but not found in event, skipping processing", featuresField));
                metrics.incrementMetric(Metric.DISCARDED);
                return event;
            }

            // Build feature vector from either pre-engineered features or from the event
            LinkedHashMap<FieldName, FieldValue> featureVector = createFeatureSpace(event);

            // Evaluating the model with feature vector
            Map<FieldName, ?> results = modelEvaluator.evaluate(featureVector);

            // Decoupling results from the JPMML-Evaluator runtime environment
            Map<String, ?> resultRecord = applyModelResults(results,event);

            // Audit response
            if(sqlTapInMemoryProcessor!=null){
                auditScoring(featureVector, resultRecord);
            }
            metrics.incrementMetric(Metric.PROCESSED);

        }catch (StreamsException e){
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            throw e;
        }
        return event;
    }

    private Map<String, ?> applyModelResults(Map<FieldName, ?> results, StreamEvent event){
        Map<String, ?> resultRecord = EvaluatorUtil.decodeAll(results);
        if (flattenResults) writeFlattenedModelResults(event, resultRecord);
        else {
            event.addValue(uuid, responseField, resultRecord);
        }
        return resultRecord;
    }

    private void writeFlattenedModelResults(StreamEvent event, Map<String, ?> resultRecord){
        resultRecord.entrySet().stream().parallel().forEach( e ->
            event.addValue(uuid, e.getKey(), e.getValue()));
    }

    /**
     * Load feature vector in evaluator format
     *
     * @param event
     * @return
     */
    private LinkedHashMap<FieldName, FieldValue> createFeatureSpace(StreamEvent event) throws StreamsException{
        Map<String, Object> featureMap = (event.contains(featuresField)) ? (Map<String, Object>) event.getValue(featuresField) : event.getDictionary();
        LinkedHashMap<FieldName, FieldValue> arguments = new LinkedHashMap<>();

        // Mapping the record field-by-field from data source schema to PMML schema
        for(InputField inputField : modelEvaluator.getInputFields() ){
            FieldName inputName = inputField.getName();

            if(!featureMap.containsKey(inputName.getValue())){
                throw new StreamsException(String.format("Event does not contain valid value for %s feature",inputName.getValue()));
            }
            Object rawValue = featureMap.get(inputName.getValue());

            // Transforming an arbitrary user-supplied value to a known-good PMML value
            FieldValue inputValue = inputField.prepare(rawValue);
            arguments.put(inputName, inputValue);
        }
        return arguments;
    }


    /**
     * Audit features and prediction
     *
     * @param features
     * @param resultRecord
     * @throws StreamsException
     */
    private void auditScoring(LinkedHashMap<FieldName, FieldValue> features, Map<String, ?> resultRecord ) {
        try {
            final StreamEvent auditEvent = new StreamEvent(name);
            writeFlattenedModelResults(auditEvent, resultRecord);
            features.entrySet().stream().parallel().forEach(e -> {
                var fName = e.getKey().getValue().replace('.','_');
                    auditEvent.addValue(uuid, fName, e.getValue().getValue());
            });

            sqlTapInMemoryProcessor.apply(auditEvent, null);
        }catch (StreamsException e) {
            if(logger.isErrorEnabled())
                logger.error(String.format("Failed to audit %s prediction to in-memory store", name), e);
        }
    }

    public int getModelReplacementCount(){
        return modelReplacementCounter.get();
    }

    public String getFeaturesField() {
        return featuresField;
    }

    @JsonProperty(value = "features field")
    public void setFeaturesField(String featuresField) {
        this.featuresField = featuresField;
    }

    public String getResponseField() {
        return responseField;
    }

    @JsonProperty(value = "response field")
    public void setResponseField(@NotNull String responseField) {
        this.responseField = responseField;
    }

    public String getPmmlFile() {
        return pmmlFile;
    }

    @JsonProperty(value = "model")
    public void setPmmlFile(@NotNull String pmmlFile) {
        this.pmmlFile = pmmlFile;
    }

    public String getModelStoreName() {
        return modelStoreName;
    }

    @JsonProperty(value = "model store")
    public void setModelStoreName(@NotNull String modelStoreName) {
        this.modelStoreName = modelStoreName;
    }

    @JsonProperty(value = "unpack results")
    public void setFlattenResults(boolean flattenResults) {
        this.flattenResults = flattenResults;
    }

    @JsonProperty(value = "audit configuration")
    public void setAudit(AuditSpecification auditspec) {
        this.auditspec = auditspec;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( name==null || name.isEmpty()){
            throw new InvalidSpecificationException("Name must be provided");
        }
        if( pmmlFile==null || modelStoreName == null){
            throw new InvalidSpecificationException("Either or both model and model store must be provided");
        }
        if(!flattenResults && responseField == null){
            throw new InvalidSpecificationException("response field must be provided");
        }
        if(auditspec!=null){
            auditspec.validate();
        }
    }
}
