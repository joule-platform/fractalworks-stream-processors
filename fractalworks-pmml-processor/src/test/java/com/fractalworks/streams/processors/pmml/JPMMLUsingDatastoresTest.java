/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.pmml;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.DuplicateTransportException;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.TransportRegister;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreQueryInterface;
import com.fractalworks.streams.transport.minio.MinioReferenceDataStore;
import com.fractalworks.streams.transport.minio.specification.CredentialsSpecification;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;
import com.fractalworks.streams.transport.minio.specification.referencedata.MinioDataStoreMountPointSpecification;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

/**
 * JPMML Iris test using JPMMLProcessor
 *
 * @author Lyndon Adams
 */
//@DisabledIf()
public class JPMMLUsingDatastoresTest {

    static String BUCKET = "models";
    static String DIRECTORY = "iris/";
    static String MODEL_FILE = "iris_rf.pmml";

    static Map<String, DataStoreQueryInterface<?>> dataStoreMap;

    public static MinioReferenceDataStore getMinioReferenceDataStore(String bucket, String directory, String objectName) throws InvalidSpecificationException, TransportException {

        CredentialsSpecification cs = new CredentialsSpecification();
        cs.setAccessKey("AKIAIOSFODNN7");
        cs.setSecretKey("wJalrXUtnFEMIK7MDENGbPxRfiCY");

        EndpointSpecification es = new EndpointSpecification();
        es.setEndpoint("https://127.0.0.1");
        es.setPort(9000);
        es.setRegion("fernvilla");
        es.setCredentialsSpecification(cs);

        var builder = MinioDataStoreMountPointSpecification.getBuilder();
        builder
                .setName(BUCKET)
                .setEndpointSpecification(es)
                .setBucketId(bucket)
                .setObjectName(objectName)
                .setDirectory(directory);

        MinioReferenceDataStore store = new MinioReferenceDataStore();
        store.setSpecification(builder.build());
        store.initialize();

        return store;
    }

    @BeforeAll
    public static void assignDataStores()
            throws InvalidSpecificationException, StorageException, DuplicateTransportException, ProcessorException, TransportException {

        var referenceDataStore = getMinioReferenceDataStore(BUCKET,DIRECTORY, MODEL_FILE);
        TransportRegister.getInstance().register(referenceDataStore);

        var dataStore = new DataStoreQueryInterface<String>();
        dataStore.setStoreName(BUCKET);
        dataStore.initialise();

        dataStoreMap = new HashMap<>();
        dataStoreMap.put(BUCKET, dataStore);
    }

    @Test
    public void validateInitialImageFromDataStore() throws ProcessorException {
        PMMLProcessor processor = new PMMLProcessor();
        processor.setDataStores( dataStoreMap);
        processor.setPmmlFile( MODEL_FILE );
        processor.setModelStoreName(BUCKET);
        processor.initialize(null);
        assertNotNull( processor);
    }

    @Test
    public void validateModelScoring() throws StreamsException, InterruptedException {
        PMMLProcessor processor = new PMMLProcessor();
        processor.setName("irisScore");
        processor.setFeaturesField("iris_features");
        processor.setDataStores( dataStoreMap);
        processor.setPmmlFile( MODEL_FILE );
        processor.setModelStoreName(BUCKET);
        processor.setResponseField("irisScore");
        processor.initialize(null);
        assertNotNull( processor);

        var features = new HashMap<String,Object>();
        features.put("Sepal.Length", 5.1d);
        features.put("Sepal.Width", 3.5d);
        features.put("Petal.Length", 1.4d);
        features.put("Petal.Width", 0.2d);

        for(int i=0;i<1000;i++ ) {
            StreamEvent event = new StreamEvent("irisPrediction");
            event.addValue("iris_features", features);

            StreamEvent response = processor.apply(event, null);
            String predictedSpecies = (String) ((Map<String,Object>)response.getValue("irisScore")).get("Predicted_Species");
            assertEquals("setosa", predictedSpecies);
        }
    }
    @Test
    public void validateModelReplacementTest() throws ProcessorException {
        PMMLProcessor processor = new PMMLProcessor();
        processor.setDataStores( dataStoreMap);
        processor.setPmmlFile( MODEL_FILE );
        processor.setModelStoreName(BUCKET);
        processor.initialize(null);

        /*
        while(processor.getModelReplacementCount() <1 ) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
         */

        await()
                .atLeast(Duration.of(120, SECONDS))
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .until(checkModelHasUpdated(processor.getModelReplacementCount()));

        assertEquals(1, processor.getModelReplacementCount());
    }

    @BeforeAll
    @AfterAll
    public static void cleanup(){
        FileUtils.deleteQuietly(new File(MODEL_FILE));
    }

    private Callable<Boolean> checkModelHasUpdated(int updatedCount) {
        return new Callable<>() {
            public Boolean call() throws Exception {
                return updatedCount == 1;
            }
        };
    }
}
