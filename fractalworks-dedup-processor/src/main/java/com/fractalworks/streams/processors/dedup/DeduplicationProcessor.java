/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.dedup;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.processor.ProcessorException;
import com.fractalworks.streams.sdk.processor.AbstractProcessor;
import com.google.common.base.Objects;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Event send-on-delta processor which removes events by applying a configured BloomFilter
 *
 * fields - list of fields to determine duplication
 * expectedUniqueElements - expected unique events. This must be calculated correctly otherwise
 *                          false positives will be generated as the number approaches and breaches the provided number.
 *                          Default: 500,000
 *
 * fpp - the desired false positive probability (must be positive and less than 1.0)
 *       Default: 0.97%
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "send on delta")
public class DeduplicationProcessor extends AbstractProcessor {

    private String[] fields;
    private long expectedUniqueElements = 0;
    private double fpp = 0.97;

    private long resetByTime = 0;
    private int resetBySeenEvents = 0;

    private ScheduledExecutorService filerChangeScheduler;
    private Future<?> futureTask;
    private final ReentrantLock bloomFilterUpdateLock = new ReentrantLock(true);
    private AtomicInteger eventSeenLatch;

    private BloomFilter<UniqueDataObject> bloomFilter;

    private CountDownLatch bloomRefreshLatch;

    public DeduplicationProcessor() {
        super();
        super.cloneEvent = false;
    }

    @Override
    public void initialize(Properties prop) throws ProcessorException {
        super.initialize(prop);
        updateBloomFilter();

        if(resetByTime > 0) {
            filerChangeScheduler = Executors.newSingleThreadScheduledExecutor();
            futureTask = filerChangeScheduler.scheduleWithFixedDelay(new ReplaceBloomFilter(), resetByTime, resetByTime, TimeUnit.SECONDS);
        } else if(resetBySeenEvents > 0){
            eventSeenLatch = new AtomicInteger(resetBySeenEvents);
        }
    }

    @Override
    public StreamEvent apply(StreamEvent event, Context context) throws StreamsException {
        if(eventSeenLatch != null && eventSeenLatch.decrementAndGet() < 0){
            updateBloomFilter();
        }

        if(filerChangeScheduler!=null && bloomFilterUpdateLock.isLocked() ) {
            // wait until new bloom filter has been created
            try {
                bloomRefreshLatch.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        return applyBloombByEvent(event);
    }

    /**
     * Applies the bloom filter to the given stream event and returns a response event.
     *
     * @param event The stream event to be processed.
     * @return The response event after applying the bloom filter.
     */
    private StreamEvent applyBloombByEvent(StreamEvent event){
        StreamEvent responseEvent = null;
        Object[] values = new Object[fields.length];
        int i=0;
        for(String field : fields){
            values[i++] = event.getValue( field);
        }
        UniqueDataObject uniqueDataObject = new UniqueDataObject(values);
        if (!bloomFilter.mightContain(uniqueDataObject)) {
            bloomFilter.put(uniqueDataObject);
            if( bloomFilter.expectedFpp() >= fpp ) {
                // Possible false positive case
                event.addValue(uuid, "deltaBreachedFPP", bloomFilter.expectedFpp());

                // Trigger filter replacement
                if( bloomFilter.approximateElementCount() > expectedUniqueElements) {
                    updateBloomFilter();
                }
            }
            responseEvent = event;
            metrics.incrementMetric(Metric.PROCESSED);
        } else {
            metrics.incrementMetric(Metric.DISCARDED);
        }
        return responseEvent;
    }

    private void updateBloomFilter() {
        try {
            bloomFilterUpdateLock.lock();
            bloomRefreshLatch = new CountDownLatch(1);
            if(futureTask !=null){
                futureTask.cancel(false);
                futureTask = filerChangeScheduler.scheduleWithFixedDelay(new ReplaceBloomFilter(), resetByTime, resetByTime, TimeUnit.SECONDS);
            }
            bloomFilter = BloomFilter.create((Funnel<UniqueDataObject>) (from, into) -> into.putInt(from.hashCode()), expectedUniqueElements, fpp);
        }finally {
            if(eventSeenLatch!=null){
                eventSeenLatch.set(resetBySeenEvents);
            }
            logger.debug("Bloom filter initialised");
            bloomRefreshLatch.countDown();
            bloomFilterUpdateLock.unlock();
        }
    }

    @JsonProperty(value = "fields", required = true)
    public void setFields(@Nonnull String[] fields) {
        this.fields = fields;
    }

    public String[] getFields() {
        return fields;
    }

    @JsonProperty(value = "expected unique elements", required = true)
    public void setExpectedUniqueElements(@Nonnull long expectedUniqueElements) {
        this.expectedUniqueElements = expectedUniqueElements;
    }

    public long getExpectedUniqueElements() {
        return expectedUniqueElements;
    }

    @JsonProperty(value = "fpp")
    public void setFpp(@Nonnull double fpp) {
        this.fpp = fpp;
    }

    public double getFpp() {
        return fpp;
    }

    public double getExpectedFpp(){
        return bloomFilter.expectedFpp();
    }

    @JsonProperty(value = "reset by time delay", required = true)
    public void setResetByTime(long resetByTime) {
        this.resetByTime = resetByTime;
    }

    public long getResetByTime() {
        return resetByTime;
    }

    @JsonProperty(value = "reset by event count", required = true)
    public void setResetBySeenEvents(int resetBySeenEvents) {
        this.resetBySeenEvents = resetBySeenEvents;
    }

    public int getResetBySeenEvents() {
        return resetBySeenEvents;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( fields == null || fields.length == 0){
            throw new InvalidSpecificationException("Fields cannot be null or empty");
        }

        if( fpp <= 0.0 || fpp >= 1.0 ){
            throw new InvalidSpecificationException("fpp must be positive and less than 1");
        }

        if(expectedUniqueElements <= 100){
            throw new InvalidSpecificationException("expected unique elements must be positive and greater than 100");
        }

        if(resetBySeenEvents <= 0){
            throw new InvalidSpecificationException("reset by event count must be positive and greater than 0 to enable feature");
        }

        if(resetByTime <= 0){
            throw new InvalidSpecificationException("reset by time delay must be positive and greater than 0 to enable feature");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeduplicationProcessor that = (DeduplicationProcessor) o;
        return expectedUniqueElements == that.expectedUniqueElements && Double.compare(that.fpp, fpp) == 0 && Objects.equal(fields, that.fields) && Objects.equal(bloomFilter, that.bloomFilter);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(fields, expectedUniqueElements, fpp, bloomFilter);
    }

    @Override
    public String toString() {
        return "DeduplicationProcessor{" +
                "fields=" + Arrays.toString(fields) +
                ", expectedUniqueElements=" + expectedUniqueElements +
                ", fpp=" + fpp +
                '}';
    }

    /**
     * Unique data object that encapsulates the required fields
     */
    private static class UniqueDataObject {
        private final Object[] values;

        public UniqueDataObject(@Nonnull Object[] values) {
            this.values = values;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            UniqueDataObject that = (UniqueDataObject) o;
            return Objects.equal(values, that.values);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(values);
        }
    }

    private class ReplaceBloomFilter implements Runnable {
        @Override
        public void run() {
            updateBloomFilter();
        }
    }
}
