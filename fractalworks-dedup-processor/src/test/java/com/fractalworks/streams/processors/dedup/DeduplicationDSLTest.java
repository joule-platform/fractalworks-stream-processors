/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.dedup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DeduplicationDSLTest {
    @Test
    public void validEventEnricherYamlTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/dedup.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        DeduplicationProcessor processor = readMapper.readValue(file, DeduplicationProcessor.class);
        processor.validate();

        assertNotNull(processor);

        String[] fOjs = new String[]{"a","b","c"};
        int i=0;
        for(String field : processor.getFields()){
            assertEquals( field, fOjs[i++]);
        }

        assertEquals( 0.85d, processor.getFpp(), 0 );
        assertEquals( 100000, processor.getExpectedUniqueElements());
        assertEquals( 5000, processor.getResetBySeenEvents());
        assertEquals( 45, processor.getResetByTime());
    }
}
