/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.processors.dedup;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

public class DeduplicationTest {

    static Random rand = new Random();

    public List<StreamEvent> createEvents(int num, boolean randEventType) {
        List<StreamEvent> events = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            String eventType = "live";
            if (randEventType) {
                eventType = (rand.nextBoolean()) ? "live" : "aftermarket";
            }

            StreamEvent evt = new StreamEvent(eventType, 100002);
            evt.addValue("symbol", "IBM");
            evt.addValue("ask", 12.67);
            evt.addValue("bid", 13.67);
            evt.addValue("date", new Date());
            events.add(evt);
        }

        StreamEvent evt = new StreamEvent("live", 100002);
        evt.addValue("symbol", "IBM");
        evt.addValue("ask", 12.77);
        evt.addValue("bid", 13.77);
        evt.addValue("date", new Date());
        events.add(evt);
        return events;
    }

    @Test
    public void deduplicationMarketEvents() throws StreamsException {
        List<StreamEvent> events = createEvents(10, true);

        DeduplicationProcessor processor = new DeduplicationProcessor();
        processor.setFields(new String[]{"symbol","bid","ask"});
        processor.setExpectedUniqueElements(100);
        processor.initialize(null);

        List<StreamEvent> responseEvents = new ArrayList<>();
        for (StreamEvent e : events) {
            StreamEvent event = processor.apply(e, null);
            if(event!=null){
                responseEvents.add(event);
            }
        }
        assertEquals(2, responseEvents.size());
    }

    @Test
    public void deduplicationEvents() throws StreamsException {
        StreamEvent evt = new StreamEvent("test");
        evt.addValue("a", 100);
        evt.addValue("b", "fred");
        evt.addValue("c", 78.99);

        DeduplicationProcessor processor = new DeduplicationProcessor();
        processor.setExpectedUniqueElements(100);
        processor.setFields(new String[]{"a","b","c"});
        processor.initialize(null);

        StreamEvent event = processor.apply(evt, null);
        assertNotNull( event);

        event = processor.apply(evt, null);
        assertNull( event);
    }

    @Test
    public void deduplicationMarketEventsWithDelay() throws StreamsException {
        List<StreamEvent> events = createEvents(10, true);

        DeduplicationProcessor processor = new DeduplicationProcessor();
        processor.setExpectedUniqueElements(100);
        processor.setFields(new String[]{"symbol","bid","ask"});
        processor.setResetByTime(1);
        processor.initialize(null);

        List<StreamEvent> responseEvents = new ArrayList<>();
        for (StreamEvent e : events) {
            StreamEvent event = processor.apply(e, null);
            if(event!=null){
                responseEvents.add(event);
            }
        }

        await().atMost(5, TimeUnit.SECONDS).until(validateReceivedEvents(responseEvents, 2));
    }

    @Test
    public void deduplicationMarketEventsByEventTrigger() throws StreamsException {
        List<StreamEvent> events = createEvents(500, true);

        DeduplicationProcessor processor = new DeduplicationProcessor();
        processor.setExpectedUniqueElements(100);
        processor.setFields(new String[]{"symbol","bid","ask"});
        processor.setResetBySeenEvents(100);
        processor.initialize(null);

        List<StreamEvent> responseEvents = new ArrayList<>();
        for (StreamEvent e : events) {
            StreamEvent event = processor.apply(e, null);
            if(event!=null){
                responseEvents.add(event);
            }
        }

        assertEquals(6, responseEvents.size());
    }

    private Callable<Boolean> validateReceivedEvents(List<StreamEvent> responseEvents, int numberOfEventsToReceive) {

        return new Callable<Boolean>() {
            public Boolean call() throws Exception {
                return (responseEvents.size() == numberOfEventsToReceive);
            }
        };
    }

}
